<?php

/**
 * @file
 * Provides base methods of the Forge form elements.
 */

/**
 * Form elements base class.
 */
abstract class Forge_Element {
  /**
   * Machine name of the project.
   *
   * @var string
   */
  public $project = '';

  /**
   * Machine name of the element.
   *
   * @var string
   */
  public $machine_name = '';

  /**
   * Title of the element.
   *
   * @var string
   */
  public $title = '';

  /**
   * Value of the element.
   *
   * @var mixed
   */
  public $value = NULL;

  /**
   * Core support flag.
   *
   * @var int
   */
  public $core_support = 0;

  /**
   * Advanced element options (required flag, etc.).
   *
   * @var array
   */
  public $options = array();

  /**
   * Machine name of the group of element.
   *
   * @var string
   */
  public $group = '';

  /**
   * Element information (description and link to api.drupal.org).
   *
   * @var array
   */
  public $info = array();

  /**
   * Machine names of dependent elements.
   *
   * List should be separated by core support.
   * For example: in 6.x core for hook_schema needs enabled hook_install and hook_uninstall, but for 7.x no.
   * @var array
   */
  public $dependencies = array();

  /**
   * Presentation theme function.
   *
   * @var string
   */
  public $presentTheme = 'forge_element_present';

  /**
   * Render theme function.
   *
   * @var string
   */
  public $renderTheme  = 'forge_element_render';

  /**
   * Machine name changed for the project.
   *
   * Example: "hook_menu" -> "mymodule_menu".
   * @var string
   */
  public $renderName   = '';

  /**
   * The file extension to be saved the rendered element.
   *
   * Example: "module" -> "mymodule.module".
   * @var string
   */
  public $renderDestination = 'module';

  /**
   * Constructor.
   *
   * @param string $title
   * @param mixed  $value
   * @param array  $raw   Raw description of the element form hook_forge_element().
   */
  public function __construct($title, $value, $raw = array()) {
    $this->title = t($title);
    $this->value = $value;
    $this->core_support = isset($raw['coreSupport']) ? $raw['coreSupport'] : Forge::CORE_SUPPORT_ALL;
    $this->options = isset($raw['options']) ? $raw['options'] : array();
    $this->info = isset($raw['info']) ? $raw['info'] : array();
    $this->dependencies = isset($raw['dependencies']) ? $raw['dependencies'] : array();
  }

  /**
   * Return HTML id of a element.
   *
   * @return string
   * @ingroup presentation
   */
  public function getHtmlId() {
    return $this->group . '-' . str_replace(array(' ', '.'), '-', $this->machine_name);
  }

  /**
   * Return HTML presentation of the element (preview).
   *
   * @return string
   * @ingroup presentation
   */
  public function getPresent() {
    return theme($this->presentTheme, array('element' => $this));
  }

  /**
   * Return operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getLinks() {
    $op = empty($this->value) ? 'add' : 'edit';
    $links = array(
      forge_popup_link($op, $this, $op),
      forge_link('clear', $this, 'clear'),
    );

    return $links;
  }

  /**
   * Clear a value of the element.
   *
   * @ingroup operations
   */
  public function clear() {
    $this->value = NULL;
  }

  /**
   * Return Drupal form for element editing.
   *
   * @param  array $input $form_state['values']['element'] array
   * @return array
   * @ingroup forms
   */
  public function getForm($input = array()) {
    return array(
      '#type' => 'textfield',
      '#title' => t($this->title),
      '#default_value' => $this->value,
      '#description' => isset($this->options['description']) ? t($this->options['description']) : '',
      '#required' => isset($this->options['required']) ? t($this->options['required']) : '',
    );
  }

  /**
   * Validate element on page submit.
   *
   * Usage example:
   * <code>
   * <?php
   * form_set_error('element', t('An error has occurred.'));
   * ?>
   * </code>
   * @param array $input $form_state['values']['element'] array
   * @ingroup forms
   */
  public function validate($input) {}

  /**
   * On-submit processing of element.
   *
   * @param array $input $form_state['values']['element'] array
   * @ingroup forms
   */
  public function submit($input) {
    $this->value = $input;
  }

  /**
   * Get a list of files that are created element.
   *
   * @return array List of files relative by project location.
   * @ingroup render
   */
  public function getFiles() {
    return array();
  }

  /**
   * Return element arguments specified in hook_forge_element().
   *
   * @param  int    $core    Forge::CORE_SUPPORT_*
   * @param  string $default
   * @return string
   */
  public function optionsArgs($core, $default = '') {
    if ($this->value) {
      if (empty($args) && isset($this->options['args'])) {
        if (is_array($this->options['args'])) {
          if (isset($this->options['args'][$core])) {
            $args = $this->options['args'][$core];
          }
        } else {
          $args = $this->options['args'];
        }
        return $args;
      }
    }
    return $default;
  }

  /**
   * Configure render parameters.
   *
   * @param  object $project     Forge_Project instance.
   * @param  int    $projectType One of Forge::PROJECT_TYPE_* constant.
   * @ingroup render
   */
  public function prerender($project, $projectType = Forge::PROJECT_TYPE_MODULE) {
    if (in_array($projectType, array(Forge::PROJECT_TYPE_MODULE, Forge::PROJECT_TYPE_PROFILE)) || $this->renderDestination == 'info') {
      $this->renderDestination = $project->machine_name . '.' . $this->renderDestination;
    }
    $this->renderName = preg_replace('/^(hook|theme|template)/', $project->machine_name, $this->machine_name);
  }

  /**
   * Return a prefix of the element.
   *
   * @param  int $core One of Forge::CORE_SUPPORT_* constant.
   * @return string
   * @ingroup render
   */
  public function getPrefix($core) {
    return '';
  }

  /**
   * Render the element.
   *
   * @param  array  $output The output separated by project files.
   * @param  int    $core   One of Forge::CORE_SUPPORT_* constant.
   * @param  string $args
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {}

  /**
   * Return a suffix of the element.
   *
   * @param  int $core One of Forge::CORE_SUPPORT_* constant.
   * @return string
   * @ingroup render
   */
  public function getSuffix($core) {
    return '';
  }
}

/**
 * Form element class with multiple values.
 *
 * The constructor is not used. Multiple values are passed as an array.
 * @see Forge_Element
 */
class Forge_Element_Multiple extends Forge_Element {
  public $presentTheme = 'forge_element_present_multiple';

  /**
   * Returns primary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getPrimaryLinks($args = array()) {
    $links = array(
      forge_popup_link('add', $this, 'add'),
      forge_link('clear', $this, 'clear'),
    );

    return $links;
  }

  /**
   * Returns secondary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getSecondaryLinks($args = array()) {
    $links = array(
      forge_popup_link('edit', $this, 'edit', array('key' => $args['key'])),
      forge_link('delete', $this, 'delete', array('key' => $args['key'])),
    );

    return $links;
  }

  /**
   * Clear values of the element.
   *
   * @see Forge_Element::clear()
   * @ingroup operations
   */
  public function clear() {
    $this->value = array();
  }

  /**
   * Deletes specified value.
   *
   * @ingroup operations
   */
  public function delete() {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $key = $query['key'];
      unset($this->value[$key]);
    }
  }

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    $form = parent::getForm($input);
    $form['#default_value'] = '';
    if (isset($query['key'])) {
      $key = $query['key'];
      if (isset($this->value[$key])) {
        $form['#default_value'] = $this->value[$key];
      }
    }

    return $form;
  }

  /**
   * On-submit processing of element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $key = $query['key'];
      $this->value[$key] = $input;
    } else {
      $this->value[] = $input;
    }
  }
}

/**
 * Boolean element class.
 *
 * @see Forge_Element
 */
class Forge_Element_Boolean extends Forge_Element {
  public $presentTheme = 'forge_element_present_boolean';

  /**
   * Return operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getLinks() {
    $links = array(
      forge_popup_link('set', $this, 'edit'),
      forge_link('unset', $this, 'clear'),
    );

    return $links;
  }

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    return array(
      '#type' => 'radios',
      '#title' => t($this->title),
      '#options' => array(
        'NULL' => t('Not used'),
        'TRUE' => 'TRUE',
        'FALSE' => 'FALSE',
      ),
      '#default_value' => $this->value ? $this->value : 'NULL',
      '#description' => isset($this->options['description']) ? t($this->options['description']) : '',
      '#required' => isset($this->options['required']) ? t($this->options['required']) : '',
    );
  }
}
