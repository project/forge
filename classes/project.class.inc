<?php

/**
 * @file
 * Provides the Forge project object type and associated methods.
 */

/**
 * Forge projects class.
 *
 * @see Forge
 */
class Forge_Project {
  /**
   * Project ID.
   *
   * @var integer
   */
  public $pid = 0;

  /**
   * Project revision ID.
   *
   * @var integer
   */
  public $vid = 0;

  /**
   * The PID of the parent project.
   *
   * @var integer
   */
  public $parent_pid = 0;

  /**
   * User ID.
   *
   * @var integer
   */
  public $uid = 0;

  /**
   * The displayed name of object.
   *
   * @var string
   */
  public $name = '';

  /**
   * Machine name of object.
   *
   * @var string
   */
  public $machine_name = '';

  /**
   * The type of this project (module, theme).
   *
   * @var string
   */
  public $type = Forge::PROJECT_TYPE_NAME_MODULE;

  /**
   * A short, preferably one-line description. This field is limited to 255
   * characters.
   *
   * @var string
   */
  public $description = '';

  /**
   * The output folder location for building project, relative to base_path().
   *
   * @var string
   */
  public $location = '';

  /**
   * Forge::CORE_SUPPORT_* flag or a combination thereof.
   *
   * @var integer
   */
  public $core_support = 0;

  /**
   * Needed cores for each branch (6x, 7x, ...).
   *
   * @var array
   */
  public $core = array();

  /**
   * Needed PHP version for each core branch (6x, 7x, ...).
   *
   * @var array
   */
  public $php = array();

  /**
   * If present, the package string groups projects together on the
   * administration pages.
   *
   * @var string
   */
  public $package = '';

  /**
   * Project version added by drupal.org.
   *
   * @var string
   */
  public $version = '';

  /**
   * Serialized revision data.
   *
   * @var array
   */
  public $data = array();

  /**
   * The log entry explaining the changes in this version.
   *
   * @var string
   */
  public $log = '';

  /**
   * Flag indicating whether the project is active.
   *
   * @var integer
   */
  public $status = Forge::PROJECT_STATUS_ACTIVE;

  /**
   * The Unix timestamp when the object was created.
   *
   * @var integer
   */
  public $created = 0;

  /**
   * The Unix timestamp when the object was most recently saved.
   *
   * @var integer
   */
  public $changed = 0;

  /**
   * The Unix timestamp when the object was most recently builded.
   *
   * @var integer
   */
  public $builded = 0;

  /**
   * Subprojects list.
   *
   * @var array
   */
  public $subprojects = array();

  /**
   * If the record insert or update failed, returns FALSE. If it succeeded,
   * returns SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   *
   * @see http://api.drupal.org/api/drupal/includes%21common.inc/constant/SAVED_NEW/7
   * @see http://api.drupal.org/api/drupal/includes%21common.inc/constant/SAVED_UPDATED/7
   * @var mixed
   */
  public $recordState = FALSE;

  /**
   * Tree of elements.
   *
   * @var array
   */
  protected $_tree = array();

  /**
   * Temporary storage for a form.
   *
   * @var object
   */
  protected $_form = NULL;

  /**
   * The flag that signals the need to save the changes temporarily stored in the session.
   */
  public $need_save = FALSE;

  /**
   * Constructor.
   *
   * @param int $pid The project ID
   */
  public function __construct($pid = NULL) {
    if ($pid) {
      $this->load($pid);
    }
  }

  /**
   * Factory for Forge_Project_* classes.
   *
   * @param  string $type Forge::PROJECT_TYPE_NAME_* constant
   * @param  int    $pid The project ID
   * @return Forge_Project
   * @throws Forge_Exception
   */
  public static function factory($type, $pid = NULL) {
    // @todo add "profile" project type.
    if (!in_array($type, array(Forge::PROJECT_TYPE_NAME_MODULE, Forge::PROJECT_TYPE_NAME_THEME))) {
      throw new Forge_Exception("Unknown project type (%type).", array('%type' => $type));
    }

    $class = 'Forge_Project_' . ucfirst(strtolower($type));
    if (!class_exists($class)) {
      throw new Forge_Exception("Unknown project classname (%class).", array('%class' => $class));
    }

    $instance = new $class($pid);
    if (!($instance instanceof Forge_Project)) {
      throw new Forge_Exception("Project class '%class' does not extend Forge_Project.", array('%class' => $class));
    }

    return $instance;
  }

  /**
   * Load the project from the database.
   *
   * @param  int $pid Project ID
   * @param  int $vid Revision ID
   * @return Forge_Project
   * @throws Forge_Exception
   */
  public function load($pid = NULL, $vid = NULL) {
    if (!$pid) {
      $pid = $this->pid;
    }
    if (!$pid) {
      throw new Forge_Exception("The specified project ID (%pid) is invalid.", array('%pid' => var_export($pid, TRUE)));
    }

    $row = db_select('forge_project', 'p')
      ->fields('p')
      ->condition('p.pid', $pid)
      ->execute()
      ->fetchAssoc();
    if (!$row) {
      throw new Forge_Exception("Could not load the project with ID %pid.", array('%pid' => $pid));
    }
    $this->vid = $row['vid'];
    $this->loadRevision($vid);
    Forge::map($this, $row);

    $subprojects = db_select('forge_project', 'p')
      ->fields('p')
      ->condition('p.parent_pid', $pid)
      ->execute();
    foreach ($subprojects as $row) {
      $this->subprojects[$row->machine_name] = $row;
    }

    return $this;
  }

  /**
   * Load the project revision from database.
   *
   * @param  int $vid
   * @return Forge_Project
   * @throws Forge_Exception
   */
  public function loadRevision($vid = NULL) {
    if (!$vid) {
      $vid = $this->vid;
    }
    if (!$vid) {
      throw new Forge_Exception("The specified project revision ID (%vid) is invalid.", array('%vid' => var_export($vid, TRUE)));
    }

    $row = db_select('forge_project_revision', 'r')
      ->fields('r')
      ->condition('r.vid', $vid)
      ->execute()
      ->fetch();
    if (!$row) {
      throw new Forge_Exception("Could not load the project revision with ID %vid.", array('%vid' => $vid));
    }
    $row->data = unserialize($row->data);
    Forge::map($this, $row->data, array('vid', 'status', 'subprojects', 'changed', 'builded'));
    $this->data = $row->data;

    return $this;
  }

  /**
   * Load the project by its machine name.
   *
   * @param  string $machineName
   * @return Forge_Project
   * @throws Forge_Exception
   */
  public static function loadMachineName($machineName, $account = NULL) {
    $query = db_select('forge_project', 'p')
      ->fields('p', array('type', 'pid'))
      ->condition('p.machine_name', $machineName);
    if (is_object($account) && !user_access('access all projects')) {
      $query->condition('p.uid', $account->uid);
    }
    $row = $query->execute()
      ->fetch();
    if (!$row) {
      throw new Forge_Exception("Could not load the project with machine name %machine_name.", array('%machine_name' => $machineName));
    }

    return self::factory($row->type, $row->pid);
  }

  /**
   * Save the project into database.
   *
   * @param  boolean $newRevision Save new revision of the project
   * @return Forge_Project
   */
  public function save($newRevision = FALSE) {
    // Remove unnecessary properties.
    $this->_form = NULL;
    $this->need_save = FALSE;

    // Check required fields.
    $required = array('uid', 'name', 'machine_name', 'location');
    Forge::checkRequired($this, $required);

    // Save object.
    if (!empty($this->pid)) {
      $this->changed = time();
      $this->recordState = drupal_write_record('forge_project', $this, array('pid'));
    } else {
      $this->log = 'initial revision';
      $this->created = time();
      $this->recordState = drupal_write_record('forge_project', $this);
    }

    $this->saveRevision($newRevision);

    return $this;
  }

  /**
   * Save the project revision into database.
   *
   * @param  boolean $newRevision Save new revision of the project
   * @return Forge_Project
   */
  public function saveRevision($newRevision = FALSE) {
    if (empty($this->data)) {
      $this->data = (array)$this;
    }

    // Check required fields.
    $required = array('uid', 'name', 'data');
    Forge::checkRequired($this, $required);

    // Save revision.
    if ($newRevision || !isset($this->vid) || empty($this->vid)) {
      $this->vid = NULL;
      $this->created = time();
      $this->recordState = drupal_write_record('forge_project_revision', $this);
      db_update('forge_project')
        ->fields(array('vid' => $this->vid))
        ->condition('pid', $this->pid)
        ->execute();
      $this->data['vid'] = $this->vid;
    } else {
      $this->changed = time();
    }
    $this->recordState = drupal_write_record('forge_project_revision', $this, array('vid'));

    return $this;
  }

  /**
   * Revert project revision.
   *
   * @param  int $vid
   * @return Forge_Project
   */
  public function revertRevision($vid) {
    $this->vid = $vid;
    return $this->loadRevision($vid)
      ->sessionClear()
      ->save();
  }

  /**
   * Delete a project from the database and session.
   */
  public function delete() {
    db_delete('forge_project_revision')
      ->condition('pid', $this->pid)
      ->execute();
    db_delete('forge_project')
      ->condition('pid', $this->pid)
      ->execute();

    $this->sessionClear();
    if ($this->parent_pid) {
      $parent_name = db_select('forge_project', 'p')
        ->fields('p', array('machine_name'))
        ->condition('p.pid', $this->parent_pid)
        ->execute()
        ->fetchField();
      if ($parent_name) {
        $parent = Forge_Project::session($parent_name);
        unset($parent->subprojects[$this->machine_name]);
        unset($parent->data['subprojects'][$this->machine_name]);
      }
    }
  }

  /**
   * Delete a project revision from the database.
   *
   * @param  int $vid
   * @return Forge_Project
   */
  public function deleteRevision($vid) {
    db_delete('forge_project_revision')
      ->condition('vid', $vid)
      ->execute();
    return $this;
  }

  /**
   * Get a list of the revisions.
   *
   * @return array
   */
  public function getRevisions() {
    $result = db_select('forge_project_revision', 'r')
      ->fields('r', array('vid', 'log', 'created', 'changed'))
      ->condition('r.pid', $this->pid)
      ->execute();
    $return = array();
    foreach ($result as $row) {
      $return[$row->vid] = $row;
    }

    return $return;
  }

  /**
   * Get a list of the operations.
   *
   * @return array
   */
  public function getOperations() {
    $operations = array();

    if ($this->status == Forge::PROJECT_STATUS_ACTIVE) {
      $operations['edit'] = array(
        'title' => t('Edit'),
        'href' => "admin/forge/project/{$this->machine_name}/edit",
      );
      if ($this->type == Forge::PROJECT_TYPE_NAME_MODULE && !$this->parent_pid) {
        $operations['add_subproject'] = array(
          'title' => t('Add subproject'),
          'href' => "admin/forge/project/{$this->machine_name}/add-subproject",
        );
      }
      if (!$this->parent_pid) {
        $operations['view'] = array(
          'title' => t('View build'),
          'href' => "admin/forge/project/{$this->machine_name}/view",
        );
        $operations['build'] = array(
          'title' => t('Build'),
          'href' => "admin/forge/project/{$this->machine_name}/build",
          'query' => drupal_get_destination(),
        );
        $operations['build_download'] = array(
          'title' => t('Build and download'),
          'href' => "admin/forge/project/{$this->machine_name}/build-download",
          'query' => drupal_get_destination(),
        );
        $operations['disable'] = array(
          'title' => t('Disable'),
          'href' => "admin/forge/project/{$this->machine_name}/disable",
        );
      }
    } else {
      $operations['enable'] = array(
        'title' => t('Enable'),
        'href' => "admin/forge/project/{$this->machine_name}/enable",
      );
    }
    $operations['delete'] = array(
      'title' => t('Delete'),
      'href' => "admin/forge/project/{$this->machine_name}/delete",
    );

    return $operations;
  }

  /**
   * Get project's tree as a list.
   *
   * @return array
   */
  public function getHeadList() {
    $list = array();
    if ($this->parent_pid) {
      $type = db_select('forge_project', 'p')
        ->fields('p', array('type'))
        ->condition('p.pid', $this->parent_pid)
        ->execute()
        ->fetchField();
      $parent = self::factory($type, $this->parent_pid);
      $list = $parent->getHeadList();
    } else {
      $list[$this->machine_name] = $this->name;

      if ($this->subprojects) {
        foreach ($this->subprojects as $machine_name => $subproject) {
          $list[$machine_name] = $subproject->name;
        }
      }
    }

    return $list;
  }

  /**
   * Get elements tree.
   *
   * @return array
   */
  public function getElementsTree() {
    if ($this->_tree) {
      return $this->_tree;
    }
    $groups = module_invoke_all('forge_group');
    foreach ($groups as $group => $options) {
      $options['elements'] = array();
      $this->_tree[$group] = $options;
    }

    switch ($this->type) {
      default: case Forge::PROJECT_TYPE_NAME_MODULE:
        $projectType = Forge::PROJECT_TYPE_MODULE;
        break;
      case Forge::PROJECT_TYPE_NAME_THEME:
        $projectType = Forge::PROJECT_TYPE_THEME;
        break;
    }
    $elements = module_invoke_all('forge_element');
    module_invoke_all('forge_element_alter', $elements);
    foreach ($elements as $element_name => $element) {
      if (($element['projectType'] & $projectType) > 0 && ($element['coreSupport'] & $this->core_support) > 0) {
        $this->_tree[$element['group']]['elements'][$element_name] = $element;
      }
    }

    return $this->_tree;
  }

  /**
   * Get elements tree as a JSON.
   *
   * @return array
   */
  public function getElementsJson() {
    $tree = $this->getElementsTree();
    $result = array();
    foreach ($tree as $group => $options) {
      $result[$group] = array(
        'title' => t($options['title']),
        'elements' => array(),
      );
      foreach ($options['elements'] as $element_name => $element) {
        $result[$group]['elements'][$element_name] = array(
          'title' => t($element['name']),
          'description' => '',
        );
        if (isset($element['info'])) {
          if (isset($element['info']['description'])) {
            $result[$group]['elements'][$element_name]['description'] = $element['info']['description'];
          }
        }
      }
    }

    return $result;
  }

  /**
   * Get project editing form object.
   *
   * @return Forge_Form
   */
  public function getForm() {
    $this->_form = new Forge_Form($this);
    $tree = $this->getElementsTree();
    foreach ($tree as $group => $options) {
      $elements = $options['elements'];
      unset($options['elements']);
      $this->_form->addGroup($group, t($options['title']), $options);
      foreach ($elements as $element_name => $element) {
        $this->_addElement($element_name, $element);
      }
    }

    return $this->_form;
  }

  /**
   * Add a element into a form.
   *
   * @param string $elementKey Machine name of the element.
   * @param array  $element    Element info described in a hook_forge_element().
   */
  protected function _addElement($elementKey, $element) {
    if (!isset($this->data[$elementKey]) || empty($this->data[$elementKey])) {
      $this->data[$elementKey] = $element['defaultValue'];
    }
    if (isset($element['group']) && !empty($element['group'])) {
      if (!isset($element['class']) || !class_exists($element['class'])) {
        throw new Forge_Exception("Class (%class) does not exists.", array('%class' => $element['class']));
      }
      $class = $element['class'];
      $this->_form->addGroup($element['group'])
        ->add($elementKey, new $class(t($element['name']),
                                      $this->data[$elementKey],
                                      $element));
    }
  }

  /**
   * Return the project that is saved in session.
   *
   * @param  mixed $project Forge project or machine name of the project.
   * @return Forge_Project
   * @throws Forge_Exception
   */
  public static function session($project) {
    if (!is_object($project) && !is_string($project)) {
      throw new Forge_Exception("The specified argument (%project) is not an Forge_Project object or machine name of project.", array('%property' => var_export($project, TRUE)));
    }
    $machine_name = is_object($project) ? $project->machine_name : $project;

    if (!isset($_SESSION['forge_project'])) {
      $_SESSION['forge_project'] = array();
    }
    if (is_object($project)) {
      if (!isset($_SESSION['forge_project'][$machine_name])) {
        $_SESSION['forge_project'][$machine_name] = $project;
      } elseif (!($_SESSION['forge_project'][$machine_name] instanceof Forge_Project)) {
        $_SESSION['forge_project'][$machine_name] = $project;
      }
    }
    $project = &$_SESSION['forge_project'][$machine_name];

    return $project;
  }

  /**
   * Remove project from an session storage.
   *
   * @param  mixed $project Forge project or machine name of the project.
   * @return Forge_Project
   * @throws Forge_Exception
   */
  public function sessionClear() {
    if (!isset($_SESSION['forge_project'])) {
      return $this;
    }
    if (isset($_SESSION['forge_project'][$this->machine_name])) {
      unset($_SESSION['forge_project'][$this->machine_name]);
    }

    return $this;
  }

  /**
   * Build the project.
   *
   * @param  array $parent_core Core supports of the parent project.
   * @throws Forge_Exception
   */
  public function build($parent_core = array()) {
    // Remove old build (only root project).
    if (!$this->parent_pid) {
      $remove_location = str_replace('/!core', '', $this->location);
      if (file_exists($remove_location)) {
        if (!file_unmanaged_delete_recursive($remove_location)) {
          throw new Forge_Exception("Could not remove the directory %dir.", array('%dir' => $this->location));
        }
      }
    }

    // Prepare building.
    $files = $this->getFiles();
    $core_support = $this->core;
    if ($parent_core) {
      $core_support = $parent_core;
    }
    foreach ($core_support as $core => $corename) {
      if (($this->core_support & $core) == 0) {
        continue;
      }
      $core_uri = str_replace('!core', $corename, $this->location);
      foreach ($files as $file) {
        $file_uri = $core_uri . DIRECTORY_SEPARATOR . $file;
        $file_dir = drupal_dirname($file_uri);
        if (!file_exists($file_dir)) {
          drupal_mkdir($file_dir, NULL, TRUE);
        }
      }
    }

    // Build and write data.
    $elements = module_invoke_all('forge_element');
    module_invoke_all('forge_element_alter', $elements);
    switch ($this->type) {
      default: case Forge::PROJECT_TYPE_NAME_MODULE:
        $projectType = Forge::PROJECT_TYPE_MODULE;
        break;
      case Forge::PROJECT_TYPE_NAME_THEME:
        $projectType = Forge::PROJECT_TYPE_THEME;
        break;
    }

    foreach ($core_support as $core => $corename) {
      if (($this->core_support & $core) == 0) {
        continue;
      }
      $render_output = array();
      foreach ($files as $file) {
        $render_output[$file] = array();
      }

      foreach ($this->_form->tree as $gid => $group) {
        foreach ($group->elements as $name => $elem) {
          $element_spec = $elements[$elem->machine_name];
          if (($element_spec['projectType'] & $projectType) > 0 && ($element_spec['coreSupport'] & $core) > 0) {
            $element = clone $elem;
            $element->prerender($this, $projectType);
            $element->render($render_output, $core);
            // Proceed dependencies.
            if ($element->value) {
              if (isset($element->dependencies[$core]) && !empty($element->dependencies[$core])) {
                foreach ($element->dependencies[$core] as $element_name) {
                  if (isset($group->elements[$element_name]) && is_object($group->elements[$element_name])) {
                    $dependency = clone $group->elements[$element_name];
                    $oldValue = $dependency->value;
                    $dependency->value = TRUE;
                    $dependency->prerender($this, $projectType);
                    $dependency->render($render_output, $core);
                    $dependency->value = $oldValue;
                  }
                }
              }
            }
          }
        }
      }
      $core_uri = str_replace('!core', $corename, $this->location);
      foreach ($render_output as $filename => &$rows) {
        $rows = array_unique($rows);
        $is_template = preg_match('/.tpl.php$/', $filename) && $this->type == Forge::PROJECT_TYPE_NAME_THEME;
        if ($is_template && empty($rows)) {
          continue;
        }
        $file_uri = $core_uri . DIRECTORY_SEPARATOR . $filename;
        $file_path = drupal_realpath($file_uri);
        $file_parts = explode('.', $file_path);
        $file_extension = array_pop($file_parts);
        if (in_array($file_extension, array('install', 'module', 'profile', 'inc', 'php', 'test')) && !$is_template) {
          array_unshift($rows, "<?php\n");
        }
        $rows = implode("\n", $rows);
        if ($rows) {
          $result = file_put_contents($file_path, $rows);
        } else {
          $result = touch($file_path);
        }
        if ($result) {
          drupal_chmod($file_uri);
        }
        else {
          throw new Forge_Exception("Security warning: Couldn't write %file file.", array('%file' => $file_uri));
        }
      }
    }
    $this->builded = time();
    db_update('forge_project')
      ->fields(array('builded' => $this->builded))
      ->condition('pid', $this->pid)
      ->execute();

    // Build subprojects.
    if ($this->subprojects) {
      foreach ($this->subprojects as $machine_name => $row) {
        $subproject = Forge_Project::factory($row->type, $row->pid);
        // Inherit the required properties (if needed).
        $changed = FALSE;
        $required = array('core', 'php', 'package');
        foreach ($required as $property) {
          if (empty($subproject->$property)) {
            $subproject->$property = $this->$property;
            $changed = TRUE;
          }
        }
        if ($changed) {
          $subproject->sessionClear()
            ->save();
        }
        // ... and build.
        $subproject->build($this->core);
      }
    }

    return $this;
  }

  /**
   * Pack and download the last build of the project.
   */
  public function download() {
    try {
      // Create archive location.
      $archive_uri = 'temporary://forge' . DIRECTORY_SEPARATOR . $this->uid;
      if (!file_exists($archive_uri)) {
        drupal_mkdir($archive_uri, NULL, TRUE);
      }

      // Create archive file.
      $archive_uri .= DIRECTORY_SEPARATOR . $this->machine_name . '.zip';
      if (file_exists($archive_uri)) {
        drupal_unlink($archive_uri);
      }
      $archive_file = drupal_realpath($archive_uri);
      $fh = fopen($archive_file, 'w');
      $archive = new ArchiverZip($archive_file);

      // Add build to archive.
      $location = drupal_realpath(str_replace(DIRECTORY_SEPARATOR . '!core', '', $this->location));
      $zipo = $archive->getArchive();
      $files = $this->listenBuild($location);
      if (is_array($files)) {
        foreach ($files as $file) {
          $localname = str_replace($location . DIRECTORY_SEPARATOR, '', $file);
          $zipo->addFile($file, $localname);
        }
      }
      $zipo->close();
      fclose($fh);

      // Download archive.
      header("Content-length:".filesize($archive_file));
      header('Content-Type: application/zip');
      header('Content-Disposition: attachment; filename="' . basename($archive_file) . '"');
      header('Content-Transfer-Encoding: binary');
      echo file_get_contents($archive_file);
      drupal_exit();
    } catch (Exception $e) {
      drupal_set_message(t("Failed to download the project.<br/>System message: '%message'.<br/>Please contact !support or go !back.", array('%message' => $e->getMessage(), '!support' => l(t('support'), 'forge/support'), '!back' => l(t('back'), $_GET['destination']))), 'error');
    }
  }

  /**
   * Listen files in a project build (with subprojects).
   *
   * @param  string $path
   * @param  string $prefix Prefix to exclude.
   * @return array
   */
  public function listenBuild($path, $prefix = '') {
    $return = array();
    $files = glob($path . DIRECTORY_SEPARATOR . '*');
    foreach ($files as $file_path) {
      if (is_dir($file_path)) {
        $subdir_files = $this->listenBuild($file_path, $prefix);
        $return = array_merge($return, $subdir_files);
      } else {
        $return[] = strlen($prefix) ? str_replace($prefix, '', $file_path) : $file_path;
      }
    }
    sort($return, SORT_STRING);

    return $return;
  }

  /**
   * Check if project is built and the build exists and is readable.
   *
   * @return boolean
   */
  public function existsBuild() {
    if (!$this->builded) {
      return FALSE;
    }
    $build_uri = str_replace('!core', '', $this->location);
    $build_path = drupal_realpath($build_uri);
    return file_exists($build_path) && is_dir($build_path) && is_readable($build_path);
  }

  /**
   * Get a list of project files.
   *
   * @return array Files list relative by project location.
   */
  public function getFiles() {
    if (!$this->_form) {
      $this->getForm();
    }

    $files = array(
      $this->machine_name . '.info',
    );
    switch ($this->type) {
      case Forge::PROJECT_TYPE_NAME_MODULE:
        $files[] = $this->machine_name . '.module';
        break;
      case Forge::PROJECT_TYPE_NAME_THEME:
        $files[] = 'template.php';
        break;
      case Forge::PROJECT_TYPE_NAME_PROFILE:
        $files[] = $this->machine_name . '.profile';
        break;
    }
    foreach ($this->_form->tree as $gid => $group) {
      foreach ($group->elements as $name => $element) {
        $element_files = $element->getFiles();
        $files = array_merge($files, $element_files);
      }
    }
    $files = array_unique($files);

    return $files;
  }

  /**
   * Get files tree relative to project location.
   *
   * @return array
   */
  public function getFilesTree() {
    $build_path = drupal_realpath(str_replace(DIRECTORY_SEPARATOR . '!core', '', $this->location));
    $files = $this->listenBuild($build_path, $build_path . DIRECTORY_SEPARATOR);
    $tree = array();
    foreach ($files as $filepath) {
      $ptree = &$tree;
      $file_parts = explode(DIRECTORY_SEPARATOR, $filepath);
      foreach ($file_parts as $key => $part) {
        if ($key < (count($file_parts)-1)) {
          if (!isset($ptree[$part])) {
            $ptree[$part] = array();
          }
          $ptree = &$ptree[$part];
        } else {
          $ptree[$part] = $filepath;
        }
      }
    }

    return $tree;
  }
}

/**
 * Forge module project class.
 *
 * @see Forge_Project
 */
class Forge_Project_Module extends Forge_Project {}

/**
 * Forge theme project class.
 *
 * @see Forge_Project
 */
class Forge_Project_Theme extends Forge_Project {}
