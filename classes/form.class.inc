<?php

/**
 * @file
 * The Forge project editing form class.
 */

/**
 * Project editing form class.
 *
 * @see Forge_Project
 */
class Forge_Form {
  /**
   * Project object reference.
   *
   * @var object
   */
  public $project;

  /**
   * Form elements storage.
   *
   * @var array
   */
  public $tree = array();

  /**
   * Constructor.
   *
   * @param Forge_Project $project
   */
  public function __construct($project) {
    $this->project = $project;
  }

  /**
   * Add new elements group.
   *
   * @param string $name
   * @param string $title
   * @param array  $options
   * @return Forge_Form_Group
   * @throws Forge_Exception
   */
  public function addGroup($name, $title = NULL, $options = array()) {
    if (!isset($this->tree[$name])) {
      $this->tree[$name] = new Forge_Form_Group($name, $title, $options);
    }
    $group = $this->tree[$name];
    $group->project = $this->project->machine_name;
    if (!($group instanceof Forge_Form_Group)) {
      throw new Forge_Exception("The specified name '%name' is not an instance of Forge_Form_Group.", array('%name' => $name));
    }

    return $group;
  }

  /**
   * Get object of the element.
   *
   * @param  string $group   Machine name of the group of element
   * @param  string $element Machine name of the element
   * @return Forge_Element
   * @throws Forge_Exception
   */
  public function getElement($group, $element) {
    if (!isset($this->tree[$group])) {
      throw new Forge_Exception("The specified group '%group' not found.", array('%group' => $group));
    }
    if (!isset($this->tree[$group]->elements[$element])) {
      throw new Forge_Exception("The specified element '%element' cannot be found in the group '%group'.", array('%element' => $element, '%group' => $group));
    }
    $element = $this->tree[$group]->elements[$element];
    if (!($element instanceof Forge_Element)) {
      throw new Forge_Exception("The specified element '%element' is not an instance of Forge_Element.", array('%element' => $element));
    }

    return $element;
  }

  /**
   * Return Drupal implementaion of a form.
   *
   * @return array
   */
  public function toDrupal() {
    $form = array(
      '#theme' => 'forge_project_edit',
      '#project' => $this->project,
      '#operations' => $this->project->getOperations(),
      '#head_list' => $this->project->getHeadList(),
      '#attributes' => array('autocomplete' => 'off'),
    );

    $form['actions']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    $form['actions']['save_revision'] = array(
      '#type' => 'submit',
      '#value' => t('Save as revision'),
    );

    $form['search'] = array(
      '#type' => 'textfield',
      '#attributes' => array('placeholder' => t('Press "S" to focus me')),
    );

    $form['groups']['group_vt'] = array(
      '#type' => 'vertical_tabs',
    );
    foreach ($this->tree as $name => $group) {
      $form['groups'][$name] = $group->toDrupal();
    }

    return $form;
  }
}

/**
 * Elements group class.
 */
class Forge_Form_Group {
  /**
   * Machine name of the project.
   *
   * @var string
   */
  public $project = '';

  /**
   * Machine name of the group.
   *
   * @var string
   */
  public $name = '';

  /**
   * Title of the group.
   *
   * @var string
   */
  public $title = '';

  /**
   * Advanced group options (description, etc.).
   *
   * @var array
   */
  public $options = array();

  /**
   * Elements storage.
   *
   * @var array
   */
  public $elements = array();

  /**
   * Constructor.
   *
   * @param string $name
   * @param string $title
   * @param array  $options
   */
  public function __construct($name, $title, $options = array()) {
    $this->name = $name;
    $this->title = t($title);
    $this->options = $options;
  }

  /**
   * Add new element to the storage.
   *
   * @param string $name Element name
   * @param mixed  $element Object of a element
   */
  public function add($name, $element) {
    $element->machine_name = $name;
    $element->group = $this->name;
    $element->project = $this->project;

    $this->elements[$name] = $element;
    return $this;
  }

  /**
   * @see Forge_Form::toDrupal()
   */
  public function toDrupal() {
    $form = array();
    foreach ($this->elements as $name => $element) {
      $form[$name] = array('#markup' => $element->getPresent());
    }
    if ($form) {
      $form['#type'] = 'fieldset';
      $form['#title'] = $this->title;
      $form['#group'] = 'group_vt';
      $form['#attributes']['id'] = 'forge-group-' . $this->name;
    }
    if (isset($this->options['weight'])) {
      $form['#weight'] = $this->options['weight'];
    }

    return $form;
  }
}
