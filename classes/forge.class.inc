<?php

/**
 * @file
 * Provides the Forge common API class.
 */

/**
 * Forge basic API class.
 */
class Forge {
  /**
   * Drupal core support flag constants.
   */
  const CORE_SUPPORT_6x           = 1;
  const CORE_SUPPORT_7x           = 2;
  const CORE_SUPPORT_8x           = 4;
  const CORE_SUPPORT_ALL          = 7; // Forge::CORE_SUPPORT_6x | Forge::CORE_SUPPORT_7x | Forge::CORE_SUPPORT_8x;
  const CORE_SUPPORT_MORE_THAN_6x = 6; // Forge::CORE_SUPPORT_7x | Forge::CORE_SUPPORT_7x
  const CORE_SUPPORT_BOTH_6x_7x   = 3; // Forge::CORE_SUPPORT_6x | Forge::CORE_SUPPORT_7x;

  /**
   * Project type constants.
   */
  const PROJECT_TYPE_NAME_MODULE  = 'module';
  const PROJECT_TYPE_NAME_THEME   = 'theme';
  const PROJECT_TYPE_NAME_PROFILE = 'profile'; // Reserved. Will be used in the future.

  /**
   * Project type flag constants.
   */
  const PROJECT_TYPE_MODULE       = 1;
  const PROJECT_TYPE_THEME        = 2;
  const PROJECT_TYPE_PROFILE      = 4; // Reserved. Will be used in the future.
  const PROJECT_TYPE_ALL          = 7; // Forge::PROJECT_TYPE_MODULE | Forge::PROJECT_TYPE_THEME | Forge::PROJECT_TYPE_PROFILE;

  /**
   * Project status constants.
   */
  const PROJECT_STATUS_ACTIVE     = 1;
  const PROJECT_STATUS_INACTIVE   = 0;

  /**
   * Returns default location path.
   *
   * @param  boolean $fullPath Return full path
   * @return string
   */
  public static function getDefaultLocationPath($fullPath = FALSE) {
    $path = 'public://forge';
    if ($fullPath) {
      $path = drupal_realpath($path);
    }
    return $path;
  }

  /**
   * Map values to the object.
   *
   * @param  object $object
   * @param  array $values $form_state['values'] array
   * @param  array $exclude Fields that excuding from mapping
   * @return object
   */
  public static function map($object, $values = array(), $exclude = array()) {
    if (!is_array($values)) {
      return $object;
    }
    foreach ($values as $key => $value) {
      if (property_exists($object, $key) && !in_array($key, $exclude)) {
        $object->$key = $value;
      }
    }

    return $object;
  }

  /**
   * Check required fields.
   *
   * @param  mixed $object
   * @param  array $required
   * @return void
   * @throws Forge_Exception
   */
  public static function checkRequired($object, $required = array()) {
    $empty = array();
    foreach ($required as $property) {
      if (empty($object->$property)) {
        $empty[] = $property;
      }
    }
    if (!empty($empty)) {
      throw new Forge_Exception("The specified fields (!fields) can not be empty.", array('!fields' => implode(', ', $empty)));
    }
  }

  /**
   * Replace array keys with data.
   *
   * @param  array  $array    Array to editing
   * @param  string $oldKey   Old key
   * @param  string $newKey   Replacement key
   * @param  mixed  $newValue New value (if required to change)
   * @return array
   */
  public static function arrayKeyReplace($array, $oldKey, $newKey, $newValue = NULL) {
    if (is_null($newValue)) {
      $newValue = $array[$oldKey];
    }
    $keys = array_keys($array);
    $offset = array_search($oldKey, $keys);
    $leftData = array_slice($array, 0, $offset, TRUE);
    $newData = array($newKey => $newValue);
    $rightData = array_slice($array, $offset + 1, NULL, TRUE);

    return array_merge($leftData, $newData, $rightData);
  }

  /**
   * Clean the arguments.
   *
   * Example:
   * <code>
   * "int $arg0 = 0, $arg1 = 'str'" -> "$arg0, $arg1"
   * </code>
   * @param  string $args Source args
   * @return string       Clean args
   */
  public static function cleanArgs($args) {
    $matches = array(
      '/\s*=>*\s*\'{1}[^\']*\'{1}/',
      '/\s*=>*\s*"[^"]*"/',
      '/\s*=>*\s*array\([^\)]*\)+/',
      '/\s*=\s*[0-9a-zA-Z_]+/',
      '/[0-9a-zA-Z\\\\_]*\s*\&*(\$+)/',
    );
    $replaces = array(
      '', '', '', '$1', ' $1',
    );
    return trim(preg_replace($matches, $replaces, $args));
  }

  /**
   * Fetch file source from Drupal repository.
   *
   * @param  string $path Relative file path
   * @param  int    $core Forge::CORE_SUPPORT_* constants
   * @return string
   */
  public static function getSource($path, $core) {
    $pieces = parse_url($path);
    $absolute = isset($pieces['scheme']) && isset($pieces['host']);
    if (!$absolute) {
      $source_url = variable_get('drupal_source_url', 'https://raw.github.com/drupal/drupal/!core');
      $corename = '7.x';
      switch ($core) {
        case self::CORE_SUPPORT_6x:
          $corename = '6.x';
          break;
        case self::CORE_SUPPORT_8x:
          $corename = '8.x';
          break;
      }
      $source_url = str_replace('!core', $corename, $source_url);
      $source_url .= '/' . ltrim($path, '/');
    }

    $contents = @file_get_contents($source_url);
    if ($contents === FALSE) {
      $file = basename($source_url);
      $contents = <<<EOL
<?php

/**
 * Failed to get the source code of file {$file}.
 * Please download it yourself here: {$source_url}.
 */
EOL;
    }

    return $contents;
  }
}

/**
 * Exception wrapper class.
 */
class Forge_Exception extends Exception {
  /**
   * Exception constructor.
   *
   * @see    http://api.drupal.org/api/drupal/includes%21bootstrap.inc/function/watchdog_exception/7
   * @param  string    $message
   * @param  array     $variables
   * @param  integer   $code
   * @param  Exception $previous
   * @return void
   */
  public function __construct($message = '', $variables = array(), $code = 0, Exception $previous = NULL) {
    watchdog_exception('forge', $this, $message, $variables);

    $message = t($message, $variables);
    parent::__construct($message, (int)$code, $previous);
  }
}
