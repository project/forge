<?php

/**
 * @file
 * Provides base methods of the Forge theme and template elements.
 */

/**
 * Theme element class.
 *
 * @see Forge_Element_Hook
 */
class Forge_Element_Theme extends Forge_Element_Hook {
  public $renderDestination = 'template.php';

  /**
   * Return a prefix of the theme hook.
   *
   * @see Forge_Element::getPrefix()
   * @ingroup render
   */
  public function getPrefix($core) {
    $custom = 'Override of ' . $this->machine_name . '()';
    return theme('forge_element_render_hook_prefix', array('element' => $this, 'core' => $core, 'custom' => $custom));
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      if (empty($args) && isset($this->options['args'])) {
        if (is_array($this->options['args'])) {
          if (isset($this->options['args'][$core])) {
            $args = $this->options['args'][$core];
          }
        } else {
          $args = $this->options['args'];
        }
      }
      $clean_args = $args ? Forge::cleanArgs($args) : '';
      $body = "return {$this->machine_name}({$clean_args});";
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'args' => $args, 'body' => $body));
    }
  }
}

/**
 * Theme element class.
 *
 * @see Forge_Element_Hook
 */
class Forge_Element_Template extends Forge_Element_Hook {
  /**
   * @see Forge_Element::getFiles()
   * @ingroup render
   */
  public function getFiles() {
    return array('templates' . DIRECTORY_SEPARATOR . $this->machine_name);
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      if (empty($args) && isset($this->options['args'])) {
        if (is_array($this->options['args'])) {
          if (isset($this->options['args'][$core])) {
            $args = $this->options['args'][$core];
          }
        } else {
          $args = $this->options['args'];
        }
      }
      $output['templates' . DIRECTORY_SEPARATOR . $this->machine_name][] = Forge::getSource($args, $core);
    }
  }
}
