<?php

/**
 * @file
 * Provides base methods of the Forge hook elements.
 */

/**
 * Hook element class.
 *
 * @see Forge_Element
 */
class Forge_Element_Hook extends Forge_Element {
  public $presentTheme = 'forge_element_present_hook';
  public $renderTheme = 'forge_element_render_hook';

  /**
   * Return operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getLinks() {
    $op = empty($this->value) ? 'enable' : 'disable';
    $links = array(
      forge_link($op, $this, $op),
    );

    return $links;
  }

  /**
   * Enable a hook.
   *
   * @ingroup operations
   */
  public function enable() {
    $this->value = TRUE;
  }

  /**
   * Disable a hook.
   *
   * @ingroup operations
   */
  public function disable() {
    $this->value = FALSE;
  }

  /**
   * Return a prefix of the hook.
   *
   * @see Forge_Element::getPrefix()
   * @ingroup render
   */
  public function getPrefix($core) {
    return theme('forge_element_render_hook_prefix', array('element' => $this, 'core' => $core));
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $args = $this->optionsArgs($core, $args);
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'args' => $args));
    }
  }

  /**
   * Return a suffix of the hook.
   *
   * @see Forge_Element::getSuffix()
   * @ingroup render
   */
  public function getSuffix($core) {
    return theme('forge_element_render_hook_suffix', array('element' => $this, 'core' => $core));
  }
}

/**
 * Form hook class with multiple values.
 *
 * @see Forge_Element_Multiple
 */
class Forge_Element_Hook_Multiple extends Forge_Element_Multiple {
  public $renderTheme = 'forge_element_render_hook';

  /**
   * Return a prefix of the hook.
   *
   * @see Forge_Element::getPrefix()
   * @ingroup render
   */
  public function getPrefix($core) {
    return theme('forge_element_render_hook_prefix', array('element' => $this, 'core' => $core));
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {}

  /**
   * Return a suffix of the hook.
   *
   * @see Forge_Element::getSuffix()
   * @ingroup render
   */
  public function getSuffix($core) {
    return theme('forge_element_render_hook_suffix', array('element' => $this, 'core' => $core));
  }
}
