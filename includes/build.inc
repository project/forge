<?php

/**
 * @file
 * Forge project building functions.
 */

/**
 * Project building menu callback.
 *
 * @param  Forge_Project $project
 * @param  string        $after_build After build operation.
 */
function forge_project_build($project, $after_build = 'view') {
  global $user;

  try {
    $changed = $project->changed ? $project->changed : $project->created;
    $builded = $project->builded;
    $message = NULL;
    // !!!
    $project->build();
    // die;
    // !!!
    if ($builded == 0) {
      $message = t('The project %project has been builded.', array('%project' => $project->name));
      $project->build();
    } elseif ($builded < $changed) {
      $message = t('The project %project was rebuilded because it has been modified since the last build.', array('%project' => $project->name));
      $project->build();
    } else {
      $message = t('Building the project was skipped because it was not modified since the last build.');
    }
    if ($message && $after_build !== 'download') {
      drupal_set_message($message);
    }
  } catch(Exception $e) {
    drupal_set_message(t("Sorry, I couldn't do build your project.<br/>Error message: '%message'.<br/>Please contact !support or go !back.", array('%message' => $e->getMessage(), '!support' => l(t('support'), 'forge/support'), '!back' => l(t('back'), $_GET['destination']))), 'error');
  }

  switch ($after_build) {
    case 'download':
      $project->download();
      break;
    default:
      drupal_goto("admin/forge/project/{$project->machine_name}/view");
      break;
  }
}

/**
 * Overview page of the project build.
 *
 * @param Forge_Project $project
 */
function forge_project_build_view($project) {
  drupal_add_library('system', 'drupal.ajax');
  ctools_include('ajax');
  forge_add_css('view');
  forge_add_js('view');

  drupal_set_title(forge_project_title($project));

  $tree = $project->getFilesTree();
  return theme('forge_project_build', array('project' => $project));
}

/**
 * Return file contents for view.
 *
 * @param  Forge_Project $project
 * @return array         Array of AJAX commands.
 */
function forge_project_build_file_view($project) {
  $result = array('#type' => 'ajax');

  $query = drupal_get_query_parameters();
  $path = $query['file'];
  $path_parts = explode(DIRECTORY_SEPARATOR, $path);
  $file = array_pop($path_parts);

  // Protection against PHP injection.
  $build_path = drupal_realpath(str_replace(DIRECTORY_SEPARATOR . '!core', '', $project->location));
  $files = $project->listenBuild($build_path, $build_path . DIRECTORY_SEPARATOR);
  if (!in_array($path, $files)) {
    watchdog('forge', 'Trying to access a file outside the context of the project.<br/>Project: %machine_name, File path: %path', array('%machine_name' => $project->machine_name, '%path' => $path), WATCHDOG_ERROR);
    $result['#commands'][] = ajax_command_html('#forge-build-preview', '<div class="messages error">' . t('Security warning.<br/>File %file is outside the context of the project.<br/>About this incident was reported to the administrator.', array('%file' => $path)) . '</div>');
    return $result;
  }

  $link_id = 'ff-' . md5($path);
  $file_link = l(
    $file,
    "admin/forge/project/{$project->machine_name}/build-view",
    array(
      'query' => array('file' => $path),
      'attributes' => array(
        'id' => $link_id,
        'class' => array('forge-file', 'open', 'use-ajax'),
      )
    )
  );
  $output = file_get_contents($build_path . DIRECTORY_SEPARATOR . $path);
  $path_parts = explode('.', $path);
  $file_extension = array_pop($path_parts);
  if (in_array($file_extension, array('install', 'module', 'inc', 'php', 'test'))) {
    $output = highlight_string($output, TRUE);
  } else {
    $output = '<pre>' . $output . '<pre>';
  }
  $result['#commands'][] = ajax_command_replace('#' . $link_id, $file_link);
  $result['#commands'][] = ajax_command_html('#forge-build-preview', $output);

  return $result;
}
