<?php

/**
 * @file
 * Element forms and functions.
 */

/**
 * Project element editing page.
 *
 * @param  boolean $js      CTools ajax flag
 * @param  string  $project Project machine name
 * @param  string  $group   Element group
 * @param  string  $element Element machine name
 * @param  string  $op
 */
function forge_element_page($js = FALSE, $project, $group, $element, $op) {
  if ($js) {
    ctools_include('modal');
    ctools_include('ajax');
  }

  if (in_array($op, array('add', 'edit'))) {
    $query = $_GET;
    unset($query['q']);
    $form_state = array(
      'title' => t('Edit element !key', array('!key' => $element)),
      'ajax' => TRUE,
      'element' => array(
        'project' => $project,
        'group' => $group,
        'name' => $element,
        'op' => $op,
        'query' => $query,
      ),
    );

    if ($js) {
      $commands = array();
      $commands = ctools_modal_form_wrapper('forge_element_form', $form_state);
      if (!empty($form_state['executed'])) {
        $commands = array();
        if ($form_state['values']['op'] == t('Update')) {
          $project = Forge_Project::session($form_state['element']['project']);
          $form = $project->getForm();
          $element = $form->getElement($group, $element);
          $commands = forge_element_update_commands($project, $element);
          $commands[] = ctools_modal_command_dismiss();
        }
        elseif ($form_state['values']['op'] == t('Cancel')) {
          $commands[] = ctools_modal_command_dismiss();
        }
        else {
          $commands = ctools_modal_form_render($form_state, '');
        }
      }
      print ajax_render($commands);
      exit;
    } else {
      $form_state['ajax'] = FALSE;
      $form = drupal_build_form('forge_element_form', $form_state);
      return drupal_render($form);
    }
  } else {
    drupal_access_denied();
  }
}

/**
 * Perform an operation over the element.
 *
 * @param  string  $project Project machine name
 * @param  string  $group   Element group
 * @param  string  $element Element machine name
 * @param  string  $op
 */
function forge_element_op($project, $group, $element, $op) {
  $project = Forge_Project::session($project);
  $form = $project->getForm();
  $element = $form->getElement($group, $element);
  $commands = array();
  if (method_exists($element, $op)) {
    $element->$op();
    $project->data[$element->machine_name] = $element->value;
    file_put_contents('/tmp/test-debug1', print_r($_SESSION, 1));
    $commands = forge_element_update_commands($project, $element);
  } else {
    drupal_access_denied();
  }

  $result = array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
  return $result;
}

/**
 * Return Ajax-commands for update element in project editing page.
 *
 * @see forge_element_page()
 * @param  object $project Forge_Project object.
 * @param  object $element Forge_Element object.
 * @return array           An Ajax-commands array.
 */
function forge_element_update_commands($project, $element) {
  $project->need_save = TRUE;

  $commands = array();
  $commands[] = ajax_command_replace('#' . $element->getHtmlId(), $element->getPresent());
  $commands[] = ajax_command_html('#forge-messages', t('Your changes will not be saved until you click the "Save" or "Save as revision".'));
  $commands[] = ajax_command_css('#forge-messages', array('display' => 'block'));
  $commands[] = ajax_command_css('#forge-actions', array('display' => 'block'));

  $query = drupal_get_query_parameters();
  if (isset($query['debug'])) {
    $commands[] = ajax_command_settings($project->data[$element->machine_name]);
  }

  return $commands;
}

/**
 * Project element editing form.
 */
function forge_element_form($form, &$form_state) {
  forge_add_css();

  $form = array(
    '#submit' => array('forge_element_form_submit'),
  );
  $project = Forge_Project::session($form_state['element']['project']);
  $project_form = $project->getForm();
  $element = $form_state['element'];
  $element = $project_form->getElement($element['group'], $element['name']);
  $values = array();
  if (isset($form_state['values']) && isset($form_state['values']['element'])) {
    $values = $form_state['values']['element'];
    if (isset($form_state['triggering_element'])) {
      $values['triggering_element_name'] = $form_state['triggering_element']['#name'];
    }
  }
  $form['element'] = $element->getForm($values);
  $form['element']['#prefix'] = '<div id="element-wrapper">';
  $form['element']['#suffix'] = '</div>';

  $form['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('forge_element_form_update'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Implements hook_validate().
 */
function forge_element_form_validate($form, &$form_state) {
  $project = Forge_Project::session($form_state['element']['project']);
  $project_form = $project->getForm();
  $element = $form_state['element'];
  $element = $project_form->getElement($element['group'], $element['name']);
  $element->validate($form_state['values']['element']);
}

/**
 * Implements hook_submit().
 */
function forge_element_form_update($form, &$form_state) {
  $project = Forge_Project::session($form_state['element']['project']);
  $project_form = $project->getForm();
  $element = $form_state['element'];
  $element = $project_form->getElement($element['group'], $element['name']);
  $element->submit($form_state['values']['element']);
  $project->data[$element->machine_name] = $element->value;
  // Simpletest debug
  $query = drupal_get_query_parameters();
  if (isset($query['debug'])) {
    $commands = forge_element_update_commands($project, $element);
    print ajax_render($commands);
    exit;
  }
}

/**
 * Implements hook_submit().
 */
function forge_element_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback that returns a part of the form.
 */
function forge_element_form_callback($form, $form_state) {
  return $form['element'];
}
