<?php

/**
 * @file
 * Revision functionality for the Forge module.
 */

/**
 * Project revision addition form.
 */
function forge_project_revision_add($form, &$form_state, $project) {
  $form['#project'] = $project;

  $form['log'] = array(
    '#type' => 'textarea',
    '#title' => t('Revision log message'),
    '#description' => t('Provide an explanation of the changes you are making. This will help other authors understand your motivations.'),
    '#required' => TRUE,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Implements hook_submit().
 */
function forge_project_revision_add_submit($form, &$form_state) {
  $form['#project']->log = $form['#project']->data['log'] = $form_state['values']['log'];
  $form['#project']->save(TRUE);
  drupal_set_message(t('The project %project has been saved as an new revision.', array('%project' => $form['#project']->name)));
}

/**
 * Project revisions overview page.
 *
 * @param  object $project
 * @return array
 */
function forge_project_revision_overview($project) {
  drupal_set_title(t('Revisions for %title', array('%title' => $project->name)), PASS_THROUGH);
  forge_add_css();

  $header = array(
    t('Revision'),
    t('Log message'),
    t('Created'),
    t('Last update'),
    t('Operations'),
  );

  $revisions = $project->getRevisions();
  $rows = array();
  foreach ($revisions as $vid => $revision) {
    $row = array(
      'vid' => array('data' => $vid),
      'log' => array('data' => $revision->log ? check_plain($revision->log) : ('<em>' . t('no message') . '</em>')),
      'created' => array('data' => format_date($revision->created)),
      'changed' => array('data' => $revision->changed ? format_date($revision->changed) : ''),
    );
    if ($project->vid == $vid) {
      $row['vid']['class'] = array('revision-current');
      $row['log']['class'] = array('revision-current');
      $row['created']['class'] = array('revision-current');
      $row['changed']['class'] = array('revision-current');
      $row[] = array(
        'data' => '<em>' . t('current revision') . '</em>',
        'class' => array('revision-current'),
      );
    } else {
      $operations = array();
      $operations['revert'] = array(
        'title' => t('revert'),
        'href' => "admin/forge/project/{$project->machine_name}/revision/{$vid}/revert",
      );
      $operations['delete'] = array(
        'title' => t('delete'),
        'href' => "admin/forge/project/{$project->machine_name}/revision/{$vid}/delete",
        'query' => drupal_get_destination(),
      );
      $operations_rendered = theme('links__ctools_dropbutton', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline'))));
      $row[] = array(
        'data' => $operations_rendered,
        'class' => 'forge-operations project-operations',
      );
    }

    $rows[] = $row;
  }

  $build['project_revisions'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $build;
}

/**
 * Revert project revision.
 *
 * @param  Forge_Project $project
 * @param  int $vid
 */
function forge_project_revision_revert($project, $vid) {
  $project->revertRevision($vid);
  drupal_set_message(t('Project %project has been reverted back to the revision %vid.', array('%project' => $project->name, '%vid' => $vid)));
  drupal_goto("admin/forge/project/{$project->machine_name}/edit");
}

/**
 * Revision deletion confirmation form.
 */
function forge_project_revision_delete_confirm($form, &$form_state, $project, $vid) {
  $form['#project'] = $project;
  $form['#vid'] = $vid;
  return confirm_form($form,
                      t('Are you sure you want to delete the revision %vid of the project %project?', array('%vid' => $vid, '%project' => $project->name)),
                      "admin/forge/project/{$project->machine_name}/revision",
                      t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function forge_project_revision_delete_confirm_submit($form, &$form_state) {
  $form['#project']->deleteRevision($form['#vid']);
  drupal_set_message(t('Revision %vid of project %project has been deleted.', array('%vid' => $form['#vid'], '%project' => $form['#project']->name)));
}
