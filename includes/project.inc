<?php

/**
 * @file
 * Project forms and functions.
 */

/**
 * Package list overview page.
 */
function forge_projects_list() {
  global $user;

  forge_add_css();

  $admin = user_access('access all projects');
  $query = db_select('forge_project', 'p')
    ->fields('p', array('pid', 'type'))
    ->condition('p.parent_pid', 0);
  if (!$admin) {
    $query->condition('uid', $user->uid);
  }
  $items = $query->execute()
    ->fetchAll();
  $rows = array();
  foreach ($items as $row) {
    $project = Forge_Project::factory($row->type, $row->pid);

    $operations = $project->getOperations();
    $operations_rendered = theme('links__ctools_dropbutton', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline'))));

    $class = '';
    if ($project->status == Forge::PROJECT_STATUS_INACTIVE) {
      $class = 'forge-disabled';
    }

    $data = array(
      theme('forge_project_info', array('project' => $project)),
      check_plain($project->description),
      theme('forge_project_subprojects', array('project' => $project, 'subprojects' => $project->subprojects)),
    );
    if ($admin) {
      $account = user_load($project->uid);
      $data[] = theme('username', array('account' => $account));
    }
    $data[] = array('data' => $operations_rendered, 'class' => 'forge-operations project-operations');
    $rows[] = array(
      'data' => $data,
      'class' => array($class),
    );
  }

  $header = array(
    t('Project name'),
    t('Description'),
    t('Subprojects'),
  );
  if ($admin) {
    $header[] = t('Author');
  }
  $header[] = t('Operations');

  $table = array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('No projects found.'),
    'attributes' => array('id' => 'ctools-export-ui-list-items'),
  );
  return theme('table', $table);
}

/**
 * Project addition form.
 *
 * @parent mixed $parent Forge_Project or NULL
 * @ingroup forms
 */
function forge_project_add($form, &$form_state, $parent = NULL) {
  if ($parent) {
    $form['type'] = array('#type' => 'value', '#value' => $parent->type);
    $core_support = array();
    $core_support_all = array(
      Forge::CORE_SUPPORT_6x,
      Forge::CORE_SUPPORT_7x,
    );
    foreach ($core_support_all as $const) {
      if (($parent->core_support & $const) > 0) {
        $core_support[] = $const;
      }
    }
    $form['core_support'] = array('#type' => 'value', '#value' => $core_support);
  } else {
    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('Project type'),
      '#options' => array(
        Forge::PROJECT_TYPE_NAME_MODULE => t('Module'),
        Forge::PROJECT_TYPE_NAME_THEME => t('Theme'),
        // @todo add profile project type.
      ),
      '#default_value' => Forge::PROJECT_TYPE_NAME_MODULE,
      '#required' => TRUE,
    );
    $form['core_support'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Supported Drupal cores'),
      '#options' => array(
        Forge::CORE_SUPPORT_6x => '6x',
        Forge::CORE_SUPPORT_7x => '7x',
      ),
      '#default_value' => array(Forge::CORE_SUPPORT_7x),
      '#required' => TRUE,
      '#description' => t('Specify which cores are supported by your project. This will provide possibility to fill specific elements for these versions.'),
    );
  }


  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Human readable name of the project.'),
    '#required' => TRUE,
  );

  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#description' => t('A unique machine readable name of the project. It must only contain lowercase letters, numbers and hyphens.'),
    '#machine_name' => array(
      'exists' => 'forge_project_exists',
      'source' => array('name'),
      'label' => t('Machine name'),
      'replace_pattern' => '[^a-z0-9_]+',
      'replace' => '_',
    ),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
  );

  $form['parent_pid'] = array('#type' => 'value', '#value' => NULL);
  $form['parent_name'] = array('#type' => 'value', '#value' => NULL);
  if ($parent) {
    $form['parent_pid']['#value'] = $parent->pid;
    $form['parent_name']['#value'] = $parent->machine_name;
  }

  $form['save_exit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and exit'),
  );

  $form['save_continue'] = array(
    '#type' => 'submit',
    '#value' => t('Save and continue'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Implements hook_submit().
 */
function forge_project_add_submit($form, &$form_state) {
  global $user;

  switch ($form_state['values']['op']) {
    case t('Save and exit'): case t('Save and continue'):
      $project = Forge_Project::factory($form_state['values']['type']);
      $project->uid = $user->uid;
      // Build location path.
      $location = array(
        Forge::getDefaultLocationPath(),
        $user->uid,
      );
      if ($form_state['values']['parent_name']) {
        $location[] = $form_state['values']['parent_name'];
        $location[] = '!core';
      }
      $location[] = $form_state['values']['machine_name'];
      if (!$form_state['values']['parent_name']) {
        $location[] = '!core';
      }
      $project->location = implode(DIRECTORY_SEPARATOR, $location);
      // Core support.
      $form_state['values']['core_support'] = array_sum($form_state['values']['core_support']);

      Forge::map($project, $form_state['values'])->save();
      if (in_array($project->recordState, array(SAVED_NEW, SAVED_UPDATED))) {
        // Update parent session.
        if ($parent_name = $form_state['values']['parent_name']) {
          if (isset($_SESSION['forge_project'][$parent_name])) {
            $_SESSION['forge_project'][$parent_name]->subprojects[$project->machine_name] = $project;
          }
        }
        // Redirects.
        drupal_set_message(t('The project has been saved.'));
        if ($form_state['values']['op'] == t('Save and continue')) {
          $form_state['redirect'] = 'admin/forge/project/' . $project->machine_name . '/edit';
        } else {
          $form_state['redirect'] = 'admin/forge';
        }
      } else {
        drupal_set_message(t('An error ocurred. See more details in !log.', array('!log' => l(t('system log'), 'admin/reports/dblog'))), 'error');
        $form_state['redirect'] = 'admin/forge';
      }
      break;
    case t('Cancel'):
      $form_state['redirect'] = 'admin/forge';
      break;
  }
}

/**
 * Project editing form.
 */
function forge_project_edit($form, &$form_state, $project) {
  if ($project->status == Forge::PROJECT_STATUS_INACTIVE) {
    drupal_access_denied();
    drupal_set_message(t('This project is disabled. To edit it, you must first enable it.'), 'warning');
    drupal_exit();
  }

  drupal_set_title(forge_project_title($project));
  $project = Forge_Project::session($project);

  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();
  forge_add_js();
  drupal_add_js(array('forge' => array('projectTree' => $project->getElementsJson())), 'setting');
  forge_add_css();

  $form = $project->getForm();

  return $form->toDrupal();
}

/**
 * Implements hook_submit().
 */
function forge_project_edit_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Save as revision')) {
    drupal_goto('admin/forge/project/' . $form['#project']->machine_name . '/add-revision', array('query' => drupal_get_destination()));
  }
  $form['#project']->save();
  drupal_set_message(t('The project %project has been saved.', array('%project' => $form['#project']->name)));
}

/**
 * Menu callback: set the status of the project.
 *
 * @param Forge_Project $project
 * @param string $status String representation of the status
 */
function forge_project_set_status($project, $status = 'enable') {
  if ($status == 'enable') {
    $status = Forge::PROJECT_STATUS_ACTIVE;
  } elseif ($status == 'disable') {
    $status = Forge::PROJECT_STATUS_INACTIVE;
  } else {
    throw new Forge_Exception("Could not set the status %status of the project.", array('%status' => $status));
  }
  $project->status = $status;
  $project->save();
  $session = Forge_Project::session($project->machine_name);
  if (is_object($session) && $session instanceof Forge_Project) {
    $session->status = $project->status;
  }
  $message = 'Project !name has been ';
  if ($status == Forge::PROJECT_STATUS_ACTIVE) {
    $message .= 'enabled.';
  } else {
    $message .= 'disabled.';
  }
  drupal_set_message(t($message, array('!name' => $project->name)));
  drupal_goto('admin/forge');
}

/**
 * Project deletion confirm form.
 */
function forge_project_delete_confirm($form, &$form_state, $project) {
  $form['#project'] = $project;
  $form['#submit'][] = 'forge_project_delete_confirm_submit';
  return confirm_form($form,
                    t('Are you sure you want to delete project %project?', array('%project' => $project->name)),
                    'admin/forge', t('This action cannot be undone.'),
                    t('Delete'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function forge_project_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $project_name = $form['#project']->name;
    $form['#project']->delete();
    drupal_set_message(t('The project %project has been deleted.', array('%project' => $project_name)));
  }
  drupal_goto('admin/forge');
}

/**
 * Returns whether a project name already exists.
 *
 * @see forge_project_add()
 * @see form_validate_machine_name()
 * @param  $value The machine name of the project
 * @return boolean
 */
function forge_project_exists($value) {
  global $user;
  $query = db_select('forge_project');
  $query->addExpression('COUNT(*)');
  $count = $query->condition('machine_name', $value)
    ->condition('uid', $user->uid)
    ->execute()
    ->fetchField();

  return $count > 0;
}
