<?php

/**
 * @file
 * Theme functions.
 */

/**
 * Convert PHP array to string presentation.
 *
 * @param $variables
 *   An associative array containing:
 *   - array: PHP array.
 *   - clean: Clean array values.
 *   - nolf:  Render as one-line and ignore keys.
 *
 * @ingroup themeable
 */
function theme_forge_array($variables) {
  $array = $variables['array'];

  if (isset($variables['clean'])) {
    if ($variables['clean']) {
      foreach ($array as &$value) {
        if (!is_array($value) && !is_object($value)) {
          $value = trim($value, " \"'");
        }
        if (is_numeric($value)) {
          $value = (int)$value;
        }
      }
    }
  }

  $output = var_export($array, 1);
  $output = str_replace("array (", "array(", $output);
  $output = preg_replace("/ => \n(\s+)array/", " => array", $output);
  $output = str_replace("'TRUE'", "TRUE", $output);

  if (isset($variables['nolf'])) {
    if ($variables['nolf']) {
      $output = preg_replace('/\n\s*/', ' ', $output);
      $output = preg_replace('/\d+\s+=>\s+/', '', $output);
      $output = str_replace('array( ', 'array(', $output);
      $output = str_replace(', )', ')', $output);
    }
  }

  return $output;
}

/**
 * Returns HTML for a project info.
 *
 * @param $variables
 *   An associative array containing:
 *   - project: Forge_Project object to render.
 *
 * @ingroup themeable
 */
function theme_forge_project_info($variables) {
  $project = $variables['project'];

  $output = '';
  $output .= '<div class="forge-project-name">' . check_plain($project->name) . "</div>\n";
  $info_items = array(
    t('Type: %value', array('%value' => t($project->type))),
    t('Machine name: %value', array('%value' => $project->machine_name))
  );
  if ($project->core) {
    $cores = array();
    foreach ($project->core as $core => $corename) {
      if (($core & $project->core_support) > 0) {
        $cores[] = $corename;
      }
    }
    $info_items[] = t('Core: %value', array('%value' => implode(' ' . t('and') . ' ', $cores)));
  }
  if ($project->package) {
    $info_items[] = t('Package: %value', array('%value' => $project->package));
  }
  if ($project->php) {
    $php = array();
    foreach ($project->php as $core => $phpname) {
      if (($core & $project->core_support) > 0) {
        $php[] = $phpname;
      }
    }
    $info_items[] = t('Php version: %value', array('%value' => implode(' ' . t('and') . ' ', $php)));
  }
  $output .= theme('item_list', array('items' => $info_items));

  return $output;
}

/**
 * Returns HTML for a project submodules.
 *
 * @param $variables
 *   An associative array containing:
 *   - subprojects: List of the subprojects.
 *
 * @ingroup themeable
 */
function theme_forge_project_subprojects($variables) {
  if (!isset($variables['subprojects']) || empty($variables['subprojects'])) {
    return t('Not found subprojects.');
  }
  $items = array();
  foreach ($variables['subprojects'] as $iid => $item) {
    if ($variables['project']->status == Forge::PROJECT_STATUS_ACTIVE) {
      $items[] = l($item->name, 'admin/forge/project/' . $item->machine_name . '/edit');
    } else {
      $items[] = $item->name;
    }
  }
  return theme('item_list', array('items' => $items));
}

/**
 * Returns HTML for a project editing form.
 * @param $variables
 *   An associative array containing:
 *   - form: editing form to render.
 *
 * @ingroup themeable
 */
function theme_forge_project_edit($variables) {
  $form = $variables['form'];
  unset($form['#theme']);

  $output = '';
  $need_save = $variables['form']['#project']->need_save;
  $has_parent = $variables['form']['#project']->parent_pid > 0;
  // Messages
  $message_class = 'messages warning';
  $message = '';
  if ($need_save) {
    $message = t('Your changes will not be saved until you click the "Save" or "Save as revision".');
  } else {
    $message_class .= ' js-hide';
  }
  $output .= '<div id="forge-messages" class="' . $message_class . '">' . $message . '</div>';
  // Head section
  $output .= '<div id="forge-actions" class="form-actions' . ($need_save ? '': ' js-hide') . '">' . drupal_render($form['actions']) . '</div>';
  $output .= '<div class="forge-head-list">';
  $output .= theme('links__ctools_dropbutton', array('links' => $form['#operations'], 'attributes' => array('class' => array('links', 'inline'))));
  $head_list = array();
  $master = TRUE;
  foreach ($form['#head_list'] as $machine_name => $name) {
    if ($master) {
      $name = '<strong>' . $name . '</strong>';
      $master = FALSE;
    }
    $head_list[] = l($name, 'admin/forge/project/' . $machine_name . '/edit', array('html' => TRUE));
  }
  if (!$has_parent) {
    $head_list[] = l(t('Add subproject'), 'admin/forge/project/' . $form['#project']->machine_name . '/add-subproject', array('attributes' => array('class' => array('add-subproject'))));
  }
  $output .= theme('item_list', array('items' => $head_list));
  $output .= '</div>';
  // Search area
  $output .= '<div class="forge-search" id="forge-search">';
  $output .= drupal_render($form['search']);
  $output .= '<div class="forge-autocomplete" id="forge-autocomplete"></div>';
  $output .= '</div>';
  // Groups section
  $output .= '<div class="forge-groups" id="forge-groups">';
  $output .= drupal_render($form['groups']);
  $output .= '</div>';
  $output .= drupal_render($form);

  return $output;
}

/**
 * Returns HTML for a project build view.
 *
 * @param $variables
 *   An associative array containing:
 *   - project: Forge_Project object instance.
 *
 * @ingroup themeable
 */
function theme_forge_project_build($variables) {
  $project = $variables['project'];
  $output = '';

  // Head section.
  $operations = array();
  $operations[] = array(
    'title' => t('Rebuild'),
    'href' => "admin/forge/project/{$project->machine_name}/build",
  );
  $operations[] = array(
    'title' => t('Download'),
    'href' => "admin/forge/project/{$project->machine_name}/build-download",
  );
  $output .= '<div class="forge-build-head">';
  $output .= theme('links', array('links' => $operations));
  $output .= '</div>';

  $output .= '<div class="forge-build-container clearfix">';
  // Tree section.
  $tree = $project->getFilesTree();
  $output .= '<div id="forge-build-tree">';
  $output .= theme('forge_project_build_tree', array('machine_name' => $project->machine_name, 'tree' => $tree, 'id' => 'ft-0'));
  $output .= '</div>';

  // Preview section.
  $output .= '<div id="forge-build-preview">';
  $output .= t('Expand the tree and select the desired file to view.');
  $output .= '</div>';

  $output .= '</div>';

  return $output;
}

/**
 * Returns HTML for a project build tree.
 *
 * @param $variables
 *   An associative array containing:
 *   - machine_name: Machine name of the project.
 *   - tree:         Array of the project tree.
 *   - id:           UL-container id.
 *
 * @ingroup themeable
 */
function theme_forge_project_build_tree($variables) {
  $machine_name = $variables['machine_name'];
  $tree = $variables['tree'];
  $id = $variables['id'];
  $output = '';

  $output = '<ul id="' . $id . '">';
  foreach ($tree as $name => $children) {
    if (is_array($children)) {
      $output .= '<li>';
      $id = 'ft-' . md5($name . rand(0, 999));
      $output .= l($name, '', array('attributes' => array('class' => array('forge-dir', 'forge-toggle'), 'data-id' => $id)));
      $output .= theme('forge_project_build_tree', array('machine_name' => $machine_name, 'tree' => $children, 'id' => $id));
      $output .= '</li>';
    }
  }
  foreach ($tree as $name => $path) {
    if (!is_array($path)) {
      $output .= '<li>';
      $output .= l(
        $name,
        "admin/forge/project/{$machine_name}/build-view",
        array(
          'query' => array('file' => $path),
          'attributes' => array(
            'id' => 'ff-' . md5($path),
            'class' => array('forge-file', 'use-ajax'),
          )
        )
      );
      $output .= '</li>';
    }
  }
  $output .= '</ul>';

  return $output;
}

/**
 * Returns HTML for element title.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element object to render.
 *   - upper: Display title in uppercase.
 *   - hook: Display title as hook.
 *
 * @ingroup themeable
 */
function theme_forge_element_title($variables) {
  $element = $variables['element'];
  $upper = isset($variables['upper']) ? $variables['upper'] : FALSE;
  $hook = isset($variables['hook']) ? $variables['hook'] : FALSE;
  $title = $element->title;

  $hook_name = '';
  if ($upper && preg_match('/^(.*)\s+\((.*)\)$/', $title, $m)) {
    $title = $m[1];
    $hook_name = $m[2];
  } elseif ($upper && $hook) {
    $hook_name = $element->machine_name;
  }

  if ($upper || $hook_name) {
    $title = '<strong>' . drupal_strtoupper($title) . ($hook_name ? ' (' . $hook_name . ')' : '') . '</strong>';
  } elseif ($hook) {
    $title .= ' (' . $element->machine_name . ')';
  }

  $output = '<span class="forge-element-title" data-id="#' . $element->getHtmlId() . '">' . $title . '</span>';

  return $output;
}

/**
 * Returns HTML for element core support information.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element object to render.
 *   - label: Display a label.
 *   - title_key: Key to choose a displayed title.
 *
 * @ingroup themeable
 */
function theme_forge_element_core_support($variables) {
  $element = $variables['element'];
  $label = isset($variables['label']) ? $variables['label'] : FALSE;
  $title_key = isset($variables['title_key']) ? $variables['title_key'] : 'icon';

  $output = '<div class="forge-element-core-support">';
  if ($label) {
    $output .= t('Links') . ': ';
  }
  $cores = array(
    Forge::CORE_SUPPORT_6x => array('url_chunk' => '6', 'core' => '6.x', 'class' => '6x', 'icon' => 'D6', 'title' => 'Drupal 6 API'),
    Forge::CORE_SUPPORT_7x => array('url_chunk' => '7', 'core' => '7.x', 'class' => '7x', 'icon' => 'D7', 'title' => 'Drupal 7 API'),
  );
  foreach ($cores as $core => $titles) {
    if ($element->core_support & $core) {
      $api_url = 'https://api.drupal.org/api/drupal/' . $titles['url_chunk'];
      if (isset($element->info)) {
        if (isset($element->info['apiUrl'])) {
          if (isset($element->info['apiUrl'][$core]) && !empty($element->info['apiUrl'][$core])) {
            $api_url = $element->info['apiUrl'][$core];
          }
        }
      }
      $output .= l($titles[$title_key], $api_url, array('external' => TRUE, 'attributes' => array('title' => t('Drupal !core support.', array('!core' => $titles['core'])), 'class' => array('core-support', 'core-support-' . $titles['class']), 'target' => '_blank')));
    } else {
      $output .= '<span class="core-support core-support-' . $titles['class'] . ' no-support">' . $titles[$title_key] . '</span>';
    }
  }
  $output .= '</div>';

  return $output;
}

/**
 * Returns HTML for element description.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_description($variables) {
  $element = $variables['element'];
  $output = '';
  $output = '<div class="forge-element-description"><div class="tooltip-arrow"></div>';
  $description = t('No description available.');
  if (isset($element->info)) {
    if (isset($element->info['description'])) {
      $description = t($element->info['description']);
    }
  }
  $output .= $description;
  $variables['label'] = TRUE;
  $variables['title_key'] = 'title';
  $output .= theme('forge_element_core_support', $variables);
  $output .= '</div>';

  return $output;
}

/**
 * Returns HTML for element presentation.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present($variables) {
  $header = $rows = array();

  $info_links = $variables['element']->getLinks();
  $info_links = theme('links__ctools_dropbutton', array('links' => $info_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables), 'class' => 'forge-element-title', 'width' => '20%'),
    array('data' => $variables['element']->value ? $variables['element']->value : ('<em>' . t('no value') . '</em>'), 'class' => 'forge-element-value', 'width' => '50%'),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $info_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  $class = array('forge-element');
  if (empty($variables['element']->value)) {
    $class[] = 'forge-element-empty';
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Returns HTML for hook element presentation.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook($variables) {
  $header = $rows = array();

  $info_links = $variables['element']->getLinks();
  $info_links = theme('links__ctools_dropbutton', array('links' => $info_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('hook' => TRUE)), 'class' => 'forge-element-name', 'width' => '30%'),
    array('data' => $variables['element']->value == TRUE ? t('used') : t('not used'), 'class' => 'forge-element-value', 'width' => '40%'),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $info_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  $class = array('forge-element', 'forge-element-hook');
  if (empty($variables['element']->value)) {
    $class[] = 'forge-element-empty';
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Returns HTML for boolean element presentation.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Boolean object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_boolean($variables) {
  $header = $rows = array();

  $info_links = $variables['element']->getLinks();
  $info_links = theme('links__ctools_dropbutton', array('links' => $info_links, 'attributes' => array('class' => array('links', 'inline'))));
  $empty = is_null($variables['element']->value) || $variables['element']->value == 'NULL';
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables), 'class' => 'forge-element-title', 'width' => '20%'),
    array('data' => $empty ? '<em>' . t('no value') . '</em>' : $variables['element']->value, 'class' => 'forge-element-value', 'width' => '50%'),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $info_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  $class = array('forge-element', 'forge-element-boolean');
  if ($empty) {
    $class[] = 'forge-element-empty';
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Returns HTML for presentation of element with multiple values.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Multiple object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_multiple($variables) {
  $header = $rows = array();

  $header_links = $info_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE)), 'class' => array('forge-element-title', 'forge-element-info-multiple-name'), 'width' => '70%'),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  if ($variables['display_keys']) {
    $rows[0][0]['colspan'] = 2;
  }
  if (is_array($variables['element']->value)) {
    foreach ($variables['element']->value as $key => $value) {
      $value_links = $variables['element']->getSecondaryLinks(array('key' => $key));
      $value_links = theme('links__ctools_dropbutton', array('links' => $value_links, 'attributes' => array('class' => array('links', 'inline'))));
      $row = array();
      if ($variables['display_keys']) {
        $row[] = array('data' => $key, 'width' => '30%');
        $row[] = array('data' => $value, 'width' => '40%', 'colspan' => 2);
      } else {
        $row[] = array('data' => $value, 'colspan' => 2);
      }
      $row[] = array('data' => $value_links, 'width' => '20%', 'class' => array('forge-operations'));
      $rows[] = array(
        'data' => $row,
        'class' => array('forge-element-info-multiple-value'),
      );
    }
  }
  $class = array('forge-element', 'forge-element-multiple');
  if (count($rows) == 2) {
    $class[] = 'forge-element-empty';
    $rows[] = array(
      array('data' => t('no rows found'), 'colspan' => $variables['display_keys'] ? 4 : 3),
    );
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Render element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Info object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_render($variables) {
  $value = isset($variables['value']) ? $variables['value'] : $variables['element']->value;
  return $variables['element']->machine_name . ' = ' . $value;
}

/**
 * Render element with multiple values.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Info_Multiple object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_multiple($variables) {
  $render = array();
  foreach ($variables['element']->value as $key => $value) {
    $render[] = $variables['element']->machine_name . '[' . (isset($variables['display_keys']) ? $key : '') . '] = ' . $value;
  }

  return implode("\n", $render);
}

/**
 * Render hook element prefix.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constant.
 *   - custom:  Prefix value independent of the core value.
 *              Used if 'core' variable is NULL.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_prefix($variables) {
  $machine_name = $variables['element']->machine_name;
  if (isset($variables['custom'])) {
    $custom = $variables['custom'];
    $variables['core'] = NULL;
  }
  switch ($variables['core']) {
    case Forge::CORE_SUPPORT_6x:
      return <<<EOL
/**
 * Implementation of {$machine_name}().
 */
EOL;
    case Forge::CORE_SUPPORT_7x:
      return <<<EOL
/**
 * Implements {$machine_name}().
 */
EOL;
    case NULL:
      return <<<EOL
/**
 * {$custom}.
 */
EOL;
  }
  return NULL;
}

/**
 * Render hook element suffix.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constant.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_suffix($variables) {
  return "\n";
}

/**
 * Render hook element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constant.
 *   - args:    Hook arguments.
 *   - body:    Hook rendered body.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook($variables) {
  $prefix = $variables['element']->getPrefix($variables['core']);
  $suffix = $variables['element']->getSuffix($variables['core']);
  $render_name = $variables['element']->renderName;
  $args = $variables['args'];
  $body = isset($variables['body']) ? $variables['body'] : '//';
  $body = '  ' . str_replace("\n", "\n  ", $body);
  return <<<EOL
{$prefix}
function {$render_name}({$args}) {
{$body}
}{$suffix}
EOL;
}
