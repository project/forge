<?php

/**
 * @file
 * Admin page callbacks for the Forge module.
 */

/**
 * Form builder; configures site performance settings.
 *
 * @ingroup forms
 */
function forge_settings() {
  $form = array();
  return system_settings_form($form);
}
