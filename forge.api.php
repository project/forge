<?php

/**
 * @file
 * Describes hooks provided by Forge module.
 */

/**
 * Describes groups defined by the module.
 *
 * @return
 *   An associative array describing in key a machine name of group, in value
 *   - array with human readable name and weight.
 */
function hook_forge_group() {
  $group['example'] = array(
    'title' => 'Example section',
    'weight' => 0,
  );
  return $group;
}

/**
 * Describes elements that are used in a group.
 *
 * @return
 *   An associative array having machine name of the group as its key.
 *   The value for each entry is an associative array that may contain the
 *   following items:
 *   - class        (optional): The class name of the element. Defaults to Forge_Element class.
 *   - name         (required): The human readable name (title) of the element.
 *   - defaultValue (optional): Mixed data. A default value of the element.
 *   - projectType  (required): Forge::PROJECT_TYPE_* constants, or a combination thereof.
 *                              Example: Forge::PROJECT_TYPE_MODULE | Forge:PROJECT_TYPE_THEME.
 *   - coreSupport  (required): Forge::CORE_SUPPORT_* constants, or a combination thereof.
 *   - group        (required): Machine name of the group of element.
 *   - options      (optional): Options array. Possible keys: required (boolean) and args (mixed).
 *   - info         (optional): Help text. Possible keys: description (string) and apiUrl (array).
 */
function hook_forge_element() {
  $element['example'] = array(
    'class' => 'Forge_Element',
    'name' => 'Example element',
    'defaultValue' => NULL,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'info',
  );
}

/**
 * Alter existing elements data defined by modules.
 *
 * @see hook_forge_element().
 */
function hook_forge_element_alter(&$data = array()) {}

/**
 * Register a Forge submodules theme implementations.
 *
 * This hook is similar to the hook_theme except its name.
 * @see hook_theme().
 */
function hook_forge_theme($existing, $type, $theme, $path) {}

/**
 * Adds a JS that needed for module.
 */
function hook_forge_module_js() {}
