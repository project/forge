/**
 * @file
 * Project build view functionality.
 */
(function ($) {

Drupal.behaviors.forgeBuildView = {
  attach: function(context) {
    $('#forge-build-tree a.forge-toggle', context).click(function(){
      var ftc = $(this).data('id');
      if ($(this).hasClass('open')) {
        $('#' + ftc).slideUp('fast');
        $(this).removeClass('open')
      } else {
        $('#' + ftc).slideDown('fast');
        $(this).addClass('open')
      }
      return false;
    });

    $('#forge-build-tree a.forge-file').click(function(){
      $('#forge-build-tree a.forge-file').removeClass('open');
    });
  }
};

})(jQuery);
