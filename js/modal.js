/**
 * @file
 * Handles CTools Modal Window features.
 */
(function ($) {

Drupal.behaviors.forgeModal = {
  attach: function(context, settings) {
    var needAutofocus = typeof(context) == 'string',
        modalId = '.ctools-modal-content #element-wrapper',
        modal = $(modalId);
    if (modal.length) {
      // Popup window height.
      $('.ctools-modal-content #modal-content').css({'max-height': (modal.height() + 75) + 'px'});
      $('#modalContent').css('position', 'fixed');
      var winHeight = $(window).height();
      if (modal.height() + 50 > winHeight) {
        // Resize trigger.
        $(window).resize();
      } else {
        var wrapper_height = 10;
        if ($.browser.mozilla) {
          wrapper_height -= 10;
        }
        $('.ctools-modal-content #modal-content').css({'height': 'auto'});
        $('.ctools-modal-content #element-wrapper').css({
          'height': (modal.height() + wrapper_height) + 'px',
          'max-height': '95%',
          'overflow-y': 'auto'
        });
        // Centered vertical position.
        var modalContent = $('#modalContent');
        var mdcTop = (winHeight / 2) - (modalContent.outerHeight() / 2);
        modalContent.css({top: mdcTop + 'px'});
      }

      // Autofocus first form element.
      if (needAutofocus) {
        var focusExists = false;
        var selector = modalId + ' input' + ',' + modalId + ' select';
        $(selector).each(function(){
          if ($(this).is(':focus')) {
            focusExists = true;
          }
        });
        if (!focusExists) {
          $(selector).first().focus();
        }
      }
    }

    // Disable AJAX form submit if autocomplete field in focus.
    $('#forge-element-form input[type=submit], #forge-element-form button', context).click(function(event){
      var ac_focused = false,
          base = 'forge-element-form';
      $('#forge-element-form .form-autocomplete').each(function(){
        if ($(this).is(':focus')) {
          ac_focused = true;
        }
      });
      if (ac_focused) {
        return false;
      }

      // Copied from ctools/modal.js
      Drupal.ajax[base].element = this;
      this.form.clk = this;
      // An empty event means we were triggered via .click() and
      // in jquery 1.4 this won't trigger a submit.
      if (event.bubbles == undefined) {
        $(this.form).trigger('submit');
        return false;
      }
    });
  }
};

})(jQuery);
