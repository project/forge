/**
 * @file
 * Common JavaScript handler.
 */
(function ($) {

Drupal.behaviors.forge = {
  attach: function(context) {
    $('span.forge-element-title', context).click(function(){
      var container = $(this).data('id');
      var description = container + ' ' + 'tr.forge-element-description';
      if ($(description).css('display') == 'none') {
        $(description).show();
        $(this).addClass('expand');
        $(this).closest('tr').children('td').css('border-bottom', 'none');
      } else {
        $(description).hide();
        $(this).removeClass('expand');
        if (!$(container).hasClass('forge-element-multiple')) {
          $(this).closest('tr').children('td').css('border-bottom', '1px solid #bebfb9');
        }
      }
    });
  }
};

})(jQuery);
