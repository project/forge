/**
 * @file
 * Search feature.
 */
(function ($) {

Drupal.behaviors.forgeSearch = {
  attach: function(context) {
    var input = $('#forge-search #edit-search', context);
    input.focus();
    // Init search engine.
    input.once('forge-search', function(){
      $('#forge-autocomplete').empty().hide();
      new Drupal.forgeSearch(input);
    });
    // Keyboard shortcut support.
    $('body').keyup(function(event){
      if (!event) {
        event = window.event;
      }
      var popup = $('.ctools-modal-content');
      if (event.keyCode == 83 && !popup.length) {
        if (input.is(':focus')) {
          return true;
        }
        input.focus();
        $('html, body').animate({scrollTop: 0}, 5);
        return false;
      }
      return true;
    });
  }
};

/**
 * ForgeSearch object.
 */
Drupal.forgeSearch = function(input) {
  var fs = this;
  this.input = input;
  this.current = 0;
  this.to = 0;
  input.keydown(function(event) {
    return fs.onkeydown(this, event);
  }).keyup(function(event) {
    return fs.onkeyup(this, event);
  }).blur(function() {
    fs.clear();
  });
}

/**
 * Handler for the "keydown" event.
 */
Drupal.forgeSearch.prototype.onkeydown = function(input, event) {
  if (!event) {
    event = window.event;
  }
  switch (event.keyCode) {
    case 40: // down arrow.
      this.down();
      return false;
    case 38: // up arrow.
      this.up();
      return false;
    case 13: // Enter.
      this.select('down');
      Drupal.forgeSearch.gotoElement(this.current);
      this.input.blur();
      return false;
    default: // All other keys.
      return true;
  }
};

/**
 * Handler for the "keyup" event.
 */
Drupal.forgeSearch.prototype.onkeyup = function(input, event) {
  if (!event) {
    event = window.event;
  }
  switch (event.keyCode) {
    case 16: // Shift.
    case 17: // Ctrl.
    case 18: // Alt.
    case 20: // Caps lock.
    case 33: // Page up.
    case 34: // Page down.
    case 35: // End.
    case 36: // Home.
    case 37: // Left arrow.
    case 38: // Up arrow.
    case 39: // Right arrow.
    case 40: // Down arrow.
      return true;

    case 9:  // Tab.
    case 13: // Enter.
    case 27: // Esc.
      this.clear();
      return true;

    default: // All other keys.
      if (input.value.length > 0 && !input.readOnly) {
        clearTimeout(this.to);
        this.to = setTimeout(to_callback, 150);
      }
      else {
        this.clear();
      }
      return true;
  }
};

/**
 * Highlights the next/prev suggestion.
 */
Drupal.forgeSearch.prototype.select = function(direction) {
  this.current = $('#forge-autocomplete li.selected');
  if (!this.current.length) {
    var suggestions = $('#forge-autocomplete li');
    if (direction == 'down') {
      this.current = suggestions.first().addClass('selected');
    } else {
      this.current = suggestions.last().addClass('selected');
    }
    return false;
  }
  return true;
};

/**
 * Highlights the next suggestion.
 */
Drupal.forgeSearch.prototype.down = function() {
  if (this.select('down')) {
    index = this.current.data('index');
    if (index<14) {
      index = index + 1;
    }
    this.current.removeClass('selected');
    $('#forge-autocomplete li[data-index="' + index + '"]').addClass('selected');
  }
};

/**
 * Highlights the previous suggestion.
 */
Drupal.forgeSearch.prototype.up = function() {
  if (this.select('up')) {
    index = this.current.data('index');
    if (index>0) {
      index = index - 1;
    }
    this.current.removeClass('selected');
    $('#forge-autocomplete li[data-index="' + index + '"]').addClass('selected');
  }
};

/**
 * Hides the autocomplete suggestions.
 */
Drupal.forgeSearch.prototype.clear = function() {
  this.input.val('');
  $('#forge-autocomplete').empty().hide();
};

/**
 * Hides the autocomplete suggestions.
 */
Drupal.forgeSearch.gotoElement = function(element) {
  var group = element.data('group'),
      name  = element.data('name'),
      selector = '#' + group + '-' + name,
      vt = $('#forge-group-' + group).data('verticalTab');
  vt.link.click();
  $('html, body').animate({
    scrollTop: parseInt($(selector).offset().top - 50)
  }, 5);
  $(selector).addClass('search-suggestion');
};

/**
 * Render and display search suggestions.
 */
Drupal.forgeSearch.render = function(input, elements) {
  var ac = $('#forge-autocomplete'),
      ul = $('<ul></ul>');
  counter = 0;
  for (elem in elements) {
    var item = elements[elem];
    var li = '<li data-index="' + counter + '" data-group="' + item.gid + '" data-name="' + item.element + '">';
    li = li + '<span class="element">' + item.group + '/' + item.element + '</span>';
    li = li + '<span class="description">' + item.description + '</span>';
    li = li + '</li>';
    $(li).mousedown(function(){var elem=$(this);Drupal.forgeSearch.gotoElement(elem);})
      .mouseover(function(){$(this).addClass('selected');})
      .mouseout(function(){$(this).removeClass('selected');})
      .appendTo(ul);
    counter++;
  }
  if (counter) {
    ac.empty().append(ul).show();
    ac.width(input.width() + 12);
  } else {
    ac.hide();
  }
}

/**
 * Timeout callback function.
 */
var to_callback = function() {
  $('.forge-element.search-suggestion').removeClass('search-suggestion');
  var input = $('#forge-search #edit-search'),
      tree = Drupal.settings.forge.projectTree,
      result = [],
      counter = 0;
  for (gid in tree) {
    for (elem in tree[gid].elements) {
      if (elem.indexOf(input.val())!=-1 && counter<15) {
        group = tree[gid];
        element = group.elements[elem];
        result[elem] = {
          gid: gid,
          group: group.title,
          element: elem,
          title: element.title,
          description: element.description || element.title,
        };
        counter++;
      }
    }
  }
  Drupal.forgeSearch.render(input, result);
}

})(jQuery);
