<?php

/**
 * @file
 * Provide Forge hooks for block.module.
 */

/**
 * Implements hook_forge_group().
 */
function block_forge_group() {
  $group['block'] = $group['theme_block'] = array(
    'title' => 'Block'
  );
  return $group;
}

/**
 * Implements hook_forge_element().
 */
function block_forge_element() {
  // Module support.
  $element['hook_block_info'] = array(
    'class' => 'Forge_Element_Hook_Block',
    'name' => 'Blocks',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'block',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '$op = \'list\', $delta = 0, $edit = array()',
        Forge::CORE_SUPPORT_7x => '',
      ),
    ),
    'info' => array(
      'description' => 'Define all blocks provided by the module.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_block/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_info/7',
      ),
    ),
  );

  $element['hook_block_info_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Blocks info alter',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'block',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$blocks, $theme, $code_blocks',
      ),
    ),
    'info' => array(
      'description' => 'Change block definition before saving to the database.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_info_alter/7',
      ),
    ),
  );

  $element['hook_block_configure'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Blocks configuration form',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'block',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$delta = \'\'',
      ),
    ),
    'info' => array(
      'description' => 'Define a configuration form for a block.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_configure/7',
      ),
    ),
  );

  $element['hook_block_save'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Save block configuration',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'block',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$delta = \'\', $edit = array()',
      ),
    ),
    'info' => array(
      'description' => 'Save the configuration options from hook_block_configure().',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_save/7',
      ),
    ),
  );

  $element['hook_block_view'] = array(
    'class' => 'Forge_Element_Hook_Block_View',
    'name' => 'Render block',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'block',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$delta = \'\'',
      ),
    ),
    'info' => array(
      'description' => 'Return a rendered or renderable view of a block.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_view/7',
      ),
    ),
  );

  $element['hook_block_view_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Alter block view',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'block',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$data, $block',
      ),
    ),
    'info' => array(
      'description' => 'Perform alterations to the content of a block.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_view_alter/7',
      ),
    ),
  );

  // Theme support
  $element['template_preprocess_block'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Preprocess block.tpl.php',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'theme_maintenance',
    'options' => array(
      'args' => '&$variables',
    ),
    'info' => array(
      'description' => 'Processes variables for block.tpl.php.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21block%21block.module/function/template_preprocess_block/7',
      ),
    ),
  );

  $element['block.tpl.php'] = array(
    'class' => 'Forge_Element_Template',
    'name' => 'Html structure of a block',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme',
    'options' => array(
      'args' => 'modules/block/block.tpl.php',
    ),
    'info' => array(
      'description' => 'Default theme implementation to display a block.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21block%21block.tpl.php/7',
      ),
    ),
  );

  return $element;
}

/**
 * Implements hook_forge_theme().
 */
function block_forge_theme($existing, $type, $theme, $path) {
  return array(
    'forge_element_present_hook_block' => array(
      'variables' => array('element' => NULL),
    ),
    'forge_element_render_hook_block' => array(
      'variables' => array('element' => NULL),
    ),
    'forge_element_render_hook_block_view' => array(
      'variables' => array('element' => NULL),
    ),
  );
}
