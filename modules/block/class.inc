<?php

/**
 * @file
 * Class implementation of the block hook elements.
 */

/**
 * Block hook class.
 *
 * @see Forge_Element_Hook
 * @see https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_info/7
 * @see https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_block/6
 */
class Forge_Element_Hook_Block extends Forge_Element_Hook {
  public $presentTheme = 'forge_element_present_hook_block';
  public $renderTheme = 'forge_element_render_hook_block';

  /**
   * Returns primary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getPrimaryLinks($args = array()) {
    $links = array(
      forge_popup_link('add block', $this, 'add'),
      forge_link('clear', $this, 'clear'),
    );

    return $links;
  }

  /**
   * Return secondary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getSecondaryLinks($args = array()) {
    $links = array(
      forge_popup_link('edit', $this, 'edit', array('delta' => $args['delta'])),
      forge_link('delete', $this, 'delete', array('delta' => $args['delta'])),
    );

    return $links;
  }

  /**
   * Deletes a value specified by 'delta' query parameter.
   *
   * @ingroup operations
   */
  public function delete() {
    $query = drupal_get_query_parameters();
    if (isset($query['delta'])) {
      unset($this->value[$query['delta']]);
    }
  }

  /**
   * Returns Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    if (isset($query['delta'])) {
      if (isset($this->value[$query['delta']])) {
        $input = $this->value[$query['delta']];
      }
    }
    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['delta'] = array(
      '#type' => 'textfield',
      '#title' => t('Delta'),
      '#default_value' => isset($input['delta']) ? $input['delta'] : '',
      '#required' => TRUE,
    );
    $form['info'] = array(
      '#type' => 'textfield',
      '#title' => t('Administrative name'),
      '#default_value' => isset($input['info']) ? $input['info'] : '',
      '#description' => t('This is used to identify the block on administration screens, and is not displayed to non-administrative users.'),
      '#required' => TRUE,
    );
    $form['cache'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Cache'),
      '#options' => array(
        DRUPAL_CACHE_PER_ROLE => 'DRUPAL_CACHE_PER_ROLE',
        DRUPAL_CACHE_PER_USER => 'DRUPAL_CACHE_PER_USER',
        DRUPAL_CACHE_PER_PAGE => 'DRUPAL_CACHE_PER_PAGE',
        DRUPAL_CACHE_GLOBAL => 'DRUPAL_CACHE_GLOBAL',
        DRUPAL_CACHE_CUSTOM => 'DRUPAL_CACHE_CUSTOM',
        DRUPAL_NO_CACHE => 'DRUPAL_NO_CACHE',
      ),
      '#default_value' => isset($input['cache']) ? $input['cache'] : array(),
    );
    $form['region'] = array(
      '#type' => 'textfield',
      '#title' => t('Region'),
      '#default_value' => isset($input['region']) ? $input['region'] : '',
    );
    $form['status'] = array(
      '#type' => 'checkbox',
      '#title' => t('Status'),
      '#default_value' => isset($input['status']) ? $input['status'] : TRUE,
    );
    $form['visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Cache'),
      '#options' => array(
        BLOCK_VISIBILITY_NOTLISTED => 'BLOCK_VISIBILITY_NOTLISTED',
        BLOCK_VISIBILITY_LISTED => 'BLOCK_VISIBILITY_LISTED',
        BLOCK_VISIBILITY_PHP => 'BLOCK_VISIBILITY_PHP',
      ),
      '#default_value' => isset($input['visibility']) ? $input['visibility'] : BLOCK_VISIBILITY_NOTLISTED,
    );
    $form['pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => isset($input['region']) ? $input['region'] : '',
      '#description' => t('A string that contains one or more page paths separated by "\n", "\r", or "\r\n".'),
    );
    $form['administrative'] = array(
      '#type' => 'checkbox',
      '#title' => t('Administrative'),
      '#default_value' => isset($input['administrative']) ? $input['administrative'] : FALSE,
      '#description' => t('Boolean that categorizes this block as usable in an administrative context.'),
    );
    $form['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#default_value' => isset($input['weight']) ? $input['weight'] : 0,
    );

    return $form;
  }

  /**
   * Submit-processing of element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['delta'])) {
      unset($this->value[$query['delta']]);
    }
    $this->value[$input['delta']] = $input;
  }

  /**
   * Directly get blocks from a session storage.
   *
   * @param  string $project    Project machine name.
   * @return array
   */
  public static function sessionGet($project) {
    $project = Forge_Project::session($project);
    if (isset($project->data['hook_block_info'])) {
      return $project->data['hook_block_info'];
    }
    return array();
  }

  /**
   * Renders the hook_block_info element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      if ($core == Forge::CORE_SUPPORT_6x) {
        $this->machine_name = 'hook_block';
        $this->renderName = $this->project . '_block';
      }
      parent::render($output, $core, $args);
      $this->machine_name = 'hook_block_info';
      $this->renderName = $this->project . '_block_info';
    }
  }
}

/**
 * Block view hook class.
 *
 * @see Forge_Element_Hook
 * @see https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_view/7
 */
class Forge_Element_Hook_Block_View extends Forge_Element_Hook {
  public $renderTheme = 'forge_element_render_hook_block_view';
}
