<?php

/**
 * Present hook_block_info element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constants.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_block($variables) {
  $header = $rows = array();

  $class = array('forge-element', 'forge-element-hook-block');
  $header_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE, 'hook' => TRUE)), 'width' => '70%', 'colspan' => 2),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  if (empty($variables['element']->value)) {
    $class[] = 'forge-element-empty';
    $rows[] = array(
      array('data' => '<em>' . t('no blocks found') . '</em>', 'colspan' => 4),
    );
  } else {
    $rows[] = array(
      array('data' => '<strong>' . t('Name') . '</strong>', 'width' => '30%'),
      array('data' => '<strong>' . t('Additional') . '</strong>', 'width' => '50%', 'colspan' => 2),
      array('data' => '', 'width' => '20%'),
    );
    foreach ($variables['element']->value as $key => $block) {
      $row = array();
      $row[] = $block['info'] . '<div class="description">' . $block['delta'] . '</div>';

      $items = array();
      $cache_array = array(
        DRUPAL_CACHE_PER_ROLE => 'DRUPAL_CACHE_PER_ROLE',
        DRUPAL_CACHE_PER_USER => 'DRUPAL_CACHE_PER_USER',
        DRUPAL_CACHE_PER_PAGE => 'DRUPAL_CACHE_PER_PAGE',
        DRUPAL_CACHE_GLOBAL => 'DRUPAL_CACHE_GLOBAL',
        DRUPAL_CACHE_CUSTOM => 'DRUPAL_CACHE_CUSTOM',
        DRUPAL_NO_CACHE => 'DRUPAL_NO_CACHE',
      );
      $cache = array();
      foreach ($cache_array as $const => $name) {
        if ($block['cache'][$const] != 0) {
          $cache[] = $name;
        }
      }
      if ($cache) {
        $items[] = t('Cache: %cache', array('%cache' => implode(' | ', $cache)));
      }

      $status = t('enabled');
      if (!$block['status']) {
        $status = t('disabled');
      }
      $items[] = t('Status: %status', array('%status' => $status));

      if ($block['region']) {
        $items[] = t('Region: %region', array('%region' => $block['region']));
      }

      $visible_array = array(
        BLOCK_VISIBILITY_NOTLISTED => 'BLOCK_VISIBILITY_NOTLISTED',
        BLOCK_VISIBILITY_LISTED => 'BLOCK_VISIBILITY_LISTED',
        BLOCK_VISIBILITY_PHP => 'BLOCK_VISIBILITY_PHP',
      );
      $visibility = current($visible_array);
      if ($block['visibility']) {
        $visibility = $visible_array[$block['visibility']];
      }
      $items[] = t('Visibility: %visibility', array('%visibility' => $visibility));

      if ($block['weight']) {
        $items[] = t('Weight: %weight', array('%weight' => $block['weight']));
      }

      $row[] = array('data' => theme('item_list', array('items' => $items)), 'colspan' => 2);

      // operations
      $row_links = $variables['element']->getSecondaryLinks(array('delta' => $block['delta']));
      $row[] = array('data' => theme('links__ctools_dropbutton', array('links' => $row_links, 'attributes' => array('class' => array('links', 'inline')))), 'class' => array('forge-operations'));

      $rows[] = $row;
    }
  }

  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));
}

/**
 * Render hook_block_info element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Block object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constants.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_block($variables) {
  $body = array('$block = array();');
  $blocks = $variables['element']->value;
  foreach ($blocks as $raw) {
    $block = array();
    $block['info'] = $raw['info'];

    $cache_array = array(
      DRUPAL_CACHE_PER_ROLE => 'DRUPAL_CACHE_PER_ROLE',
      DRUPAL_CACHE_PER_USER => 'DRUPAL_CACHE_PER_USER',
      DRUPAL_CACHE_PER_PAGE => 'DRUPAL_CACHE_PER_PAGE',
      DRUPAL_CACHE_GLOBAL => 'DRUPAL_CACHE_GLOBAL',
      DRUPAL_CACHE_CUSTOM => 'DRUPAL_CACHE_CUSTOM',
      DRUPAL_NO_CACHE => 'DRUPAL_NO_CACHE',
    );
    if ($variables['core'] == Forge::CORE_SUPPORT_6x) {
      // Drupal 6 does not know this constant.
      unset($cache_array[DRUPAL_CACHE_CUSTOM]);
    }
    $cache = array();
    foreach ($cache_array as $const => $name) {
      if ($raw['cache'][$const] != 0) {
        $cache[] = $name;
      }
    }
    if ($cache) {
      $block['cache'] = implode(' | ', $cache);
    }

    if ($raw['administrative']) {
      $block['properties'] = array(
        'administrative' => TRUE,
      );
    }

    $block['weight'] = $raw['weight'];
    if ($raw['status']) {
      $block['status'] = '1';
    }
    if ($raw['region']) {
      $block['region'] = $raw['region'];
    }

    $visible_array = array(
      BLOCK_VISIBILITY_NOTLISTED => 'BLOCK_VISIBILITY_NOTLISTED',
      BLOCK_VISIBILITY_LISTED => 'BLOCK_VISIBILITY_LISTED',
      BLOCK_VISIBILITY_PHP => 'BLOCK_VISIBILITY_PHP',
    );
    if ($raw['visibility']) {
      // Drupal 6 does not use constants for visibility.
      $block['visibility'] = $variables['core'] == Forge::CORE_SUPPORT_6x ? $raw['visibility'] : $visible_array[$raw['visibility']];
    }
    if ($raw['pages']) {
      $block['pages'] = $raw['pages'];
    }

    $block = theme('forge_array', array('array' => $block));
    $body[] = '$block[\'' . $raw['delta'] . '\'] = ' . $block . ';';
  }
  $body[] = '';
  $body[] = 'return $block;';

  // Drupal 6 does everything in one hook_block().
  if ($variables['core'] == Forge::CORE_SUPPORT_6x) {
    foreach ($body as &$row) {
      $row = '  ' . $row;
    }
    array_unshift($body, 'if ($op == \'list\') {');
    $body[] = '}';
    $body[] = 'if ($op == \'view\') {';
    $body[] = '  switch($delta) {';
    foreach ($blocks as $block) {
      $body[] = '    case \'' . $block['delta'] . '\':';
      $body[] = '      $block[\'subject\'] = \'' . $block['info'] . '\';';
      $body[] = '      $block[\'content\'] = \'\';';
      $body[] = '      return $block;';
    }
    $body[] = '  }';
    $body[] = '}';
  }

  $body = implode("\n", $body);
  $body = preg_replace("/'(array\(.*\))'/", '$1', $body);
  $body = str_replace("\'", "'", $body);
  $body = preg_replace("/'(DRUPAL_.*|BLOCK_.*)'/", '$1', $body);

  $variables['body'] = $body;

  return theme('forge_element_render_hook', $variables);
}

/**
 * Render hook_block_view element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Block object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constants.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_block_view($variables) {
  $body = array('$block = array();');
  $blocks = Forge_Element_Hook_Block::sessionGet($variables['element']->project);

  $body[] = 'switch($delta) {';
  foreach ($blocks as $block) {
    $body[] = '  case \'' . $block['delta'] . '\':';
    $body[] = '    $block[\'subject\'] = \'' . $block['info'] . '\';';
    $body[] = '    $block[\'content\'] = \'\';';
    $body[] = '    return $block;';
  }
  $body[] = '}';

  $body = implode("\n", $body);
  $body = preg_replace("/'(array\(.*\))'/", '$1', $body);

  $variables['body'] = $body;

  return theme('forge_element_render_hook', $variables);
}
