<?php

/**
 * @file
 * Provide Forge hooks for menu.module.
 */

/**
 * Implements hook_forge_group().
 */
function menu_forge_group() {
  $group['menu'] = $group['theme_menu'] = array(
    'title' => 'Menu'
  );
  return $group;
}

/**
 * Implements hook_forge_element().
 */
function menu_forge_element() {
  // Module support.
  $element['hook_menu'] = array(
    'class' => 'Forge_Element_Hook_Menu',
    'name' => 'Menu items and page callbacks',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'menu',
    'info' => array(
      'description' => 'Define the menu items and URL callbacks.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_menu/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7',
      ),
    ),
  );
  $element['hook_menu_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Alter menu items',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'menu',
    'options' => array(
      'args' => '&$items',
    ),
    'info' => array(
      'description' => 'Alter the data being saved to the {menu_router} table after hook_menu is invoked.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_menu_alter/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_alter/7',
      ),
    ),
  );
  $element['hook_menu_get_item_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Alter a menu router item',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'menu',
    'options' => array(
      'args' => '&$router_item, $path, $original_map',
    ),
    'info' => array(
      'description' => 'Alter a menu router item right after it has been retrieved from the database or cache.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_get_item_alter/7',
      ),
    ),
  );
  $element['hook_menu_breadcrumb_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Alter menu breadcrumb',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'menu',
    'options' => array(
      'args' => '&$active_trail, $item',
    ),
    'info' => array(
      'description' => 'Alter links in the active trail before it is rendered as the breadcrumb.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_breadcrumb_alter/7',
      ),
    ),
  );
  $element['hook_menu_contextual_links_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Alter contextual links',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'menu',
    'options' => array(
      'args' => '&$links, $router_item, $root_path',
    ),
    'info' => array(
      'description' => 'Alter contextual links before they are rendered.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_contextual_links_alter/7',
      ),
    ),
  );
  $element['hook_menu_local_tasks_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Alter tabs and actions',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'menu',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$data, $router_item, $root_path',
        Forge::CORE_SUPPORT_8x => '&$data, $router_item',
      ),
    ),
    'info' => array(
      'description' => 'Alter tabs and actions displayed on the page before they are rendered.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_local_tasks_alter/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_menu_local_tasks_alter/8',
      ),
    ),
  );
  $element['hook_menu_site_status_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Control site status',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'menu',
    'options' => array(
      'args' => '&$menu_site_status, $path',
    ),
    'info' => array(
      'description' => 'Control site status before menu dispatching.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_site_status_alter/7',
      ),
    ),
  );

  $element['hook_menu_link_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Menu link alter',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'menu',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '&$item, $menu',
        Forge::CORE_SUPPORT_7x => '&$item',
      ),
    ),
    'info' => array(
      'description' => 'Alter the data being saved to the {menu_links} table by menu_link_save().',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_menu_link_alter/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_link_alter/7',
      ),
    ),
  );
  $menu_link_hook_args = array(
    Forge::CORE_SUPPORT_7x => '$link',
    Forge::CORE_SUPPORT_8x => '\\Drupal\\menu_link\\Entity\\MenuLink $menu_link',
  );
  $element['hook_menu_link_insert'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Menu link insertion',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'menu',
    'options' => array(
      'args' => $menu_link_hook_args,
    ),
    'info' => array(
      'description' => 'Inform modules that a menu link has been created.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_link_insert/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21menu_link%21menu_link.api.php/function/hook_menu_link_insert/8',
      ),
    ),
  );
  $element['hook_menu_link_update'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Menu link update',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'menu',
    'options' => array(
      'args' => $menu_link_hook_args,
    ),
    'info' => array(
      'description' => 'Inform modules that a menu link has been updated.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_link_update/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21menu_link%21menu_link.api.php/function/hook_menu_link_update/8',
      ),
    ),
  );
  $element['hook_menu_link_delete'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Menu link deletion',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'menu',
    'options' => array(
      'args' => $menu_link_hook_args,
    ),
    'info' => array(
      'description' => 'Inform modules that a menu link has been deleted.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu_link_delete/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21menu_link%21menu_link.api.php/function/hook_menu_link_delete/8',
      ),
    ),
  );

  $menu_hook_args = array(
    Forge::CORE_SUPPORT_7x => '$menu',
    Forge::CORE_SUPPORT_8x => '\\Drupal\\system\\MenuInterface $menu',
  );
  $element['hook_menu_insert'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Menu creation',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'menu',
    'options' => array(
      'args' => $menu_hook_args,
    ),
    'info' => array(
      'description' => 'Respond to a custom menu creation.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21menu%21menu.api.php/function/hook_menu_insert/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21menu_ui%21menu_ui.api.php/function/hook_menu_insert/8',
      ),
    ),
  );
  $element['hook_menu_update'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Menu update',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'menu',
    'options' => array(
      'args' => $menu_hook_args,
    ),
    'info' => array(
      'description' => 'Respond to a custom menu update.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21menu%21menu.api.php/function/hook_menu_update/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21menu_ui%21menu_ui.api.php/function/hook_menu_update/8',
      ),
    ),
  );
  $element['hook_menu_delete'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Menu deletion',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'menu',
    'options' => array(
      'args' => $menu_hook_args,
    ),
    'info' => array(
      'description' => 'Respond to a custom menu deletion.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21menu%21menu.api.php/function/hook_menu_delete/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21menu_ui%21menu_ui.api.php/function/hook_menu_delete/8',
      ),
    ),
  );

  // Theme support
  $element['theme_menu_tree'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme wrapper for a menu',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'theme_menu',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '$tree',
        Forge::CORE_SUPPORT_7x => '$variables',
        Forge::CORE_SUPPORT_8x => '$variables',
      ),
    ),
    'info' => array(
      'description' => 'Returns HTML for a wrapper for a menu sub-tree.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_tree/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_tree/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21includes%21menu.inc/function/theme_menu_tree/8',
      ),
    ),
  );

  $element['theme_menu_item'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme menu item and submenu',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_6x,
    'group' => 'theme_menu',
    'options' => array(
      'args' => '$link, $has_children, $menu = \'\', $in_active_trail = FALSE, $extra_class = NULL',
    ),
    'info' => array(
      'description' => 'Generate the HTML output for a menu item and submenu.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_item/6',
      ),
    ),
  );

  $element['theme_menu_item_link'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Them menu link',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_6x,
    'group' => 'theme_menu',
    'options' => array(
      'args' => '$link',
    ),
    'info' => array(
      'description' => 'Generate the HTML output for a single menu link.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_item_link/6',
      ),
    ),
  );

  $element['theme_menu_link'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme menu link and submenu',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'theme_menu',
    'options' => array(
      'args' => 'array $variables',
    ),
    'info' => array(
      'description' => 'Returns HTML for a menu link and submenu.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_link/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21includes%21menu.inc/function/theme_menu_link/8',
      ),
    ),
  );

  $element['theme_menu_local_task'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme single local task',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'theme_menu',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '$link, $active = FALSE',
        Forge::CORE_SUPPORT_7x => '$variables',
        Forge::CORE_SUPPORT_8x => '$variables',
      ),
    ),
    'info' => array(
      'description' => 'Returns HTML for a single local task link.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_local_task/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_local_task/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21includes%21menu.inc/function/theme_menu_local_task/8',
      ),
    ),
  );

  $element['theme_menu_local_tasks'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme primary and secondary local tasks',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'theme_menu',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '',
        Forge::CORE_SUPPORT_7x => '&$variables',
        Forge::CORE_SUPPORT_8x => '&$variables',
      ),
    ),
    'info' => array(
      'description' => 'Returns HTML for primary and secondary local tasks.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_local_tasks/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_local_tasks/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21includes%21menu.inc/function/theme_menu_local_tasks/8',
      ),
    ),
  );

  $element['theme_menu_local_action'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme single local action link',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'theme_menu',
    'options' => array(
      'args' => '$variables',
    ),
    'info' => array(
      'description' => 'Returns HTML for a single local action link.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/theme_menu_local_action/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21includes%21menu.inc/function/theme_menu_local_action/8',
      ),
    ),
  );

  $element['template_preprocess_menu_tree'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Preprocess a menu tree',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'theme_menu',
    'options' => array(
      'args' => '$variables',
    ),
    'info' => array(
      'description' => 'Implements template_preprocess_HOOK() for theme_menu_tree().',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/includes%21menu.inc/function/template_preprocess_menu_tree/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21includes%21menu.inc/function/template_preprocess_menu_tree/8',
      ),
    ),
  );

  return $element;
}

/**
 * Implements hook_forge_theme().
 */
function menu_forge_theme($existing, $type, $theme, $path) {
  return array(
    'forge_element_present_hook_menu' => array(
      'variables' => array('element' => NULL),
    ),
    'forge_element_render_hook_menu' => array(
      'variables' => array('element' => NULL),
    ),
    'forge_element_render_hook_menu_wildcard' => array(
      'variables' => array('wildcard' => NULL),
    ),
    'forge_element_render_hook_menu_callback' => array(
      'variables' => array('type' => NULL, 'callback' => NULL, 'arguments' => NULL, 'arguments_prefix' => NULL),
    ),
  );
}

/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing data.
 */
function forge_menu_autocomplete($project, $section, $string = '') {
  $matches = array();
  $string = trim($string, "\"' ");
  if ($project instanceof Forge_Project && $string) {
    switch ($section) {
      case 'page-callback':
        foreach ($project->data['hook_menu'] as $item) {
          if ($pc = $item['page callback']) {
            if (strpos($pc, $string) === 0) {
              $matches[$pc] = $pc;
            }
          }
        }
        break;
      case 'page-arguments':
        foreach ($project->data['hook_menu'] as $item) {
          if ($pc = $item['page arguments']) {
            $pc = explode(',', $pc);
            foreach ($pc as $value) {
              $value = trim($value, "\"' ");
              if (!in_array($value, array('TRUE', 'FALSE', 'NULL')) && !is_numeric($value) && strpos($value, $string) === 0) {
                $matches["'{$value}'"] = $value;
              }
            }
          }
        }
        break;
      case 'access-callback':
        foreach ($project->data['hook_menu'] as $item) {
          if ($ac = $item['access_fs']['callback']) {
            if (strpos($ac, $string) === 0) {
              $matches[$ac] = $ac;
            }
          }
        }
        break;
      case 'access-arguments':
        if ($permission = $project->data['hook_permission']) {
          foreach ($permission as $key => $row) {
            if (strpos($key, $string) === 0) {
              $title = $key;
              if ($row['title']) {
                $title = $title . ' (' . $key . ')';
              }
              $matches["'{$key}'"] = $title;
            }
          }
        }
        break;
      case 'file':
        foreach ($project->data['hook_menu'] as $item) {
          if ($file = $item['extra']['file_fs']['file']) {
            if (strpos($file, $string) === 0) {
              $matches[$file] = $file;
            }
          }
        }
        break;
    }
  }

  drupal_json_output($matches);
}
