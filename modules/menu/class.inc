<?php

/**
 * @file
 * Class implementation of the menu hook elements.
 */

/**
 * Menu hook class.
 *
 * @see Forge_Element_Hook
 * @see https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7
 * @see https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_menu/6
 */
class Forge_Element_Hook_Menu extends Forge_Element_Hook {
  public $presentTheme = 'forge_element_present_hook_menu';
  public $renderTheme = 'forge_element_render_hook_menu';

  /**
   * Returns primary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getPrimaryLinks($args = array()) {
    $links = array(
      forge_popup_link('add item', $this, 'add'),
      forge_link('clear', $this, 'clear'),
    );

    return $links;
  }

  /**
   * Return secondary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getSecondaryLinks($args = array()) {
    $links = array(
      forge_popup_link('edit', $this, 'edit', array('key' => $args['key'])),
      forge_link('delete', $this, 'delete', array('key' => $args['key'])),
    );

    return $links;
  }

  /**
   * Deletes a value specified by 'key' query parameter.
   *
   * @ingroup operations
   */
  public function delete() {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      unset($this->value[$query['key']]);
    }
  }

  /**
   * Returns Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      if (isset($this->value[$query['key']])) {
        $input = $this->value[$query['key']];
      }
    }
    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );

    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Item URL'),
      '#default_value' => isset($input['path']) ? $input['path'] : '',
      '#required' => TRUE,
      '#description' => t('Wildcards are applicable. See !link for details.', array('!link' => '<a href="https://drupal.org/node/109153" target="_blank">https://drupal.org/node/109153</a>')),
    );

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Item title'),
      '#default_value' => isset($input['title']) ? $input['title'] : '',
      '#required' => TRUE,
      '#description' => t('The untranslated title of the menu item.'),
    );

    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($input['description']) ? $input['description'] : '',
      '#description' => t('The untranslated description of the menu item.'),
    );

    $form['page callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Page callback'),
      '#default_value' => isset($input['page callback']) ? $input['page callback'] : '',
      '#required' => TRUE,
      '#description' => t('The function to be called to display a page when user visits the path. Example: <em>drupal_get_form</em>'),
      '#autocomplete_path' => 'admin/forge/' . $this->project . '/autocomplete/menu/page-callback',
    );
    $form['page arguments'] = array(
      '#type' => 'textfield',
      '#title' => t('Page arguments'),
      '#default_value' => isset($input['page arguments']) ? $input['page arguments'] : '',
      '#description' => t("A list of arguments to pass to the page callback function, with path component substitution. Example: <em>'mymodule_edit_form', 2, 'add'</em>"),
      '#autocomplete_path' => 'admin/forge/' . $this->project . '/autocomplete/menu/page-arguments',
    );

    $form['access_fs'] = array(
      '#type' => 'fieldset',
      '#title' => t('Item access'),
      '#collapsible' => TRUE,
      '#collapsed' => !isset($input['access_fs']['callback']) && !isset($input['access_fs']['arguments']),
    );
    $ac = l('user_access', 'https://api.drupal.org/api/drupal/modules%21user%21user.module/function/user_access/7', array('external' => TRUE));
    $form['access_fs']['callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Access callback'),
      '#default_value' => isset($input['access_fs']['callback']) ? $input['access_fs']['callback'] : '',
      '#description' => t('A function returning TRUE if the user has access rights to this menu item, and FALSE if not. Defaults to !default(). Example: <em>node_access</em>', array('!default' => $ac)),
      '#autocomplete_path' => 'admin/forge/' . $this->project . '/autocomplete/menu/access-callback',
    );
    $form['access_fs']['arguments'] = array(
      '#type' => 'textfield',
      '#title' => t('Access arguments'),
      '#default_value' => isset($input['access_fs']['arguments']) ? $input['access_fs']['arguments'] : '',
      '#description' => t("An array of arguments to pass to the access callback function, with path component substitution. Example: <em>'access own node'</em>"),
      '#autocomplete_path' => 'admin/forge/' . $this->project . '/autocomplete/menu/access-arguments',
    );

    $type = array();
    if (isset($input['type'])) {
      $type = array_filter($input['type']);
      if (!$type) {
        $type = array(MENU_NORMAL_ITEM);
      }
    }
    $form['type'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Item type'),
      '#options' => array(
        'MENU_NORMAL_ITEM'        => t('<strong>MENU_NORMAL_ITEM</strong> (Normal menu items show up in the menu tree and can be moved/hidden by the administrator).'),
        'MENU_CALLBACK'           => t('<strong>MENU_CALLBACK</strong> (Callbacks simply register a path so that the correct information is generated when the path is accessed).'),
        'MENU_SUGGESTED_ITEM'     => t('<strong>MENU_SUGGESTED_ITEM</strong> (Modules may "suggest" menu items that the administrator may enable).'),
        'MENU_LOCAL_ACTION'       => t('<strong>MENU_LOCAL_ACTION</strong> (Local actions are menu items that describe actions on the parent item such as adding a new user or block, and are rendered in the action-links list in your theme).'),
        'MENU_DEFAULT_LOCAL_TASK' => t('<strong>MENU_DEFAULT_LOCAL_TASK</strong> (Every set of local tasks should provide one "default" task, which should display the same page as the parent item).'),
        'MENU_LOCAL_TASK'         => t('<strong>MENU_LOCAL_TASK</strong> (Local tasks are menu items that describe different displays of data, and are generally rendered as tabs).'),
      ),
      '#default_value' => $type,
      '#required' => TRUE,
      '#description' => t('A combination of flags describing properties of the menu item.'),
    );

    $form['extra'] = array(
      '#type' => 'fieldset',
      '#title' => t('Additional'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $extra_collapsed = TRUE;

    // Title callback.
    $form['extra']['title_fs'] = array(
      '#type' => 'fieldset',
      '#title' => t('Title callback'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $t = l('t', 'https://api.drupal.org/api/drupal/includes%21bootstrap.inc/function/t/7', array('external' => TRUE));
    $form['extra']['title_fs']['callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Title callback'),
      '#default_value' => isset($input['extra']['title_fs']) ? $input['extra']['title_fs']['callback'] : '',
      '#description' => t('Function to generate the title; defaults to !default(). If you require only the raw string to be output, set this to FALSE.', array('!default' => $t)),
    );
    $form['extra']['title_fs']['arguments'] = array(
      '#type' => 'textfield',
      '#title' => t('Title arguments'),
      '#default_value' => isset($input['extra']['title_fs']) ? $input['extra']['title_fs']['arguments'] : '',
      '#description' => t('Arguments to send to !callback() or your custom callback, with path component substitution as described above.', array('!callback' => $t)),
    );
    $form['extra']['title_fs']['#collapsed'] = !$form['extra']['title_fs']['callback']['#default_value'] && !$form['extra']['title_fs']['arguments']['#default_value'];
    $extra_collapsed &= $form['extra']['title_fs']['#collapsed'];

    // Page callback file.
    $form['extra']['file_fs'] = array(
      '#type' => 'fieldset',
      '#title' => t('Page callback location'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['extra']['file_fs']['file'] = array(
      '#type' => 'textfield',
      '#title' => t('File name'),
      '#default_value' => isset($input['extra']['file_fs']) ? $input['extra']['file_fs']['file'] : '',
      '#description' => t('A file that will be included before the page callback is called. Example: <em>mymodule.admin.inc</em>'),
      '#autocomplete_path' => 'admin/forge/' . $this->project . '/autocomplete/menu/file',
    );
    $form['extra']['file_fs']['path'] = array(
      '#type' => 'textfield',
      '#title' => t('File path'),
      '#default_value' => isset($input['extra']['file_fs']) ? $input['extra']['file_fs']['path'] : '',
      '#description' => t('The path to the directory containing the file specified above. This defaults to the path to the module implementing the hook.'),
    );
    $form['extra']['file_fs']['#collapsed'] = !$form['extra']['file_fs']['file']['#default_value'] && !$form['extra']['file_fs']['path']['#default_value'];
    $extra_collapsed &= $form['extra']['file_fs']['#collapsed'];

    // Theme callback.
    $form['extra']['theme_fs'] = array(
      '#type' => 'fieldset',
      '#title' => t('Item theming'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['extra']['theme_fs']['callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Theme callback'),
      '#default_value' => isset($input['extra']['theme_fs']) ? $input['extra']['theme_fs']['callback'] : '',
      '#description' => t('A function returning the machine-readable name of the theme that will be used to render the page. Example: <em>_block_custom_theme</em>', array('!default' => $ac)),
    );
    $form['extra']['theme_fs']['arguments'] = array(
      '#type' => 'textfield',
      '#title' => t('Theme arguments'),
      '#default_value' => isset($input['extra']['theme_fs']) ? $input['extra']['theme_fs']['arguments'] : '',
      '#description' => t("An array of arguments to pass to the theme callback function, with path component substitution. Example: <em>'seven'</em>"),
    );
    $form['extra']['theme_fs']['#collapsed'] = !$form['extra']['theme_fs']['callback']['#default_value'] && !$form['extra']['theme_fs']['arguments']['#default_value'];
    $extra_collapsed &= $form['extra']['theme_fs']['#collapsed'];

    // Tab settings
    $form['extra']['tab_fs'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tab options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['extra']['tab_fs']['parent'] = array(
      '#type' => 'textfield',
      '#title' => t('Tab parent'),
      '#default_value' => isset($input['extra']['tab_fs']) ? $input['extra']['tab_fs']['parent'] : '',
      '#description' => t("For local task menu items, the path of the task's parent item; defaults to the same path without the last component."),
    );
    $form['extra']['tab_fs']['root'] = array(
      '#type' => 'textfield',
      '#title' => t('Tab root'),
      '#default_value' => isset($input['extra']['tab_fs']) ? $input['extra']['tab_fs']['root'] : '',
      '#description' => t("For local task menu items, the path of the closest non-tab item."),
    );
    $form['extra']['tab_fs']['#collapsed'] = !$form['extra']['tab_fs']['parent']['#default_value'] && !$form['extra']['tab_fs']['root']['#default_value'];
    $extra_collapsed &= $form['extra']['tab_fs']['#collapsed'];

    $dc = l('drupal_deliver_html_page', 'https://api.drupal.org/api/drupal/includes%21common.inc/function/drupal_deliver_html_page/7', array('external' => TRUE));
    $form['extra']['delivery callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Delivery callback'),
      '#default_value' => isset($input['extra']['delivery callback']) ? $input['extra']['delivery callback'] : '',
      '#description' => t("The function to call to package the result of the page callback function and send it to the browser. Defaults to !default().", array('!default' => $dc)),
    );
    $extra_collapsed &= !$form['extra']['delivery callback']['#default_value'];

    $form['extra']['menu_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Menu name'),
      '#default_value' => isset($input['extra']['menu_name']) ? $input['extra']['menu_name'] : '',
      '#description' => t("Set this to a custom menu if you don't want your item to be placed in Navigation."),
    );
    $extra_collapsed &= !$form['extra']['menu_name']['#default_value'];

    $form['extra']['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#delta' => 50,
      '#default_value' => isset($input['extra']['weight']) ? $input['extra']['weight'] : 0,
      '#description' => t('An integer that determines the relative position of items in the menu; higher-weighted items sink. Defaults to 0.'),
    );
    $extra_collapsed &= !$form['extra']['weight']['#default_value'];

    $form['extra']['expanded'] = array(
      '#type' => 'checkbox',
      '#title' => t('Expanded'),
      '#default_value' => isset($input['extra']['expanded']) ? $input['extra']['expanded'] : FALSE,
      '#description' => t('If checked, and if a menu link is provided for this menu item (as a result of other properties), then the menu link is always expanded.'),
    );
    $extra_collapsed &= !$form['extra']['expanded']['#default_value'];

    $form['extra']['#collapsed'] = $extra_collapsed;

    return $form;
  }

  /**
   * Submit-processing of element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $this->value[$query['key']] = $input;
    } else {
      $this->value[] = $input;
    }
    // Update hook_permission.
    if (class_exists('Forge_Element_Hook_Permission')) {
      foreach ($this->value as $item) {
        $arguments = $item['access_fs']['arguments'];
        if ($arguments) {
          $arguments = explode(',', $arguments);
          foreach ($arguments as $arg) {
            $permission = trim($arg, "\"' ");
            if (!in_array($permission, array('TRUE', 'FALSE', 'NULL')) && !is_numeric($permission)) {
              Forge_Element_Hook_Permission::sessionSet($this->project, $permission);
            }
          }
        }
      }
    }
  }

  /**
   * @see Forge_Element::getFiles()
   * @ingroup render
   */
  public function getFiles() {
    $files = array();
    foreach ($this->value as $item) {
      $file = $item['extra']['file_fs']['file'];
      $path = $item['extra']['file_fs']['path'];
      if (!empty($file) && empty($path)) {
        $files[] = $file;
      }
    }

    return $files;
  }

  /**
   * Renders the hook_menu element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core));

      $existing_wildcard = $existing_callback = array();
      $skip_callback = array(
        'TRUE',
        'FALSE',
        'NULL',
      );
      $contrib_wildcard = array(
        '%aggregator_feed',
        '%aggregator_category',
        '%comment',
        '%comment_node_type',
        '%contact',
        '%field_test_entity_test',
        '%field_ui_menu',
        '%filter_format',
        '%forum_forum',
        '%forum_term',
        '%image_style',
        '%image_effect',
        '%image_effect_definition',
        '%menu',
        '%menu_link',
        '%menu_test_argument',
        '%menu_test_other_argument',
        '%node',
        '%node_type',
        '%path',
        '%shortcut_set',
        '%mymodule_abc',
        '%blocked_ip',
        '%actions',
        '%taxonomy_term',
        '%taxonomy_vocabulary',
        '%taxonomy_vocabulary_machine_name',
        '%user',
        '%user_uid_optional',
        '%user_role',
        '%user_category',
        '%views_ui_cache',
      );
      foreach ($this->value as $item) {
        // Create non-contrib wildcards.
        $path = explode('/', $item['path']);
        foreach ($path as $part) {
          if (preg_match('/^%[a-z_]+$/', $part)) {
            if (!in_array($part, $contrib_wildcard) && !in_array($part, $existing_wildcard)) {
              $existing_wildcard[] = $part;
              $output[$this->renderDestination][] = theme('forge_element_render_hook_menu_wildcard', array('wildcard' => $part));
            }
          }
        }
        // Create callbacks.
        $cb_types = array('access', 'title', 'theme');
        foreach ($cb_types as $type) {
          $variables = NULL;
          if (isset($item[$type . '_fs'])) {
            $variables = $item[$type . '_fs'];
          } elseif (isset($item['extra'][$type . '_fs'])) {
            $variables = $item['extra'][$type . '_fs'];
          }
          if ($variables) {
            $cb_exists = in_array($variables['callback'], $existing_callback);
            $cb_skip = in_array($variables['callback'], $skip_callback) || is_numeric($variables['callback']);
            if (!empty($variables['callback']) && !$cb_exists && !$cb_skip) {
              $existing_callback[] = $variables['callback'];
              $variables['type'] = $type;
              $variables['path'] = $item['path'];
              $output[$this->renderDestination][] = theme('forge_element_render_hook_menu_callback', $variables);
            }
          }
        }
        // Create page callbacks
        if (isset($item['page callback'])) {
          if (strpos($item['page callback'], $this->project) !== FALSE || $item['page callback'] == 'drupal_get_form') {
            $variables = array('type' => 'menu');
            if ($item['page callback'] == 'drupal_get_form') {
              $variables['arguments'] = explode(',', $item['page arguments']);
              $variables['callback'] = trim(array_shift($variables['arguments']), " '\"");
              $variables['arguments'] = implode(',', $variables['arguments']);
              switch ($core) {
                case Forge::CORE_SUPPORT_6x:
                  $variables['arguments_prefix'] = '&$form_state';
                  break;
                case Forge::CORE_SUPPORT_7x:
                  $variables['arguments_prefix'] = '$form, &$form_state';
                  break;
              }
            } else {
              $variables['callback'] = $item['page callback'];
              $variables['arguments'] = $item['page arguments'];
            }
            $cb_file = $this->renderDestination;
            if (isset($item['extra']['file_fs'])) {
              $file = $item['extra']['file_fs'];
              if (!empty($file['file']) && empty($file['path'])) {
                $cb_file = $file['file'];
              }
            }
            $cb_exists = in_array($variables['callback'], $existing_callback);
            $cb_skip = in_array($variables['callback'], $skip_callback) || is_numeric($variables['callback']);
            if (!$cb_exists && !$cb_skip) {
              $existing_callback[] = $variables['callback'];
              $variables['path'] = $item['path'];
              $output[$cb_file][] = theme('forge_element_render_hook_menu_callback', $variables);
            }
          }
        }
      }
    }
  }
}
