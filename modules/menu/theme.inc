<?php

function theme_forge_element_present_hook_menu($variables) {
  $header = $rows = array();

  $class = array('forge-element', 'forge-element-hook-menu');
  $header_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE, 'hook' => TRUE)), 'width' => '70%', 'colspan' => 4),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 6),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  if (empty($variables['element']->value)) {
    $class[] = 'forge-element-empty';
    $rows[] = array(
      array('data' => '<em>' . t('no items found') . '</em>', 'colspan' => 6),
    );
  } else {
    $rows[] = array(
      array('data' => '<strong>' . t('URL') . '</strong>', 'width' => '15%'),
      array('data' => '<strong>' . t('Title and description') . '</strong>', 'width' => '20%'),
      array('data' => '<strong>' . t('Type') . '</strong>', 'width' => '15%'),
      array('data' => '<strong>' . t('Additional') . '</strong>', 'width' => '30%', 'colspan' => 2),
      array('data' => '', 'width' => '20%'),
    );
    foreach ($variables['element']->value as $key => $value) {
      $row = array();
      $row[] = $value['path'];

      // Title
      $title = $value['title'];
      $title = array(t('Title: %value', array('%value' => $title)));
      $callback = $value['extra']['title_fs']['callback'];
      $arguments = $value['extra']['title_fs']['arguments'];
      if (!empty($callback) || strlen($arguments) > 0) {
        if ($callback) {
          $title[] = t('Callback: %value', array('%value' => $callback));
        }
        if (strlen($arguments) > 0) {
          $title[] = t('Arguments: %value', array('%value' => $arguments));
        }
      }
      if ($value['description']) {
        $title[] = t('Description: %value', array('%value' => $value['description']));
      }
      $title = theme('item_list', array('items' => $title));
      $row[] = $title;

      // Item type
      $values = array(
        'MENU_NORMAL_ITEM',
        'MENU_CALLBACK',
        'MENU_SUGGESTED_ITEM',
        'MENU_LOCAL_ACTION',
        'MENU_DEFAULT_LOCAL_TASK',
        'MENU_LOCAL_TASK',
      );
      $types = array();
      foreach ($values as $type) {
        if (!empty($value['type'][$type])) {
          $types[] = $type;
        }
      }
      if (empty($types)) {
        $types[] = 'MENU_NORMAL_ITEM';
      }
      $row[] = theme('item_list', array('items' => $types));

      // Additional
      $extra = array();
      // - page callback and location
      $extra[] = t('Page callback: %value', array('%value' => $value['page callback']));
      if ($value['page arguments']) {
        $extra[] = t('Page arguments: %value', array('%value' => $value['page arguments']));
      }
      if ($value['extra']['file_fs']['file']) {
        $extra[] = t('File: %value', array('%value' => $value['extra']['file_fs']['file']));
      }
      if ($value['extra']['file_fs']['path']) {
        $extra[] = t('Path: %value', array('%value' => $value['extra']['file_fs']['path']));
      }
      // - access
      if ($value['access_fs']['callback']) {
        $extra[] = t('Access callback: %value', array('%value' => $value['access_fs']['callback']));
      }
      if (strlen($value['access_fs']['arguments'])>0) {
        $extra[] = t('Access arguments: %value', array('%value' => $value['access_fs']['arguments']));
      }
      // - theme
      if ($value['extra']['theme_fs']['callback']) {
        $extra[] = t('Theme callback: %value', array('%value' => $value['extra']['theme_fs']['callback']));
      }
      if ($value['extra']['theme_fs']['arguments']) {
        $extra[] = t('Theme arguments: %value', array('%value' => $value['extra']['theme_fs']['arguments']));
      }
      // - tab
      if ($value['extra']['tab_fs']['parent']) {
        $extra[] = t('Tab parent: %value', array('%value' => $value['extra']['tab_fs']['parent']));
      }
      if ($value['extra']['tab_fs']['root']) {
        $extra[] = t('Tab root: %value', array('%value' => $value['extra']['tab_fs']['root']));
      }
      // - delivery
      if ($value['extra']['delivery callback']) {
        $extra[] = t('Delivery callback: %value', array('%value' => $value['extra']['delivery callback']));
      }
      // - menu name
      if ($value['extra']['menu_name']) {
        $extra[] = t('Menu name: %value', array('%value' => $value['extra']['menu_name']));
      }
      if ($value['extra']['weight']) {
        $extra[] = t('Weight: %value', array('%value' => $value['extra']['weight']));
      }
      if ($value['extra']['expanded']) {
        $extra[] = t('Expanded: %value', array('%value' => t('yes')));
      }
      $row[] = array('data' => theme('item_list', array('items' => $extra)), 'colspan' => 2);

      // operations
      $row_links = $variables['element']->getSecondaryLinks(array('key' => $key));
      $row[] = array('data' => theme('links__ctools_dropbutton', array('links' => $row_links, 'attributes' => array('class' => array('links', 'inline')))), 'class' => array('forge-operations'));

      $rows[] = $row;
    }
  }

  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));
}

/**
 * Render hook_schema element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Menu object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constants.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_menu($variables) {
  $body = array('$items = array();', '');
  $items = $variables['element']->value;
  foreach ($items as $raw) {
    $item = array();
    // Title.
    if (!empty($raw['title'])) {
      $item['title'] = $raw['title'];
    }
    if (!empty($raw['description'])) {
      $item['description'] = $raw['description'];
    }
    if (isset($raw['extra']['title_fs'])) {
      $title = $raw['extra']['title_fs'];
      if (!empty($title['callback'])) {
        $item['title callback'] = $title['callback'];
      }
      if (!empty($title['arguments'])) {
        $item['title arguments'] = theme('forge_array', array('array' => explode(',', $title['arguments']), 'clean' => TRUE, 'nolf' => TRUE));
      }
    }
    // Page callback.
    if (isset($raw['page callback'])) {
      $item['page callback'] = $raw['page callback'];
      if (!empty($raw['page arguments'])) {
        $item['page arguments'] = theme('forge_array', array('array' => explode(',', $raw['page arguments']), 'clean' => TRUE, 'nolf' => TRUE));
      }
    }
    if (!empty($raw['extra']['delivery callback'])) {
      $item['delivery callback'] = $raw['extra']['delivery callback'];
    }
    // Access callback.
    if (isset($raw['access_fs'])) {
      if (!empty($raw['access_fs']['callback'])) {
        $item['access callback'] = $raw['access_fs']['callback'];
      }
      if (!empty($raw['access_fs']['arguments'])) {
        $item['access arguments'] = theme('forge_array', array('array' => explode(',', $raw['access_fs']['arguments']), 'clean' => TRUE, 'nolf' => TRUE));
      }
    }
    // Theme callback.
    if (isset($raw['extra']['theme_fs'])) {
      $theme = $raw['extra']['theme_fs'];
      if (!empty($theme['callback'])) {
        $item['theme callback'] = $theme['callback'];
      }
      if (!empty($theme['arguments'])) {
        $item['theme arguments'] = theme('forge_array', array('array' => explode(',', $theme['arguments']), 'clean' => TRUE, 'nolf' => TRUE));
      }
    }
    // Menu attributes.
    if (!empty($raw['extra']['menu_name'])) {
      $item['menu_name'] = $raw['extra']['menu_name'];
    }
    if (!empty($raw['extra']['weight'])) {
      $item['weight'] = (int)$raw['extra']['weight'];
    }
    if (!empty($raw['extra']['expanded'])) {
      $item['expanded'] = 'TRUE';
    }
    if (isset($raw['extra']['tab_fs'])) {
      $tab = $raw['extra']['tab_fs'];
      if (!empty($tab['parent'])) {
        $item['parent'] = $tab['parent'];
      }
      if (!empty($tab['root'])) {
        $item['root'] = $tab['root'];
      }
    }
    // Menu callback location.
    if (isset($raw['extra']['file_fs'])) {
      $file = $raw['extra']['file_fs'];
      if (!empty($file['file'])) {
        $item['file'] = $file['file'];
      }
      if (!empty($file['path'])) {
        $item['path'] = $file['path'];
      }
    }
    // Item type.
    $types = array();
    foreach ($raw['type'] as $type) {
      if ($type) {
        $types[] = $type;
      }
    }
    if ($types) {
      $item['type'] = implode('|', $types);
    }

    $item = theme('forge_array', array('array' => $item));
    $body[] = '$items[\'' . $raw['path'] . '\'] = ' . $item . ';';
  }
  $body[] = '';
  $body[] = 'return $items;';
  $body = implode("\n", $body);
  $body = preg_replace("/'(array\(.*\))'/", '$1', $body);
  $body = str_replace("\'", "'", $body);
  $body = preg_replace("/'(MENU_.*)'/", '$1', $body);

  $variables['args'] = '';
  $variables['body'] = $body;

  return theme('forge_element_render_hook', $variables);
}

/**
 * Render menu item wildcard.
 *
 * @param $variables
 *   An associative array containing:
 *   - wildcard: Wirlcard name.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_menu_wildcard($variables) {
  $wildcard = $variables['wildcard'];
  $function = trim($wildcard, ' %') . '_load';
  $argument = '$load_arg';
  return <<<EOL
/**
 * {$wildcard} loader.
 */
function {$function}({$argument}) {
  //
}

EOL;
}

/**
 * Render menu item callback.
 *
 * @param $variables
 *   An associative array containing:
 *   - type:      Type of callback.
 *   - callback:  Callback function.
 *   - arguments: Callback function arguments.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_menu_callback($variables) {
  $type = ucfirst($variables['type']);
  $path = $variables['path'];
  $callback = $variables['callback'];
  $arguments = $variables['arguments'];
  if (!empty($arguments)) {
    $arguments = explode(',', $arguments);
    $counter = 0;
    foreach ($arguments as &$arg) {
      $arg = '$arg' . $counter;
      $counter++;
    }
    $arguments = implode(', ', $arguments);
  }
  if (isset($variables['arguments_prefix'])) {
    $arguments = $variables['arguments_prefix'] . (empty($arguments) ? '' : (', ' . $arguments));
  }

  return <<<EOL
/**
 * {$type} callback for '{$path}'.
 */
function {$callback}({$arguments}) {
  //
}

EOL;
}
