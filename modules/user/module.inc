<?php

/**
 * @file
 * Provides Forge hooks for node.module.
 */

/**
 * Implements hook_forge_group().
 */
function user_forge_group() {
  $group['user'] = $group['theme_user'] = array(
    'title' => 'User',
  );
  $group['user_role'] = array(
    'title' => 'Role',
  );
  return $group;
}

/**
 * Implements hook_forge_element().
 */
function user_forge_element() {
  // Module support.
  $element['hook_user'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User account actions',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_6x,
    'group' => 'user',
    'options' => array(
      'args' => '$op, &$edit, &$account, $category = NULL',
    ),
    'info' => array(
      'description' => 'This hook allows modules to react when operations are performed on user accounts.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_user/6',
      ),
    ),
  );

  $element['hook_user_load'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Load user',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => '$users',
    ),
    'info' => array(
      'description' => '',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_load/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_load/8',
      ),
    ),
  );

  $element['hook_user_delete'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User deletion',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => '$account',
    ),
    'info' => array(
      'description' => 'Respond to user deletion.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_delete/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_delete/8',
      ),
    ),
  );

  $element['hook_user_cancel'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User cancellations',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => '$edit, $account, $method',
    ),
    'info' => array(
      'description' => 'Act on user account cancellations.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_cancel/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_cancel/8',
      ),
    ),
  );

  $element['hook_user_cancel_methods_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User cancellation methods',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => '&$methods',
    ),
    'info' => array(
      'description' => 'Modify account cancellation methods.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_cancel_methods_alter/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_cancel_methods_alter/8',
      ),
    ),
  );

  $element['hook_user_operations'] = array(
    'class' => 'Forge_Element_Hook_User_Operations',
    'name' => 'Mass user operations',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'user',
    'info' => array(
      'description' => 'This hook enables modules to inject custom operations into the mass operations dropdown found at admin/people, by associating a callback function with the operation, which is called when the form is submitted. The callback function receives one initial argument, which is an array of the checked users.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_operations/7',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_user_operations/6',
      ),
    ),
  );

  $element['hook_user_categories'] = array(
    'class' => 'Forge_Element_Hook_User_Categories',
    'name' => 'User categories',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'user',
    'info' => array(
      'description' => 'Retrieve a list of user setting or profile information categories.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_categories/7',
      ),
    ),
  );

  $element['hook_user_presave'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User presave',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$edit, $account, $category',
        Forge::CORE_SUPPORT_8x => '$account',
      ),
    ),
    'info' => array(
      'description' => 'A user account is about to be created or updated.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_presave/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_presave/8',
      ),
    ),
  );

  $element['hook_user_insert'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User insert',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$edit, $account, $category',
        Forge::CORE_SUPPORT_8x => '$account',
      ),
    ),
    'info' => array(
      'description' => 'The module should save its custom additions to the user object into the database.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_insert/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_insert/8',
      ),
    ),
  );

  $element['hook_user_update'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User update',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$edit, $account, $category',
        Forge::CORE_SUPPORT_8x => '$account',
      ),
    ),
    'info' => array(
      'description' => 'Modules may use this hook to update their user data in a custom storage after a user account has been updated.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_update/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_update/8',
      ),
    ),
  );

  $element['hook_user_login'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User login',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$edit, $account',
        Forge::CORE_SUPPORT_8x => '$account',
      ),
    ),
    'info' => array(
      'description' => 'The user just logged in.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_login/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_login/8',
      ),
    ),
  );

  $element['hook_user_logout'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User logout',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => '$account',
    ),
    'info' => array(
      'description' => 'The user just logged out.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_logout/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_logout/8',
      ),
    ),
  );

  $element['hook_user_view'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User view',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$account, $view_mode, $langcode',
        Forge::CORE_SUPPORT_8x => '\Drupal\user\UserInterface $account, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display, $view_mode, $langcode',
      ),
    ),
    'info' => array(
      'description' => "The user's account information is being displayed.",
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_view/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_view/8',
      ),
    ),
  );

  $element['hook_user_view_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User view alter',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$build',
        Forge::CORE_SUPPORT_8x => '&$build, \Drupal\user\UserInterface $account, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display',
      ),
    ),
    'info' => array(
      'description' => 'This hook is called after the content has been assembled in a structured array and may be used for doing processing which requires that the complete user content structure has been built.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_view_alter/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_view_alter/8',
      ),
    ),
  );

  $element['hook_user_role_presave'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User role presave',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user_role',
    'options' => array(
      'args' => '$role',
    ),
    'info' => array(
      'description' => 'Inform other modules that a user role is about to be saved.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_role_presave/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_role_presave/8',
      ),
    ),
  );

  $element['hook_user_role_insert'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User role insert',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user_role',
    'options' => array(
      'args' => '$role',
    ),
    'info' => array(
      'description' => 'Inform other modules that a user role has been added.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_role_insert/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_role_insert/8',
      ),
    ),
  );

  $element['hook_user_role_update'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User role update',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user_role',
    'options' => array(
      'args' => '$role',
    ),
    'info' => array(
      'description' => 'Inform other modules that a user role has been updated.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_role_update/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_role_update/8',
      ),
    ),
  );

  $element['hook_user_role_delete'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'User role delete',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'user_role',
    'options' => array(
      'args' => '$role',
    ),
    'info' => array(
      'description' => 'Inform other modules that a user role has been deleted.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_role_delete/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.api.php/function/hook_user_role_delete/8',
      ),
    ),
  );

  // Theme support.
  $element['theme_user_list'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Users list',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_user',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '$users, $title = NULL',
        Forge::CORE_SUPPORT_7x => '$variables',
      ),
    ),
    'info' => array(
      'description' => 'Make a list of users.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user.module/function/theme_user_list/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.module/function/theme_user_list/7',
      ),
    ),
  );

  $element['theme_user_admin_permissions'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme permissions',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'theme_user',
    'options' => array(
      'args' => '$variables',
    ),
    'info' => array(
      'description' => 'Returns HTML for the administer permissions page.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.admin.inc/function/theme_user_admin_permissions/7',
      ),
    ),
  );

  $element['theme_user_permission_description'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme permission',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'theme_user',
    'options' => array(
      'args' => '$variables',
    ),
    'info' => array(
      'description' => 'Returns HTML for an individual permission description.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.admin.inc/function/theme_user_permission_description/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.admin.inc/function/theme_user_permission_description/8',
      ),
    ),
  );

  $element['theme_user_admin_roles'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme roles list',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'theme_user',
    'options' => array(
      'args' => '$variables',
    ),
    'info' => array(
      'description' => 'Returns HTML for the role order and new role form.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.admin.inc/function/theme_user_admin_roles/7',
      ),
    ),
  );

  $element['theme_user_signature'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'User signature',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'theme_user',
    'options' => array(
      'args' => '$variables',
    ),
    'info' => array(
      'description' => 'Returns HTML for a user signature.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user.module/function/theme_user_signature/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.module/function/theme_user_signature/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21user.module/function/theme_user_signature/8',
      ),
    ),
  );

  $element['template_preprocess_user_picture'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Process variables for user-picture.tpl.php.',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_node',
    'options' => array(
      'args' => '&$variables',
    ),
    'info' => array(
      'description' => 'Process variables for user-picture.tpl.php.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user.module/function/template_preprocess_user_picture/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.module/function/template_preprocess_user_picture/7',
      ),
    ),
  );

  $element['template_preprocess_user_profile'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Process variables for user-profile.tpl.php.',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_user',
    'options' => array(
      'args' => '&$variables',
    ),
    'info' => array(
      'description' => 'Process variables for user-profile.tpl.php.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user.pages.inc/function/template_preprocess_user_profile/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.pages.inc/function/template_preprocess_user_profile/7',
      ),
    ),
  );

  $element['template_preprocess_user_profile_category'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Process variables for user-profile-category.tpl.php.',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_user',
    'options' => array(
      'args' => '&$variables',
    ),
    'info' => array(
      'description' => 'Process variables for user-profile-category.tpl.php.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user.pages.inc/function/template_preprocess_user_profile_category/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.pages.inc/function/template_preprocess_user_profile_category/7',
      ),
    ),
  );

  $element['template_preprocess_user_profile_item'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Process variables for user-profile-item.tpl.php.',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_user',
    'options' => array(
      'args' => '&$variables',
    ),
    'info' => array(
      'description' => 'Process variables for user-profile-item.tpl.php.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user.pages.inc/function/template_preprocess_user_profile_item/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user.pages.inc/function/template_preprocess_user_profile_item/7',
      ),
    ),
  );

  $element['user-picture.tpl.php'] = array(
    'class' => 'Forge_Element_Template',
    'name' => 'Theme implementation to present a user picture',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_user',
    'options' => array(
      'args' => 'modules/user/user-picture.tpl.php',
    ),
    'info' => array(
      'description' => "Default theme implementation to present a picture configured for the user's account.",
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user-picture.tpl.php/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user-picture.tpl.php/7',
      ),
    ),
  );

  $element['user-profile.tpl.php'] = array(
    'class' => 'Forge_Element_Template',
    'name' => 'Theme implementation to to present user profile',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'theme_user',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => 'modules/user/user-picture.tpl.php',
        Forge::CORE_SUPPORT_7x => 'modules/user/user-picture.tpl.php',
        Forge::CORE_SUPPORT_8x => 'core/modules/user/templates/user-profile.tpl.php',
      ),
    ),
    'info' => array(
      'description' => 'Default theme implementation to present all user profile data.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user-profile.tpl.php/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user-profile.tpl.php/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21user%21templates%21user-profile.tpl.php/8',
      ),
    ),
  );

  $element['user-profile-category.tpl.php'] = array(
    'class' => 'Forge_Element_Template',
    'name' => 'Theme implementation to to present profile categories',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_user',
    'options' => array(
      'args' => 'modules/user/user-profile-category.tpl.php',
    ),
    'info' => array(
      'description' => 'Default theme implementation to present profile categories (groups of profile items).',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user-profile-category.tpl.php/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user-profile-category.tpl.php/7',
      ),
    ),
  );

  $element['user-profile-item.tpl.php'] = array(
    'class' => 'Forge_Element_Template',
    'name' => 'Theme implementation to to present profile items',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_user',
    'options' => array(
      'args' => 'modules/user/user-profile-item.tpl.php',
    ),
    'info' => array(
      'description' => 'Default theme implementation to present profile items (values from user account profile fields or modules).',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21user%21user-profile-item.tpl.php/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21user%21user-profile-item.tpl.php/7',
      ),
    ),
  );

  return $element;
}

/**
 * Implements hook_forge_theme().
 */
function user_forge_theme($existing, $type, $theme, $path) {
  return array(
    'forge_element_present_hook_user_operations' => array(
      'variables' => array('element' => NULL),
    ),
    'forge_element_present_hook_user_categories' => array(
      'variables' => array('element' => NULL),
    ),
  );
}
