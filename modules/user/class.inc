<?php

/**
 * @file
 * Class implementation of a user hook elements.
 */

/**
 * User operations hook class.
 *
 * @see Forge_Element_Hook_Multiple
 * @see https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_user_operations/6
 * @see https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_operations/7
 */
class Forge_Element_Hook_User_Operations extends Forge_Element_Hook_Multiple {
  public $presentTheme = 'forge_element_present_hook_user_operations';

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $input = $this->value[$query['key']];
    }

    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => isset($input['key']) ? $input['key'] : '',
      '#description' => t('Machine name of an operation.'),
      '#required' => TRUE,
    );
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($input['label']) ? $input['label'] : '',
      '#description' => t('The label for the operation, displayed in the dropdown menu.'),
      '#required' => TRUE,
    );
    $form['callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Callback'),
      '#default_value' => isset($input['callback']) ? $input['callback'] : '',
      '#description' => t('The function to call for the operation.'),
      '#required' => TRUE,
    );
    $form['callback arguments'] = array(
      '#type' => 'textfield',
      '#title' => t('Callback arguments'),
      '#default_value' => isset($input['callback arguments']) ? $input['callback arguments'] : '',
      '#description' => t('An list of additional arguments to pass to the callback function.'),
    );

    return $form;
  }

  /**
   * On-submit processing of the element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $this->value = Forge::arrayKeyReplace($this->value, $query['key'], $input['key'], $input);
    } else {
      $this->value[$input['key']] = $input;
    }
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $operations = $this->value;
      $rows = array();
      foreach ($operations as $key => $op) {
        $row = array(
          'label' => "t('" . $op['label'] . "')",
          'callback' => $op['callback'],
        );
        if ($op['callback arguments']) {
          $row['callback arguments'] = $op['callback arguments'];
        }
        $rows[$key] = $row;
      }
      $body = theme('forge_array', array('array' => $rows));
      $body = preg_replace("/'(t\(.*\))'/", '$1', $body);
      $body = str_replace(array("t(\'", "\')"), array("t('", "')"), $body);
      $body = "return $body;";

      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'body' => $body));

      $cb_proceed = array();
      foreach ($operations as $key => $op) {
        if (!in_array($op['callback'], $cb_proceed)) {
          $cb_proceed[] = $op['callback'];
          $arguments = '$accounts';
          if ($op['callback arguments']) {
            $arguments = explode(',', $op['callback arguments']);
            $counter = 0;
            foreach ($arguments as &$arg) {
              $arg = trim($arg);
              if (!preg_match('/^\$(.*?)$/', $arg)) {
                $arg = '$arg' . $counter;
                $counter++;
              }
            }
            $arguments = implode(', ', $arguments);
          }
          $callback = <<<PHP
/**
 * User operation '{$key}' callback.
 */
function {$op['callback']}({$arguments}) {
  //
}
PHP;
          $output[$this->renderDestination][] = $callback;
        }
      }
    }
  }
}

/**
 * User categories hook class.
 *
 * @see Forge_Element_Hook_Multiple
 * @see https://api.drupal.org/api/drupal/modules%21user%21user.api.php/function/hook_user_categories/7
 */
class Forge_Element_Hook_User_Categories extends Forge_Element_Hook_Multiple {
  public $presentTheme = 'forge_element_present_hook_user_categories';

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $input = $this->value[$query['key']];
    }

    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => isset($input['key']) ? $input['key'] : '',
      '#description' => t('Machine name of an category.'),
      '#required' => TRUE,
    );
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($input['title']) ? $input['title'] : '',
      '#description' => t('The human-readable, localized name of the category.'),
      '#required' => TRUE,
    );
    $form['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#default_value' => isset($input['weight']) ? $input['weight'] : 0,
      '#description' => t("An integer specifying the category's sort ordering."),
    );
    $form['access callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Access callback'),
      '#default_value' => isset($input['access callback']) ? $input['access callback'] : '',
      '#description' => t('Name of the access callback function to use to determine whether the user can edit the category.'),
    );
    $form['access arguments'] = array(
      '#type' => 'textfield',
      '#title' => t('Callback arguments'),
      '#default_value' => isset($input['access arguments']) ? $input['access arguments'] : '',
      '#description' => t('Arguments for the access callback function. Defaults to array(1).'),
    );

    return $form;
  }

  /**
   * On-submit processing of the element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $this->value = Forge::arrayKeyReplace($this->value, $query['key'], $input['key'], $input);
    } else {
      $this->value[$input['key']] = $input;
    }
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $categories = $this->value;
      $rows = array();
      foreach ($categories as $key => $cat) {
        $row = array(
          'name' => $key,
          'title' => "t('" . $cat['title'] . "')",
        );
        if ($cat['weight']) {
          $row['weight'] = $cat['weight'];
        }
        if ($cat['access callback']) {
          $row['access callback'] = $cat['access callback'];
        }
        if ($cat['access arguments']) {
          $row['access arguments'] = "array(" . $cat['access arguments'] . ")";
        }
        $rows[] = $row;
      }
      $body = theme('forge_array', array('array' => $rows));
      $body = preg_replace("/'(t\(.*\))'/", '$1', $body);
      $body = preg_replace("/'(array\(.*\))'/", '$1', $body);
      $body = str_replace("\'", "'", $body);
      $body = "return $body;";

      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'body' => $body));

      $cb_proceed = array();
      foreach ($categories as $key => $cat) {
        if (!in_array($cat['access callback'], $cb_proceed)) {
          $cb_proceed[] = $cat['access callback'];
          $arguments = '';
          if ($cat['access arguments']) {
            $arguments = explode(',', $cat['access arguments']);
            $counter = 0;
            foreach ($arguments as &$arg) {
              $arg = trim($arg);
              if (!preg_match('/^\$(.*?)$/', $arg)) {
                $arg = '$arg' . $counter;
                $counter++;
              }
            }
            $arguments = implode(', ', $arguments);
          }
          $callback = <<<PHP
/**
 * User category '{$key}' access callback.
 */
function {$cat['access callback']}({$arguments}) {
  //
}
PHP;
          $output[$this->renderDestination][] = $callback;
        }
      }
    }
  }
}
