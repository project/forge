<?php

/**
 * Returns HTML for presentation of user operations hook.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_User_Operations object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_user_operations($variables) {
  $header = $rows = array();

  $class = array('forge-element', 'forge-element-hook-user-operations');
  $header_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE, 'hook' => TRUE)), 'width' => '70%', 'colspan' => 2),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );

  if ($value = $variables['element']->value) {
    $rows[] = array(
      array('data' => '<strong>' . t('Name') . '</strong>', 'width' => '20%'),
      array('data' => '<strong>' . t('Callback') . '</strong>', 'width' => '50%'),
      array('data' => '', 'width' => '30%', 'colspan' => 2),
    );

    foreach ($value as $key => $item) {
      $info = array();
      $info[] = t('Callback: %value', array('%value' => $item['callback']));
      if ($item['callback arguments']) {
        $info[] = t('Arguments: %value', array('%value' => $item['callback arguments']));
      }
      $info = theme('item_list', array('items' => $info));

      $row_links = $variables['element']->getSecondaryLinks(array('key' => $key));
      $row_links = array('data' => theme('links__ctools_dropbutton', array('links' => $row_links, 'attributes' => array('class' => array('links', 'inline')))), 'class' => array('forge-operations'));

      $rows[] = array(
        $item['label'] . '<div class="description">' . $key . '</div>',
        array('data' => $info, 'colspan' => 2),
        $row_links,
      );
    }
  } else {
    $rows[] = array(array('data' => t('no operations found'), 'colspan' => 4));
    $class[] = 'forge-element-empty';
  }

  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));
}

/**
 * Returns HTML for presentation of user categories hook.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_User_Operations object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_user_categories($variables) {
  $header = $rows = array();

  $class = array('forge-element', 'forge-element-hook-user-categories');
  $header_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE, 'hook' => TRUE)), 'width' => '70%', 'colspan' => 2),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );

  if ($value = $variables['element']->value) {
    $rows[] = array(
      array('data' => '<strong>' . t('Name') . '</strong>', 'width' => '20%'),
      array('data' => '<strong>' . t('Callback') . '</strong>', 'width' => '50%'),
      array('data' => '', 'width' => '30%', 'colspan' => 2),
    );

    foreach ($value as $key => $item) {
      $info = array();
      $info[] = t('Access callback: %value', array('%value' => $item['access callback']));
      if ($item['weight']) {
        $info[] = t('Weight: %value', array('%value' => $item['weight']));
      }
      if ($item['access arguments']) {
        $info[] = t('Access arguments: %value', array('%value' => $item['access arguments']));
      }
      $info = theme('item_list', array('items' => $info));

      $row_links = $variables['element']->getSecondaryLinks(array('key' => $key));
      $row_links = array('data' => theme('links__ctools_dropbutton', array('links' => $row_links, 'attributes' => array('class' => array('links', 'inline')))), 'class' => array('forge-operations'));

      $rows[] = array(
        $item['title'] . '<div class="description">' . $key . '</div>',
        array('data' => $info, 'colspan' => 2),
        $row_links,
      );
    }
  } else {
    $rows[] = array(array('data' => t('no categories found'), 'colspan' => 4));
    $class[] = 'forge-element-empty';
  }

  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));
}
