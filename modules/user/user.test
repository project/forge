<?php

/**
 * @file
 * Tests for user elements.
 */

/**
 * User hooks testing class.
 */
class ForgeElementUserTestCase extends ForgeWebTestCase {
  protected $user;

  public static function getInfo() {
    return array(
      'name' => 'User element',
      'description' => 'User element API.',
      'group' => 'Forge',
    );
  }

  public function setUp() {
    parent::setUp(array('ctools', 'forge'));
    $this->user = $this->drupalCreateUser(array('access forge'));
  }

  /**
   * Testing info hook_user_operations element.
   */
  public function testHookUserOperations() {
    $this->drupalLogin($this->user);
    $name = $this->randomName();
    $machine_name = drupal_strtolower('fp' . $name);
    $project = $this->forgeCreateProject($name);

    $this->drupalGet('admin/forge/project/' . $machine_name . '/edit');
    $edit = array(
      'element[key]' => 'op0',
      'element[label]' => 'Example operation 0',
      'element[callback]' => $machine_name . '_user_operations_op0',
    );
    $this->drupalPost('admin/forge/' . $machine_name . '/nojs/element/user/hook_user_operations/add', $edit, t('Update'), array('query' => array('debug' => 1)));
    $edit = array(
      'element[key]' => 'op1',
      'element[label]' => 'Example operation 1',
      'element[callback]' => $machine_name . '_user_operations_op1',
      'element[callback arguments]' => '$accounts, TRUE',
    );
    $this->drupalPost('admin/forge/' . $machine_name . '/nojs/element/user/hook_user_operations/add', $edit, t('Update'), array('query' => array('debug' => 1)));
    $expected = array(
      'op0' => array(
        'key' => 'op0',
        'label' => 'Example operation 0',
        'callback' => $machine_name . '_user_operations_op0',
        'callback arguments' => '',
      ),
      'op1' => array(
        'key' => 'op1',
        'label' => 'Example operation 1',
        'callback' => $machine_name . '_user_operations_op1',
        'callback arguments' => '$accounts, TRUE',
      ),
    );
    $this->assertCommand($expected, t('User operation addition test passed.'));

    $edit = array(
      'element[key]' => 'op1',
      'element[label]' => 'Example operation 1',
      'element[callback]' => $machine_name . '_user_operations_op1',
      'element[callback arguments]' => '$accounts, FALSE',
    );
    $this->drupalPost('admin/forge/' . $machine_name . '/nojs/element/user/hook_user_operations/edit', $edit, t('Update'), array('query' => array('key' => 'op1', 'debug' => 1)));
    $expected['op1']['callback arguments'] = '$accounts, FALSE';
    $this->assertCommand($expected, t('User operation editing test passed.'));

    $this->drupalGet('admin/forge/' . $machine_name . '/element/user/hook_user_operations/delete', array('query' => array('key' => 'op1', 'debug' => TRUE)));
    unset($expected['op1']);
    $this->assertCommand($expected, t('User operation deletion test passed.'));

    $this->drupalGet('admin/forge/' . $machine_name . '/element/user/hook_user_operations/clear', array('query' => array('debug' => TRUE)));
    $this->assertCommand(array(), t('User operations clearing test passed.'));
  }

  /**
   * Testing info hook_user_categories element.
   */
  public function testHookUserCategories() {
    $this->drupalLogin($this->user);
    $name = $this->randomName();
    $machine_name = drupal_strtolower('fp' . $name);
    $project = $this->forgeCreateProject($name);

    $this->drupalGet('admin/forge/project/' . $machine_name . '/edit');
    $edit = array(
      'element[key]' => 'cat0',
      'element[title]' => 'Example category 0',
      'element[weight]' => '0',
      'element[access callback]' => $machine_name . '_user_category_access',
    );
    $this->drupalPost('admin/forge/' . $machine_name . '/nojs/element/user/hook_user_categories/add', $edit, t('Update'), array('query' => array('debug' => 1)));
    $edit = array(
      'element[key]' => 'cat1',
      'element[title]' => 'Example category 1',
      'element[weight]' => '0',
      'element[access callback]' => $machine_name . '_user_category_access',
      'element[access arguments]' => "1, 'foo'",
    );
    $this->drupalPost('admin/forge/' . $machine_name . '/nojs/element/user/hook_user_categories/add', $edit, t('Update'), array('query' => array('debug' => 1)));
    $expected = array(
      'cat0' => array(
        'key' => 'cat0',
        'title' => 'Example category 0',
        'weight' => '0',
        'access callback' => $machine_name . '_user_category_access',
        'access arguments' => '',
      ),
      'cat1' => array(
        'key' => 'cat1',
        'title' => 'Example category 1',
        'weight' => '0',
        'access callback' => $machine_name . '_user_category_access',
        'access arguments' => "1, 'foo'",
      ),
    );
    $this->assertCommand($expected, t('User category addition test passed.'), array());

    $edit = array(
      'element[key]' => 'cat1',
      'element[title]' => 'Example category 1',
      'element[weight]' => 5,
      'element[access callback]' => $machine_name . '_user_category_access',
      'element[access arguments]' => "1, 'bar'",
    );
    $this->drupalPost('admin/forge/' . $machine_name . '/nojs/element/user/hook_user_categories/edit', $edit, t('Update'), array('query' => array('key' => 'cat1', 'debug' => 1)));
    $expected['cat1']['weight'] = 5;
    $expected['cat1']['access arguments'] = "1, 'bar'";
    $this->assertCommand($expected, t('User category editing test passed.'), array());

    $this->drupalGet('admin/forge/' . $machine_name . '/element/user/hook_user_categories/delete', array('query' => array('key' => 'cat1', 'debug' => TRUE)));
    unset($expected['cat1']);
    $this->assertCommand($expected, t('User category deletion test passed.'));

    $this->drupalGet('admin/forge/' . $machine_name . '/element/user/hook_user_categories/clear', array('query' => array('debug' => TRUE)));
    $this->assertCommand(array(), t('User categories clearing test passed.'));
  }
}
