<?php

/**
 * Returns HTML for presentation of hode info hook.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Node_Info object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_node_info($variables) {
  $header = $rows = array();

  $class = array('forge-element', 'forge-element-hook-node-info');
  $header_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE, 'hook' => TRUE)), 'width' => '70%', 'colspan' => 2),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );

  if ($value = $variables['element']->value) {
    $rows[] = array(
      array('data' => '<strong>' . t('Name') . '</strong>', 'width' => '30%'),
      array('data' => '<strong>' . t('Attributes') . '</strong>', 'width' => '40%'),
      array('data' => '', 'width' => '30%', 'colspan' => 2),
    );

    foreach ($value as $key => $item) {
      $info = array();
      $info[] = t('Machine name: %value', array('%value' => $item['machine_name']));
      $info[] = t('Base: %value', array('%value' => $item['base']));
      $info[] = t('Description: %value', array('%value' => $item['description']));
      if ($item['help']) {
        $info[] = t('Help: %value', array('%value' => $item['help']));
      }
      if ($item['has_title']) {
        $info[] = t('Has title: %value', array('%value' => t('Yes')));
        $info[] = t('Title: %value', array('%value' => $item['title_label']));
      }
      if ($item['locked']) {
        $info[] = t('Locked: %value', array('%value' => t('Yes')));
      }
      $info = theme('item_list', array('items' => $info));

      $row_links = $variables['element']->getSecondaryLinks(array('key' => $key));
      $row_links = array('data' => theme('links__ctools_dropbutton', array('links' => $row_links, 'attributes' => array('class' => array('links', 'inline')))), 'class' => array('forge-operations'));

      $rows[] = array(
        $item['name'],
        $info,
        '',
        $row_links,
      );
    }
  } else {
    $rows[] = array(array('data' => t('no node info found'), 'colspan' => 4));
    $class[] = 'forge-element-empty';
  }

  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));
}
