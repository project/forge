<?php

/**
 * @file
 * Provides Forge hooks for node.module.
 */

/**
 * Implements hook_forge_group().
 */
function node_forge_group() {
  $group['node'] = $group['theme_node'] = array(
    'title' => 'Node',
  );
  $group['node_type'] = array(
    'title' => 'Node type',
  );
  return $group;
}

/**
 * Implements hook_forge_element().
 */
function node_forge_element() {
  // Module support.
  $element['hook_node_load'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Load node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$nodes, $types',
        Forge::CORE_SUPPORT_8x => '$nodes',
      ),
    ),
    'info' => array(
      'description' => 'Act on arbitrary nodes being loaded from the database.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_load/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_load/8',
      ),
    ),
  );
  $hook_node_args = array(
    Forge::CORE_SUPPORT_7x => '$node',
    Forge::CORE_SUPPORT_8x => '\\Drupal\\Core\\Entity\\EntityInterface $node',
  );
  $element['hook_node_insert'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Insert node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => $hook_node_args,
    ),
    'info' => array(
      'description' => 'Respond to creation of a new node.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_insert/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_insert/8',
      ),
    ),
  );
  $element['hook_node_update'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Update node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => $hook_node_args,
    ),
    'info' => array(
      'description' => 'Respond to updates to a node.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_update/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_update/8',
      ),
    ),
  );
  $element['hook_node_delete'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Delete node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => $hook_node_args,
    ),
    'info' => array(
      'description' => 'Respond to node deletion.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_delete/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_delete/8',
      ),
    ),
  );
  $element['hook_node_revision_delete'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Delete node revision',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => $hook_node_args,
    ),
    'info' => array(
      'description' => 'Respond to deletion of a node revision.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_revision_delete/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_revision_delete/8',
      ),
    ),
  );
  $element['hook_node_view'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'View node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$node, $view_mode, $langcode',
        Forge::CORE_SUPPORT_8x => '\\Drupal\\Core\\Entity\\EntityInterface $node, \\Drupal\\Core\\Entity\\Display\\EntityViewDisplayInterface $display, $view_mode, $langcode',
      ),
    ),
    'info' => array(
      'description' => 'Act on a node that is being assembled before rendering.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_view/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_view/8',
      ),
    ),
  );
  $element['hook_node_view_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node view alteration',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$build',
        Forge::CORE_SUPPORT_8x => '&$build, \\Drupal\\Core\\Entity\\EntityInterface $node, \\Drupal\\Core\\Entity\\Display\\EntityViewDisplayInterface $display',
      ),
    ),
    'info' => array(
      'description' => 'Alter the results of node_view().',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_view_alter/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_view_alter/8',
      ),
    ),
  );
  $element['hook_node_presave'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Presave node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$node',
        Forge::CORE_SUPPORT_8x => '\\Drupal\\Core\\Entity\\EntityInterface $node',
      ),
    ),
    'info' => array(
      'description' => 'Act on a node being inserted or updated.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_presave/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_presave/8',
      ),
    ),
  );
  $element['hook_node_prepare'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Prepare node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'node',
    'options' => array(
      'args' => '$node',
    ),
    'info' => array(
      'description' => 'Act on a node object about to be shown on the add/edit form.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_prepare/7',
      ),
    ),
  );
  $element['hook_load'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Load node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node',
    'options' => array(
      'args' => '$node',
    ),
    'info' => array(
      'description' => 'Act on nodes being loaded from the database.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21node.php/function/hook_load/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_load/7',
      ),
    ),
  );
  $element['hook_insert'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Insert node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node',
    'options' => array(
      'args' => '$node',
    ),
    'info' => array(
      'description' => 'Respond to creation of a new node.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21node.php/function/hook_insert/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_insert/7',
      ),
    ),
  );
  $element['hook_update'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Update node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node',
    'options' => array(
      'args' => '$node',
    ),
    'info' => array(
      'description' => 'Respond to updates to a node.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21node.php/function/hook_update/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_update/7',
      ),
    ),
  );
  $element['hook_delete'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Delete node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '&$node',
        Forge::CORE_SUPPORT_7x => '$node',
      ),
    ),
    'info' => array(
      'description' => 'Respond to node deletion.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21node.php/function/hook_delete/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_delete/7',
      ),
    ),
  );
  $element['hook_view'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'View node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '$node, $teaser = FALSE, $page = FALSE',
        Forge::CORE_SUPPORT_7x => '$node, $view_mode',
      ),
    ),
    'info' => array(
      'description' => 'Display a node.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21node.php/function/hook_view/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_view/7',
      ),
    ),
  );
  $element['hook_prepare'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Prepare node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '&$node',
        Forge::CORE_SUPPORT_7x => '$node',
      ),
    ),
    'info' => array(
      'description' => 'Act on a node object about to be shown on the add/edit form.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21node.php/function/hook_prepare/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_prepare/7',
      ),
    ),
  );

  $element['hook_node_access'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node access',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$node, $op, $account',
        Forge::CORE_SUPPORT_8x => '\\Drupal\\node\\NodeInterface $node, $op, $account, $langcode',
      ),
    ),
    'info' => array(
      'description' => 'Control access to a node.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_access/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_access/8',
      ),
    ),
  );
  $element['hook_node_access_records'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node permissions',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '$node',
        Forge::CORE_SUPPORT_7x => '$node',
        Forge::CORE_SUPPORT_8x => '\\Drupal\\node\\NodeInterface $node',
      ),
    ),
    'info' => array(
      'description' => 'Set permissions for a node to be written to the database.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_node_access_records/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_access_records/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_access_records/8',
      ),
    ),
  );
  $element['hook_node_access_records_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node permission alteration',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '&$grants, $node',
        Forge::CORE_SUPPORT_8x => '&$grants, Drupal\Core\Entity\EntityInterface $node',
      ),
    ),
    'info' => array(
      'description' => 'Alter permissions for a node before it is written to the database.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_access_records_alter/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_access_records_alter/8',
      ),
    ),
  );
  $element['hook_node_grants'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node grants',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'node',
    'options' => array(
      'args' => '$account, $op',
    ),
    'info' => array(
      'description' => 'Inform the node access system what permissions the user has.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_node_grants/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_grants/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_grants/8',
      ),
    ),
  );
  $element['hook_node_grants_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node grants alteration',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => '&$grants, $account, $op',
    ),
    'info' => array(
      'description' => 'Alter user access rules when trying to view, edit or delete a node.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_grants_alter/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_grants_alter/8',
      ),
    ),
  );

  $element['hook_form'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node form',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '&$node, $form_state',
        Forge::CORE_SUPPORT_7x => '$node, &$form_state',
      ),
    ),
    'info' => array(
      'description' => 'Display a node editing form.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21node.php/function/hook_form/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_form/7',
      ),
    ),
  );
  $hook_node_form_handler_args = array(
    Forge::CORE_SUPPORT_7x => '$node, $form, &$form_state',
    Forge::CORE_SUPPORT_8x => '\\Drupal\\Core\\Entity\\EntityInterface $node, $form, &$form_state',
  );
  $element['hook_node_validate'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node form validate',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => $hook_node_form_handler_args,
    ),
    'info' => array(
      'description' => 'Perform node validation before a node is created or updated.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_validate/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_validate/8',
      ),
    ),
  );
  $element['hook_node_submit'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node form submit',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => $hook_node_form_handler_args,
    ),
    'info' => array(
      'description' => 'Act on a node after validated form values have been copied to it.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_submit/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_submit/8',
      ),
    ),
  );
  $element['hook_validate'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node form validate',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '$node, &$form',
        Forge::CORE_SUPPORT_7x => '$node, $form, &$form_state',
      ),
    ),
    'info' => array(
      'description' => 'Perform node validation before a node is created or updated.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21node.php/function/hook_validate/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_validate/7',
      ),
    ),
  );

  $element['hook_node_operations'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node operations',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node',
    'info' => array(
      'description' => 'Add mass node operations.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_node_operations/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_operations/7',
      ),
    ),
  );
  $element['hook_ranking'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node ranking',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'info' => array(
      'description' => 'Provide additional methods of scoring for core search results for nodes.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_ranking/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_ranking/8',
      ),
    ),
  );

  $hook_search_args = array(
    Forge::CORE_SUPPORT_7x => '$node',
    Forge::CORE_SUPPORT_8x => '\\Drupal\\Core\\Entity\\EntityInterface $node, $langcode',
  );
  $element['hook_node_update_index'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Update node index',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => $hook_search_args,
    ),
    'info' => array(
      'description' => 'Act on a node being indexed for searching.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_update_index/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_update_index/8',
      ),
    ),
  );
  $element['hook_node_search_result'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Node search result view',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node',
    'options' => array(
      'args' => $hook_search_args,
    ),
    'info' => array(
      'description' => 'Act on a node being displayed as a search result.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_search_result/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_search_result/8',
      ),
    ),
  );

  $element['hook_node_info'] = array(
    'class' => 'Forge_Element_Hook_Node_Info',
    'name' => 'Node type',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'node_type',
    'info' => array(
      'description' => 'Define module-provided node types.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21node.php/function/hook_node_info/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_info/7',
      ),
    ),
  );
  $hook_node_type_args = array(
    Forge::CORE_SUPPORT_7x => '$info',
    Forge::CORE_SUPPORT_8x => '\\Drupal\\node\\NodeTypeInterface $type',
  );
  $element['hook_node_type_insert'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Insert node type',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node_type',
    'options' => array(
      'args' => $hook_node_type_args,
    ),
    'info' => array(
      'description' => 'Respond to node type creation.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_type_insert/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_type_insert/8',
      ),
    ),
  );
  $element['hook_node_type_update'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Update node type',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node_type',
    'options' => array(
      'args' => $hook_node_type_args,
    ),
    'info' => array(
      'description' => 'Respond to node type updates.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_type_update/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_type_update/8',
      ),
    ),
  );
  $element['hook_node_type_delete'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Delete node type',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'node_type',
    'options' => array(
      'args' => $hook_node_type_args,
    ),
    'info' => array(
      'description' => 'Respond to node type deletion.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_type_delete/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21node%21node.api.php/function/hook_node_type_delete/8',
      ),
    ),
  );

  // Theme support.
  $element['theme_node_form'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme node submission form',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_6x,
    'group' => 'theme_node',
    'options' => array(
      'args' => '$form',
    ),
    'info' => array(
      'description' => 'Present a node submission form.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21node%21node.pages.inc/function/theme_node_form/6',
      ),
    ),
  );

  $element['theme_node_preview'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme node preview',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '$node',
        Forge::CORE_SUPPORT_7x => '$variables',
      ),
    ),
    'info' => array(
      'description' => 'Display a node preview for display during node creation and editing.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21node%21node.pages.inc/function/theme_node_preview/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.pages.inc/function/theme_node_preview/7',
      ),
    ),
  );

  $element['theme_node_list'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme listing of links to nodes',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_6x,
    'group' => 'theme_node',
    'options' => array(
      'args' => '$items, $title = NULL',
    ),
    'info' => array(
      'description' => 'Format a listing of links to nodes.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21node%21node.module/function/theme_node_list/6',
      ),
    ),
  );

  $element['theme_node_submitted'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme submitted',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_6x,
    'group' => 'theme_node',
    'options' => array(
      'args' => '$node',
    ),
    'info' => array(
      'description' => 'Format the "Submitted by username on date/time" for each node',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21node%21node.module/function/theme_node_submitted/6',
      ),
    ),
  );

  $element['theme_node_log_message'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme a log message.',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_6x,
    'group' => 'theme_node',
    'options' => array(
      'args' => '$log',
    ),
    'info' => array(
      'description' => 'Theme a log message.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21node%21node.module/function/theme_node_log_message/6',
      ),
    ),
  );

  $element['theme_node_recent_block'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme list of recent nodes',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'theme_node',
    'options' => array(
      'args' => '$variables',
    ),
    'info' => array(
      'description' => 'Returns HTML for a list of recent content.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.module/function/theme_node_recent_block/7',
      ),
    ),
  );

  $element['theme_node_recent_content'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Theme list of recent nodes (in block)',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'theme_node',
    'options' => array(
      'args' => '$variables',
    ),
    'info' => array(
      'description' => 'Returns HTML for a recent node to be displayed in the recent content block.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.module/function/theme_node_recent_content/7',
      ),
    ),
  );

  $element['node.tpl.php'] = array(
    'class' => 'Forge_Element_Template',
    'name' => 'Theme implementation to display a node',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme',
    'options' => array(
      'args' => 'modules/node/node.tpl.php',
    ),
    'info' => array(
      'description' => 'Default theme implementation to display a node.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21node%21node.tpl.php/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21node%21node.tpl.php/7',
      ),
    ),
  );

  return $element;
}

/**
 * Implements hook_forge_theme().
 */
function node_forge_theme($existing, $type, $theme, $path) {
  return array(
    'forge_element_present_hook_node_info' => array(
      'variables' => array('element' => NULL),
    ),
  );
}

/**
 * Returns whether a node type already exists.
 *
 * @param  $value The machine name of the node type
 * @return boolean
 */
function node_type_info_exists($value) {
  return FALSE;
}
