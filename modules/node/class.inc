<?php

/**
 * @file
 * Class implementation of a node hook elements.
 */

/**
 * Node type definition hook class.
 *
 * @see Forge_Element_Hook
 * @see https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_info/7
 */
class Forge_Element_Hook_Node_Info extends Forge_Element_Hook {
  public $presentTheme = 'forge_element_present_hook_node_info';

  /**
   * Returns primary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getPrimaryLinks() {
    $links = array(
      forge_popup_link('add', $this, 'add'),
      forge_link('clear', $this, 'clear'),
    );

    return $links;
  }

  /**
   * Returns secondary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getSecondaryLinks($args = array()) {
    $links = array(
      forge_popup_link('edit', $this, 'edit', array('key' => $args['key'])),
      forge_link('delete', $this, 'delete', array('key' => $args['key'])),
    );

    return $links;
  }

  /**
   * Deletes a value specified by 'key' query parameter.
   *
   * @ingroup operations
   */
  public function delete() {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      unset($this->value[$query['key']]);
    }
  }

  /**
   * Returns Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $input = $this->value[$query['key']];
    }

    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => isset($input['name']) ? $input['name'] : '',
      '#required' => TRUE,
      '#description' => t('The human-readable name of the node type.'),
    );
    $form['machine_name'] = array(
      '#type' => 'machine_name',
      '#default_value' => isset($input['machine_name']) ? $input['machine_name'] : '',
      '#machine_name' => array(
        'exists' => 'node_type_info_exists',
        'source' => array('name'),
      ),
    );
    $form['base'] = array(
      '#type' => 'textfield',
      '#title' => t('Base'),
      '#default_value' => isset($input['base']) ? $input['base'] : '',
      '#required' => TRUE,
      '#description' => t('The base string used to construct callbacks corresponding to this node type (for example, if base is defined as example_foo, then example_foo_insert will be called when inserting a node of that type). This string is usually the name of the module, but not always.'),
    );

    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($input['description']) ? $input['description'] : '',
      '#required' => TRUE,
      '#description' => t('A brief description of the node type.'),
    );

    $form['help'] = array(
      '#type' => 'textarea',
      '#title' => t('Help'),
      '#default_value' => isset($input['help']) ? $input['help'] : '',
      '#description' => t('Help information shown to the user when creating a node of this type.'),
    );

    $form['has_title'] = array(
      '#type' => 'checkbox',
      '#title' => t('Has title'),
      '#default_value' => isset($input['has_title']) ? $input['has_title'] : '',
      '#description' => t('A Boolean indicating whether or not this node type has a title field.'),
      '#ajax' => array(
        'callback' => 'forge_element_form_callback',
        'wrapper' => 'element-wrapper',
        'effect' => 'none',
      ),
    );
    if (isset($input)) {
      if (isset($input['has_title']) && $input['has_title']) {
        $form['title_label'] = array(
          '#type' => 'textfield',
          '#title' => t('Title label'),
          '#default_value' => isset($input['title_label']) ? $input['title_label'] : '',
          '#description' => t('The label for the title field of this content type.'),
        );
      }
    }

    $form['locked'] = array(
      '#type' => 'checkbox',
      '#title' => t('Locked'),
      '#default_value' => isset($input['locked']) ? $input['locked'] : '',
      '#description' => t('A boolean indicating whether the administrator can change the machine name of this type.'),
    );

    return $form;
  }

  /**
   * On-submit processing of the element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $this->value = Forge::arrayKeyReplace($this->value, $query['key'], $input['machine_name'], $input);
    } else {
      $this->value[$input['machine_name']] = $input;
    }
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $types = $this->value;
      foreach ($types as &$type) {
        unset($type['machine_name']);
        $type['name'] = "t('{$type['name']}')";
        $type['description'] = "t('{$type['description']}')";
        if (!empty($type['help'])) {
          $type['help'] = "t('{$type['help']}')";
        } else {
          unset($type['help']);
        }
        if (empty($type['has_title'])) {
          unset($type['has_title']);
          unset($type['title_label']);
        } elseif (!empty($type['title_label'])) {
          $type['title_label'] = "t('{$type['title_label']}')";
        }
        if (empty($type['locked'])) {
          unset($type['locked']);
        }
      }
      $body = theme('forge_array', array('array' => $types));
      $body = preg_replace("/'(t\(.*\))'/", '$1', $body);
      $body = str_replace("\'", "'", $body);
      $body = "return $body;";

      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'body' => $body));
    }
  }
}
