<?php

/**
 * @file
 * Class implementation of .info file elements.
 */

/**
 * .info file elements class.
 *
 * @see Forge_Element
 * @see https://drupal.org/node/542202
 * @see https://drupal.org/node/231036
 * @see https://drupal.org/node/171205
 * @see https://drupal.org/node/171206
 */
class Forge_Element_Info extends Forge_Element {
  public $renderDestination = 'info';

  /**
   * Render the element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this));
    }
  }
}

/**
 * Info element class with multiple values.
 *
 * @see Forge_Element_Boolean
 */
class Forge_Element_Info_Boolean extends Forge_Element_Boolean {
  public $renderDestination = 'info';

  /**
   * Render the element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value == 'TRUE') {
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this));
    }
  }
}

/**
 * Info element class for PHP version.
 *
 * @see Forge_Element_Info
 */
class Forge_Element_Info_Php extends Forge_Element_Info {
  public $presentTheme = 'forge_element_present_requirement';

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
      '#description' => isset($this->options['description']) ? t($this->options['description']) : '',
    );
    $project = Forge_Project::session($this->project);
    if (($project->core_support & Forge::CORE_SUPPORT_6x) > 0) {
      $form[Forge::CORE_SUPPORT_6x] = array(
        '#type' => 'textfield',
        '#title' => t('Needed PHP version for 6.x branch'),
        '#default_value' => $this->value[Forge::CORE_SUPPORT_6x],
        '#required' => TRUE,
      );
    }
    if (($project->core_support & Forge::CORE_SUPPORT_7x) > 0) {
      $form[Forge::CORE_SUPPORT_7x] = array(
        '#type' => 'textfield',
        '#title' => t('Needed PHP version for 7.x branch'),
        '#default_value' => $this->value[Forge::CORE_SUPPORT_7x],
        '#required' => TRUE,
      );
    }

    return $form;
  }

  /**
   * @see Forge_Element::validate()
   * @ingroup forms
   */
  public function validate($input) {
    if (isset($input[Forge::CORE_SUPPORT_6x])) {
      $d6_min = '4.4.0';
      if ($input[Forge::CORE_SUPPORT_6x] < $d6_min) {
        form_set_error('element][' . Forge::CORE_SUPPORT_6x, t('Wrong value. Drupal !core branch needs a minimum PHP version !version', array('!core' => '6.x', '!version' => $d6_min)));
      }
    }
    if (isset($input[Forge::CORE_SUPPORT_7x])) {
      if ($input[Forge::CORE_SUPPORT_7x] < DRUPAL_MINIMUM_PHP) {
        form_set_error('element][' . Forge::CORE_SUPPORT_7x, t('Wrong value. Drupal !core branch needs a minimum PHP version !version', array('!core' => '7.x', '!version' => DRUPAL_MINIMUM_PHP)));
      }
    }
  }

  /**
   * Render the element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value[$core]) {
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'value' => $this->value[$core]));
    }
  }
}

/**
 * Info element class for Drupal Core version.
 *
 * @see Forge_Element_Info
 */
class Forge_Element_Info_Core extends Forge_Element_Info {
  public $presentTheme = 'forge_element_present_requirement';

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
      '#description' => isset($this->options['description']) ? t($this->options['description']) : '',
    );
    $project = Forge_Project::session($this->project);
    if (($project->core_support & Forge::CORE_SUPPORT_6x) > 0) {
      $form[Forge::CORE_SUPPORT_6x] = array(
        '#type' => 'textfield',
        '#title' => t('Needed core version for 6.x branch'),
        '#default_value' => $this->value[Forge::CORE_SUPPORT_6x],
        '#required' => TRUE,
      );
    }
    if (($project->core_support & Forge::CORE_SUPPORT_7x) > 0) {
      $form[Forge::CORE_SUPPORT_7x] = array(
        '#type' => 'textfield',
        '#title' => t('Needed core version for 7.x branch'),
        '#default_value' => $this->value[Forge::CORE_SUPPORT_7x],
        '#required' => TRUE,
      );
    }

    return $form;
  }

  /**
   * @see Forge_Element::validate()
   * @ingroup forms
   */
  public function validate($input) {
    if (isset($input[Forge::CORE_SUPPORT_6x])) {
      if (!preg_match('/^6\.(x|\d+)$/', $input[Forge::CORE_SUPPORT_6x])) {
        form_set_error('element][' . Forge::CORE_SUPPORT_6x, t('Wrong value. !core branch of module needs Core version !core and derivatives (for example: !core or !example).', array('!core' => '6.x', '!example' => '6.12')));
      }
    }
    if (isset($input[Forge::CORE_SUPPORT_7x])) {
      if (!preg_match('/^7\.(x|\d+)$/', $input[Forge::CORE_SUPPORT_7x])) {
        form_set_error('element][' . Forge::CORE_SUPPORT_7x, t('Wrong value. !core branch of module needs Core version !core and derivatives (for example: !core or !example).', array('!core' => '7.x', '!example' => '7.12')));
      }
    }
  }

  /**
   * Render the element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value[$core]) {
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'value' => $this->value[$core]));
    }
  }
}

/**
 * Info element class with multiple values.
 *
 * @see Forge_Element_Multiple
 */
class Forge_Element_Info_Multiple extends Forge_Element_Multiple {
  public $presentTheme = 'forge_element_present_multiple';
  public $renderTheme = 'forge_element_render_multiple';
  public $renderDestination = 'info';

  /**
   * Render the element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this));
    }
  }
}

/**
 * Info regions elment class.
 *
 * @see Forge_Element_Info_Multiple
 */
class Forge_Element_Info_Multiple_Regions extends Forge_Element_Info_Multiple {
  /**
   * Return a HTML presentation of the element (preview).
   *
   * @return string
   * @ingroup presentation
   */
  public function getPresent() {
    return theme($this->presentTheme, array('element' => $this, 'display_keys' => TRUE));
  }

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Machine name'),
      '#default_value' => isset($query['key']) ? $query['key'] : '',
      '#required' => TRUE,
    );
    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']] : '',
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * On-submit processing of element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $key = $query['key'];
      $this->value = Forge::arrayKeyReplace($this->value, $query['key'], $input['key'], $input['value']);
    } else {
      $this->value[$input['key']] = $input['value'];
    }
  }

  /**
   * Render the element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'display_keys' => TRUE));
    }
  }
}

/**
 * Info stylesheets element class.
 *
 * @see Forge_Element_Info_Multiple
 */
class Forge_Element_Info_Multiple_Stylesheets extends Forge_Element_Info_Multiple {
  public $presentTheme = 'forge_element_present_info_multiple_stylesheets';
  public $renderTheme = 'forge_element_render_info_multiple_stylesheets';

  /**
   * Returns primary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getPrimaryLinks($args = array()) {
    $links = array(
      forge_popup_link('add', $this, 'add', array('media' => $args['media'])),
      forge_link('clear', $this, 'clear', array('media' => $args['media'])),
    );

    return $links;
  }

  /**
   * Returns secondary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getSecondaryLinks($args = array()) {
    $links = array(
      forge_popup_link('edit', $this, 'edit', array('media' => $args['media'], 'key' => $args['key'])),
      forge_link('delete', $this, 'delete', array('media' => $args['media'], 'key' => $args['key'])),
    );

    return $links;
  }

  /**
   * Clears values of the element.
   *
   * @see Forge_Element::clear()
   * @ingroup operations
   */
  public function clear() {
    $query = drupal_get_query_parameters();
    $media = $query['media'];
    $this->value[$media] = array();
  }

  /**
   * Deletes specified value.
   *
   * @ingroup operations
   */
  public function delete() {
    $query = drupal_get_query_parameters();
    $media = $query['media'];
    if (isset($query['media']) && isset($query['key'])) {
      $key = $query['key'];
      unset($this->value[$media][$key]);
    }
  }

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    $form = parent::getForm($input);
    $form['#default_value'] = '';
    $media = $query['media'];
    if (isset($query['key'])) {
      $key = $query['key'];
      if (isset($this->value[$media][$key])) {
        $form['#default_value'] = $this->value[$media][$key];
      }
    }

    return $form;
  }

  /**
   * On-submit processing of element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    $media = $query['media'];
    if (isset($query['key'])) {
      $key = $query['key'];
      $this->value[$media][$key] = $input;
    } else {
      $this->value[$media][] = $input;
    }
  }

  /**
   * @see Forge_Element::getFiles()
   * @ingroup render
   */
  public function getFiles() {
    $files = array();
    foreach ($this->value as $media => $media_files) {
      $files = array_merge($files, $media_files);
    }

    return $files;
  }

  /**
   * Render the element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this));
  }
}

/**
 * Info javascript element class.
 *
 * @see Forge_Element_Info_Multiple
 */
class Forge_Element_Info_Multiple_Js extends Forge_Element_Info_Multiple {
  /**
   * @see Forge_Element::getFiles()
   * @ingroup render
   */
  public function getFiles() {
    return $this->value;
  }
}

/**
 * Info regions elment class.
 *
 * @see Forge_Element_Info_Multiple
 */
class Forge_Element_Info_Multiple_Files extends Forge_Element_Info_Multiple {
  /**
   * @see Forge_Element::getFiles()
   * @ingroup render
   */
  public function getFiles() {
    return $this->value;
  }
}
