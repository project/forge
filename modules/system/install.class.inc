<?php

/**
 * @file
 * Class implementation of a install hook elements.
 */

/**
 * Install elements class.
 */
class Forge_Element_Hook_Install extends Forge_Element_Hook {
  public $renderDestination = 'install';

  /**
   * @see Forge_Element::getFiles()
   * @ingroup render
   */
  public function getFiles() {
    if ($this->value) {
      return array($this->project . '.install');
    }
    return array();
  }
}

/**
 * hook_install element class.
 */
class Forge_Element_Hook_Install_Install extends Forge_Element_Hook_Install {
  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $body = '';
      $project = Forge_Project::session($this->project);
      if (!($project instanceof Forge_Project)) {
        return;
      }
      if ($core == Forge::CORE_SUPPORT_6x && $project->data['hook_schema']) {
        $body = "drupal_install_schema('{$this->project}');";
      }
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'args' => '', 'body' => $body));
    }
  }
}

/**
 * hook_uninstall element class.
 */
class Forge_Element_Hook_Install_Uninstall extends Forge_Element_Hook_Install {
  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $body = '';
      $project = Forge_Project::session($this->project);
      if (!($project instanceof Forge_Project)) {
        return;
      }
      if ($core == Forge::CORE_SUPPORT_6x && $project->data['hook_schema']) {
        $body = "drupal_uninstall_schema('{$this->project}');";
      }
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'args' => '', 'body' => $body));
    }
  }
}

/**
 * Database schema hook class.
 *
 * @see Forge_Element_Hook
 * @see https://drupal.org/node/146939
 */
class Forge_Element_Hook_Schema extends Forge_Element_Hook_Install {
  public $presentTheme = 'forge_element_present_hook_schema';
  public $renderTheme = 'forge_element_render_hook_schema';

  /**
   * Return primary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getPrimaryLinks($args = array()) {
    $links = array(
      forge_popup_link('add table', $this, 'add'),
      forge_link('clear', $this, 'clear'),
    );

    return $links;
  }

  /**
   * Return secondary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getSecondaryLinks($args = array(), $key) {
    $title = '';
    switch ($key) {
      case 'pkey': case 'ukey': case 'fkey':
        $title = 'key';
        break;
      default:
        $title = $key;
        break;
    }
    if ($args[$key] == '0') {
      $title0 = 'add ' . $title;
      $op0 = 'add';
      $op1 = 'clear';
    } else {
      $title0 = 'edit';
      $op0 = 'edit';
      $op1 = 'delete';
    }
    $links = array(
      forge_popup_link($title0, $this, $op0, $args),
      forge_link($op1, $this, $op1, $args),
    );

    return $links;
  }

  /**
   * Return a operations with a table.
   *
   * @return array
   * @ingroup operations
   */
  public function getTableLinks($args = array()) {
    $links = array(
      forge_popup_link('edit table', $this, 'edit', $args),
      forge_link('delete', $this, 'delete', $args),
    );

    return $links;
  }

  /**
   * Return operations with a field.
   *
   * @return array
   * @ingroup operations
   */
  public function getFieldLinks($args = array()) {
    return $this->getSecondaryLinks($args, 'field');
  }

  /**
   * Return operations with a primary keys.
   *
   * @return array
   * @ingroup operations
   */
  public function getPkeyLinks($args = array()) {
    if (empty($this->value[$args['table']]['primary key'])) {
      $title = 'add key';
      $op = 'add';
    } else {
      $title = 'edit key';
      $op = 'edit';
    }
    $links = array(
      forge_popup_link($title, $this, $op, $args),
      forge_link('clear', $this, 'clear', $args),
    );

    return $links;
  }

  /**
   * Return operations with a unique key.
   *
   * @return array
   * @ingroup operations
   */
  public function getUkeyLinks($args = array()) {
    return $this->getSecondaryLinks($args, 'ukey');
  }

  /**
   * Return operations with a unique key.
   *
   * @return array
   * @ingroup operations
   */
  public function getIndexLinks($args = array()) {
    return $this->getSecondaryLinks($args, 'index');
  }

  /**
   * Return operations with a unique key.
   *
   * @return array
   * @ingroup operations
   */
  public function getFkeyLinks($args = array()) {
    return $this->getSecondaryLinks($args, 'fkey');
  }

  /**
   * Return operations with a table options.
   *
   * @return array
   * @ingroup operations
   */
  public function getToLinks($args = array()) {
    $keys = array('mysql_suffix', 'mysql_engine', 'mysql_character_set', 'collation');
    $empty = TRUE;
    foreach ($keys as $key) {
      if (!empty($this->value[$args['table']][$key])) {
        $empty = FALSE;
        break;
      }
    }
    $op = $empty ? 'add' : 'edit';
    $links = array(
      forge_popup_link($op, $this, $op, $args),
      forge_link('clear', $this, 'clear', $args),
    );

    return $links;
  }

  /**
   * Clear values of the element.
   *
   * @see Forge_Element::clear()
   * @ingroup operations
   */
  public function clear() {
    $query = drupal_get_query_parameters();
    if (isset($query['table'])) {
      if (isset($query['field'])) {
        $this->value[$query['table']]['fields'] = array();
      } elseif (isset($query['pkey'])) {
        $this->value[$query['table']]['primary key'] = array();
      } elseif (isset($query['ukey'])) {
        $this->value[$query['table']]['unique keys'] = array();
      } elseif (isset($query['index'])) {
        $this->value[$query['table']]['indexes'] = array();
      } elseif (isset($query['fkey'])) {
        $this->value[$query['table']]['foreign keys'] = array();
      } elseif (isset($query['option'])) {
        $this->value[$query['table']]['mysql_suffix'] = '';
        $this->value[$query['table']]['mysql_engine'] = '';
        $this->value[$query['table']]['mysql_character_set'] = '';
        $this->value[$query['table']]['collation'] = '';
      }
    } else {
      $this->value = array();
    }
  }

  /**
   * Delete specified value.
   *
   * @ingroup operations
   */
  public function delete() {
    $query = drupal_get_query_parameters();
    if (isset($query['table'])) {
      if (isset($query['field'])) {
        unset($this->value[$query['table']]['fields'][$query['field']]);
      } elseif (isset($query['pkey'])) {
        $this->value[$query['table']]['primary key'] = array();
      } elseif (isset($query['ukey'])) {
        unset($this->value[$query['table']]['unique keys'][$query['ukey']]);
      } elseif (isset($query['index'])) {
        unset($this->value[$query['table']]['indexes'][$query['index']]);
      } elseif (isset($query['fkey'])) {
        unset($this->value[$query['table']]['foreign keys'][$query['fkey']]);
      } else {
        unset($this->value[$query['table']]);
      }
    }
  }

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );

    if (isset($query['field']) || isset($input['field'])) {
      // Fields processing.
      if (empty($input) && !empty($query['field'])) {
        $input = $this->value[$query['table']]['fields'][$query['field']];
      }
      $this->_fieldForm($form, $input);
    } elseif(isset($query['pkey'])) {
      // Primary keys processing.
      $fields = $this->getFieldsArray($query['table']);
      $form['pkey'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Primary keys'),
        '#options' => $fields,
        '#default_value' => $this->value[$query['table']]['primary key'],
        '#description' => t('Select one or more key column specifiers that form the primary key.'),
      );
    } elseif(isset($query['ukey'])) {
      // Unique keys processing.
      $fields = $this->getFieldsArray($query['table']);
      $form['ukey'] = array(
        '#type' => 'textfield',
        '#title' => t('Unique key name'),
        '#default_value' => $query['ukey'] ? $query['ukey'] : '',
        '#required' => TRUE,
      );
      $ukeys = $this->value[$query['table']]['unique keys'];
      $form['column'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Columns'),
        '#options' => $fields,
        '#default_value' => isset($ukeys[$query['ukey']]) ? $ukeys[$query['ukey']] : array(),
        '#required' => TRUE,
      );
    } elseif(isset($query['index'])) {
      // Indexes processing.
      $fields = $this->getFieldsArray($query['table']);
      $form['index'] = array(
        '#type' => 'textfield',
        '#title' => t('Index name'),
        '#default_value' => $query['index'] ? $query['index'] : '',
        '#required' => TRUE,
      );
      $indexes = $this->value[$query['table']]['indexes'];
      $form['column'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Columns'),
        '#options' => $fields,
        '#default_value' => isset($indexes[$query['index']]) ? $indexes[$query['index']] : array(),
        '#required' => TRUE,
      );
    } elseif(isset($query['fkey']) || isset($input['fkey'])) {
      // Foreign keys processing.
      $fk = $this->value[$query['table']]['foreign keys'];
      if (empty($input) && !empty($query['fkey'])) {
        $input = $fk[$query['fkey']];
        $input['column'] = array('wrapper' => array());
        foreach ($fk[$query['fkey']]['columns'] as $lc => $fc) {
          $input['column']['wrapper'][] = array(
            'lcolumn' => $lc,
            'fcolumn' => $fc,
          );
        }
        $fk = $input['column']['wrapper'];
      } else {
        $fk = array(array());
      }
      $this->_fkeyForm($form, $input, $fk);
    } elseif(isset($query['option'])) {
      $project = Forge_Project::session($this->project);
      if (($project->core_support & Forge::CORE_SUPPORT_6x) > 0) {
        $form['mysql_suffix'] = array(
          '#type' => 'textfield',
          '#title' => t('MySQL suffix'),
          '#default_value' => isset($this->value[$query['table']]['mysql_suffix']) ? $this->value[$query['table']]['mysql_suffix'] : '',
          '#description' => t('Drupal 6 only. In MySQL databases, a string that is added as a suffix in the CREATE TABLE sentence. Example: !value', array('!value' => 'DEFAULT CHARACTER SET UTF8 ENGINE = INNODB AUTO_INCREMENT=3844')),
        );
      }
      if (($project->core_support & Forge::CORE_SUPPORT_7x) > 0) {
        $form['mysql_engine'] = array(
          '#type' => 'textfield',
          '#title' => t('MySQL engine'),
          '#default_value' => isset($this->value[$query['table']]['mysql_engine']) ? $this->value[$query['table']]['mysql_engine'] : '',
          '#description' => t('Drupal 7 only. In MySQL databases, the engine to use instead of the default. Example: !value', array('!value' => 'MyISAM')),
        );
        $form['mysql_character_set'] = array(
          '#type' => 'textfield',
          '#title' => t('MySQL character set'),
          '#default_value' => isset($this->value[$query['table']]['mysql_character_set']) ? $this->value[$query['table']]['mysql_character_set'] : '',
          '#description' => t('Drupal 7 only. In MySQL databases, the character set to use instead of the default. Example: !value', array('!value' => 'UTF8')),
        );
        $form['collation'] = array(
          '#type' => 'textfield',
          '#title' => t('MySQL collaction'),
          '#default_value' => isset($this->value[$query['table']]['collation']) ? $this->value[$query['table']]['collation'] : '',
          '#description' => t('Drupal 7 only. In MySQL databases, the collation to use instead of the default. Example: !value', array('!value' => 'utf8_general_ci')),
        );
      }
    } else {
      // Table processing.
      $form['table'] = array(
        '#type' => 'textfield',
        '#title' => t('Table'),
        '#default_value' => isset($query['table']) ? $query['table'] : '',
        '#required' => TRUE,
      );
      $form['description'] = array(
        '#type' => 'textfield',
        '#title' => t('Table description'),
        '#default_value' => isset($query['table']) ? $this->value[$query['table']]['description'] : '',
        '#description' => t('A string describing this table and its purpose.'),
      );
    }

    return $form;
  }

  /**
   * Modify the element form for editing field.
   *
   * @see Forge_Element_Hook_Schema::getForm()
   * @param  array $form
   * @param  array $input
   * @ingroup forms
   */
  private function _fieldForm(&$form, $input = array()) {
    $form['field'] = array(
      '#type' => 'textfield',
      '#title' => t('Field'),
      '#default_value' => isset($input['field']) ? $input['field'] : '',
      '#required' => TRUE,
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($input['description']) ? $input['description'] : '',
    );
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => array(
        'varchar' => 'VARCHAR',
        'char' => 'CHAR',
        'int' => 'INT',
        'serial' => 'SERIAL (AUTOINCREMENT)',
        'float' => 'FLOAT',
        'numeric' => 'NUMERIC',
        'text' => 'TEXT',
        'blob' => 'BLOB',
        'datetime' => 'DATETIME',
        'custom' => t('Database driver specific type'),
      ),
      '#default_value' => isset($input['type']) ? $input['type'] : '',
      '#required' => TRUE,
      '#ajax' => array(
        'callback' => 'forge_element_form_callback',
        'wrapper' => 'element-wrapper',
        'effect' => 'none',
      ),
      '#description' => t('Generic type of a field.'),
    );

    if (!isset($input['type'])) {
      $input['type'] = 'varchar';
    }
    if (isset($input['type'])) {
      // Database specific type.
      if ($input['type'] == 'custom') {
        $form['custom'] = array(
          '#type' => 'fieldset',
          '#title' => t('Settings of field type for specific database driver'),
        );
        $form['custom']['mysql'] = array(
          '#type' => 'textfield',
          '#title' => t('MySQL'),
          '#description' => t("Example: 'TIME'."),
          '#default_value' => isset($input['custom']['mysql']) ? $input['custom']['mysql'] : '',
        );
        $form['custom']['pgsql'] = array(
          '#type' => 'textfield',
          '#title' => t('PostgreSQL'),
          '#description' => t("Example: 'time without time zone'."),
          '#default_value' => isset($input['custom']['pgsql']) ? $input['custom']['pgsql'] : '',
        );
        $form['custom']['sqlite'] = array(
          '#type' => 'textfield',
          '#title' => t('SQLite'),
          '#default_value' => isset($input['custom']['sqlite']) ? $input['custom']['sqlite'] : '',
        );
        $form['custom']['mssql'] = array(
          '#type' => 'textfield',
          '#title' => t('Microsoft SQL Server'),
          '#description' => t('Supported by additional module.'),
          '#default_value' => isset($input['custom']['mssql']) ? $input['custom']['mssql'] : '',
        );
        $form['custom']['oracle'] = array(
          '#type' => 'textfield',
          '#title' => t('Oracle'),
          '#description' => t('Supported by additional module.'),
          '#default_value' => isset($input['custom']['oracle']) ? $input['custom']['oracle'] : '',
        );
      }

      // Field size and length.
      $size = array();
      if (in_array($input['type'], array('serial', 'int', 'float', 'text'))) {
        $size = array(
          'normal' => 'normal',
          'tiny' => 'tiny',
          'small' => 'small',
          'medium' => 'medium',
          'big' => 'big',
        );
      }
      if (in_array($input['type'], array('numeric', 'varchar', 'char', 'datetime'))) {
        $size = array(
          'normal' => 'normal',
        );
      }
      if ($input['type'] == 'blob') {
        $size = array(
          'normal' => 'normal',
          'big' => 'big',
        );
      }
      if ($size) {
        $form['size'] = array(
          '#type' => 'select',
          '#title' => t('Size'),
          '#options' => $size,
        );
      }
      if (in_array($input['type'], array('char', 'varchar', 'text'))) {
        $form['length'] = array(
          '#type' => 'textfield',
          '#title' => t('Length'),
          '#size' => 10,
          '#default_value' => isset($input['length']) ? $input['length'] : '',
        );
      }

      // Not null flag.
      if (!in_array($input['type'], array('serial'))) {
        $form['not null'] = array(
          '#type' => 'checkbox',
          '#title' => t('Not NULL'),
          '#description' => t('If checked, no NULL values will be allowed in this database column.'),
          '#default_value' => isset($input['not null']) ? $input['not null'] : '',
        );
      }

      // Default value.
      if (!in_array($input['type'], array('serial', 'text', 'blob'))) {
        $form['default'] = array(
          '#type' => 'textfield',
          '#title' => t('Default value'),
          '#default_value' => isset($input['default']) ? $input['default'] : '',
        );
      }

      // 'Numeric' settings.
      if ($input['type'] == 'numeric') {
        $form['precision'] = array(
          '#type' => 'textfield',
          '#title' => t('Precision'),
          '#default_value' => isset($input['precision']) ? $input['precision'] : '',
        );
        $form['scale'] = array(
          '#type' => 'textfield',
          '#title' => t('Scale'),
          '#default_value' => isset($input['scale']) ? $input['scale'] : '',
        );
      }

      // Unsigned flag.
      if (in_array($input['type'], array('int', 'float', 'numeric'))) {
        $form['unsigned'] = array(
          '#type' => 'checkbox',
          '#title' => t('Unsigned'),
          '#default_value' => isset($input['unsigned']) ? $input['unsigned'] : '',
        );
      }

      // Serialise flag.
      if (in_array($input['type'], array('text', 'blob'))) {
        $form['serialize'] = array(
          '#type' => 'checkbox',
          '#title' => t('Serialize'),
          '#default_value' => isset($input['serialize']) ? $input['serialize'] : '',
        );
      }
    }
  }

  /**
   * Modify the element form for editing foreign keys.
   *
   * @see Forge_Element_Hook_Schema::getForm()
   * @param  array $form
   * @param  array $input
   * @param  array $fk
   * @ingroup forms
   */
  private function _fkeyForm(&$form, $input, $fk = array()) {
    $query = drupal_get_query_parameters();
    $form['fkey'] = array(
      '#type' => 'textfield',
      '#title' => t('Foreign key name'),
      '#default_value' => isset($query['fkey']) && $query['fkey'] !== '0' ? $query['fkey'] : (isset($input['fkey']) ? $input['fkey'] : ''),
      '#description' => t("Example: 'fk_user'"),
    );
    $form['ftable'] = array(
      '#type' => 'textfield',
      '#title' => t('Foreign table'),
      '#default_value' => isset($input['table']) ? $input['table'] : (isset($input['ftable']) ? $input['ftable'] : ''),
      '#description' => t("Example: 'user'"),
    );
    if (isset($input['fkey'])) {
      $fk = $input['column']['wrapper'];
      if (isset($input['triggering_element_name'])) {
        if ($input['triggering_element_name'] == 'add_more') {
          $fk[] = array();
        }
        if (preg_match('/remove_(\d+)/', $input['triggering_element_name'], $m)) {
          unset($fk[$m[1]]);
        }
      }
    } elseif (!count($fk)) {
      $fk[] = array();
    }
    $form['column'] = array(
      '#type' => 'fieldset',
      '#title' => t('Columns'),
    );
    $form['column']['wrapper'] = array(
      '#type' => 'item',
      '#prefix' => '<div id="column-wrapper">',
      '#suffix' => '</div>',
      '#theme' => 'forge_element_present_hook_schema_fkey'
    );
    $fields = $this->getFieldsArray(isset($query['table']) ? $query['table'] : $input['table'], FALSE);
    foreach ($fk as $i => $row) {
      $form['column']['wrapper'][$i]['lcolumn'] = array(
        '#type' => 'select',
        '#options' => $fields,
        '#default_value' => isset($row['lcolumn']) ? $row['lcolumn'] : '',
        '#attributes' => array('class' => array('form-short')),
      );
      $form['column']['wrapper'][$i]['fcolumn'] = array(
        '#type' => 'textfield',
        '#size' => 30,
        '#default_value' => isset($row['fcolumn']) ? $row['fcolumn'] : '',
        '#attributes' => array('class' => array('form-short')),
      );
      $form['column']['wrapper'][$i]['remove'] = array(
        '#type' => 'submit',
        '#name' => 'remove_' . $i,
        '#value' => t('Remove'),
        '#ajax' => array(
          'callback' => 'forge_element_form_callback',
          'wrapper' => 'element-wrapper',
          'effect' => 'none',
        ),
      );
    }
    $form['column']['add_more'] = array(
      '#type' => 'submit',
      '#name' => 'add_more',
      '#value' => t('Add more'),
      '#ajax' => array(
        'callback' => 'forge_element_form_callback',
        'wrapper' => 'element-wrapper',
        'effect' => 'none',
      ),
    );
    $form['table'] = array(
      '#type' => 'hidden',
      '#value' => isset($query['table']) ? $query['table'] : $input['table'],
    );
  }

  /**
   * Submit-processing of element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();

    if (isset($query['table'])) {
      if (isset($query['field'])) {
        if ($query['field'] == '0') {
          $this->value[$query['table']]['fields'][$input['field']] = $input;
        } else {
          $array = $this->value[$query['table']]['fields'];
          $array = Forge::arrayKeyReplace($array, $query['field'], $input['field'], $input);
          $this->value[$query['table']]['fields'] = $array;
          $this->_rebindKeys($query['table'], $query['field'], $input['field']);
        }
      } elseif(isset($query['pkey'])) {
        $this->value[$query['table']]['primary key'] = array_filter($input['pkey']);
      } elseif(isset($query['ukey'])) {
        if ($query['ukey'] == '0') {
          $this->value[$query['table']]['unique keys'][$input['ukey']] = array_filter($input['column']);
        } else {
          $array = $this->value[$query['table']]['unique keys'];
          $array = Forge::arrayKeyReplace($array, $query['ukey'], $input['ukey'], array_filter($input['column']));
          $this->value[$query['table']]['unique keys'] = $array;
        }
      } elseif(isset($query['index'])) {
        if ($query['index'] == '0') {
          $this->value[$query['table']]['indexes'][$input['index']] = array_filter($input['column']);
        } else {
          $array = $this->value[$query['table']]['indexes'];
          $array = Forge::arrayKeyReplace($array, $query['index'], $input['index'], array_filter($input['column']));
          $this->value[$query['table']]['indexes'] = $array;
        }
      } elseif(isset($query['fkey'])) {
        $key = $query['fkey'];
        $row = array(
          'table' => $input['ftable'],
          'columns' => array(),
        );
        foreach ($input['column']['wrapper'] as $crow) {
          if ($crow['lcolumn'] && $crow['fcolumn']) {
            $row['columns'][$crow['lcolumn']] = $crow['fcolumn'];
          }
        }
        if ($query['fkey'] == '0') {
          $this->value[$query['table']]['foreign keys'][$input['fkey']] = $row;
        } else {
          $array = $this->value[$query['table']]['foreign keys'];
          $array = Forge::arrayKeyReplace($array, $key, $input['fkey'], $row);
          $this->value[$query['table']]['foreign keys'] = $array;
        }
      } elseif(isset($query['option'])) {
        $keys = array('mysql_suffix', 'mysql_engine', 'mysql_character_set', 'collation');
        foreach ($keys as $key) {
          if (!empty($input[$key])) {
            $this->value[$query['table']][$key] = $input[$key];
          }
        }
      } else {
        $table = $this->value[$query['table']];
        $table['description'] = $input['description'];
        $this->value = Forge::arrayKeyReplace($this->value,
          $query['table'], $input['table'], $table);
      }
    } else {
      $this->value[$input['table']] = array(
        'description' => $input['description'],
        'fields' => array(),
        'primary key' => array(),
        'unique keys' => array(),
        'indexes' => array(),
        'foreign keys' => array(),
        'mysql_suffix' => '',
        'mysql_engine' => '',
        'mysql_character_set' => '',
        'collation' => '',
      );
    }
  }

  /**
   * Return array of a table fields.
   *
   * @param  string $table Machine name of a table.
   * @return array         An associative array contains as key field machine name, in value - description.
   * @ingroup forms
   */
  public function getFieldsArray($table, $stylize = TRUE) {
    $fields = array();
    if (isset($this->value[$table]['fields'])) {
      foreach ($this->value[$table]['fields'] as $field => $struct) {
        $fields[$field] = $struct['description'] ? $struct['description'] . ($stylize ? ' (<strong>' . $field . '</strong>)' : ' (' . $field . ')') : $field;
      }
    }

    return $fields;
  }

  /**
   * Rebind keys and indexes after renaming a field.
   *
   * @param  string $table   Table name.
   * @param  string $oldName Old name of a field.
   * @param  string $newName New name of a field.
   */
  protected function _rebindKeys($table, $oldField, $newField) {
    $table = &$this->value[$table];
    if (isset($table['primary key'][$oldField])) {
      unset($table['primary key'][$oldField]);
      $table['primary key'][$newField] = $newField;
    }
    if (is_array($table['unique keys'])) {
      foreach ($table['unique keys'] as $key => &$columns) {
        if (isset($columns[$oldField])) {
          unset($columns[$oldField]);
          $columns[$newField] = $newField;
        }
      }
    }
    if (is_array($table['indexes'])) {
      foreach ($table['indexes'] as $key => &$columns) {
        if (isset($columns[$oldField])) {
          unset($columns[$oldField]);
          $columns[$newField] = $newField;
        }
      }
    }
    if (is_array($table['foreign keys'])) {
      foreach ($table['foreign keys'] as $key => &$row) {
        if (isset($row['columns'][$oldField])) {
          $row['columns'][$newField] = $row['columns'][$oldField];
          unset($row['columns'][$oldField]);
        }
      }
    }
  }
}

/**
 * Database update hook class.
 *
 * @see Forge_Element_Hook_Multiple
 */
class Forge_Element_Hook_Update_N extends Forge_Element_Hook_Multiple {
  public $presentTheme = 'forge_element_present_multiple';
  public $renderTheme = 'forge_element_render_hook_update_n';
  public $renderDestination = 'install';
  private $renderKey = NULL;

  /**
   * Return a HTML presentation of the element (preview).
   *
   * @return string
   * @ingroup presentation
   */
  public function getPresent() {
    return theme($this->presentTheme, array('element' => $this, 'display_keys' => TRUE));
  }

  /**
   * Return Drupal form for element editing.
   *
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();

    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['number'] = array(
      '#type' => 'textfield',
      '#title' => t('Update number'),
      '#default_value' => isset($query['key']) ? $query['key'] : '',
      '#required' => TRUE,
    );
    $form['number_description'] = array(
      '#type' => 'item',
      '#markup' => t("<div class=\"description\"><p>The numbers are composed of three parts:</p>
<ul>
  <li>1 digit for Drupal core compatibility.</li>
  <li>1 digit for your module's major release version (e.g., is this the 7.x-1.* (1) or 7.x-2.* (2) series of your module?). This digit should be 0 for initial porting of your module to a new Drupal core API.</li>
  <li>2 digits for sequential counting, starting with 00.</li>
</ul>
<p>Examples:</p>
<ul>
  <li><strong>mymodule_update_7000()</strong>: This is the required update for mymodule to run with Drupal core API 7.x when upgrading from Drupal core API 6.x.</li>
  <li><strong>mymodule_update_7100()</strong>: This is the first update to get the database ready to run mymodule 7.x-1.*.</li>
  <li><strong>mymodule_update_7200()</strong>: This is the first update to get the database ready to run mymodule 7.x-2.*. Users can directly update from 6.x-2.* to 7.x-2.* and they get all 70xx and 72xx updates, but not 71xx updates, because those reside in the 7.x-1.x branch only.</li>
</ul></div>"),
    );

    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']] : '',
      '#description' => t('Brief description of the update. Default: "Implements hook_update_N()."'),
    );

    return $form;
  }

  /**
   * On-submit processing of element.
   *
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      unset($this->value[$query['key']]);
    }
    $this->value[$input['number']] = $input['description'];
    ksort($this->value);
  }

  /**
   * @see Forge_Element::getFiles()
   * @ingroup render
   */
  public function getFiles() {
    if ($this->value) {
      return array($this->project . '.install');
    }
    return array();
  }

  /**
   * Return a prefix of the hook.
   *
   * @see Forge_Element_Hook::getPrefix()
   * @ingroup render
   */
  public function getPrefix($core) {
    $variables = array('element' => $this, 'core' => $core);
    if ($this->renderKey) {
      if ($custom = $this->value[$this->renderKey]) {
        $variables['core'] = NULL;
        $variables['custom'] = $custom;
      }
    }
    return theme('forge_element_render_hook_prefix', $variables);
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      foreach ($this->value as $number => $description) {
        $this->renderKey = $number;
        $this->renderName = $this->project . '_update_' . $number;
        $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core));
      }
    }
  }
}

/**
 * Database update dependencies hook class.
 *
 * @see Forge_Element_Hook
 */
class Forge_Element_Hook_Update_Dependencies extends Forge_Element_Hook_Multiple {
  public $presentTheme = 'forge_element_present_hook_update_dependencies';
  public $renderDestination = 'install';

  /**
   * Return secondary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getSecondaryLinks($args = array()) {
    $links = array(
      forge_popup_link('edit', $this, 'edit', $args),
      forge_link('delete', $this, 'delete', $args),
    );

    return $links;
  }

  /**
   * Delete specified value.
   *
   * @ingroup operations
   */
  public function delete() {
    $query = drupal_get_query_parameters();
    if (isset($query['module'])) {
      unset($this->value[$query['module']]);
    }
  }

  /**
   * Return Drupal form for element editing.
   *
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();

    if (!isset($input['module']) && isset($query['module'])) {
      $input['module'] = $query['module'];
      $input['number'] = $query['number'];
      $input['dependencies'] = array('wrapper' => array());
      foreach ($this->value[$input['module']][$input['number']] as $module => $numbers) {
        foreach ($numbers as $number) {
          $input['dependencies']['wrapper'][] = array(
            'module' => $module,
            'number' => $number,
          );
        }
      }
    } elseif (isset($input['triggering_element_name'])) {
      if ($input['triggering_element_name'] == 'add_more') {
        $input['dependencies']['wrapper'][] = array();
      } elseif (preg_match('/remove_(\d+)/', $input['triggering_element_name'], $m)) {
        unset($input['dependencies']['wrapper'][$m[1]]);
      }
    } else {
      $input['dependencies']['wrapper'][] = array(array());
    }

    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['module'] = array(
      '#type' => 'textfield',
      '#title' => t('Module'),
      '#default_value' => isset($query['module']) ? $query['module'] : isset($input['module']) ? $input['module'] : '',
      '#description' => t('Machine name of the dependent module.'),
    );
    $form['number'] = array(
      '#type' => 'textfield',
      '#title' => t('Update hook number'),
      '#default_value' => isset($query['number']) ? $query['number'] : isset($input['number']) ? $input['number'] : '',
      '#description' => t('Number of dependent update hook.'),
    );

    $form['dependencies'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dependecies'),
    );
    $form['dependencies']['wrapper'] = array(
      '#type' => 'item',
      '#theme' => 'forge_element_present_hook_update_dependence',
    );
    foreach ($input['dependencies']['wrapper'] as $i => $row) {
      $form['dependencies']['wrapper'][$i]['module'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($row['module']) ? $row['module'] : '',
        '#size' => 50,
      );
      $form['dependencies']['wrapper'][$i]['number'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($row['number']) ? $row['number'] : '',
        '#size' => 50,
      );
      $form['dependencies']['wrapper'][$i]['remove'] = array(
        '#type' => 'submit',
        '#name' => 'remove_' . $i,
        '#value' => t('Remove'),
        '#ajax' => array(
          'callback' => 'forge_element_form_callback',
          'wrapper' => 'element-wrapper',
          'effect' => 'none',
        ),
      );
    }
    $form['dependencies']['add_more'] = array(
      '#type' => 'submit',
      '#name' => 'add_more',
      '#value' => t('Add more'),
      '#ajax' => array(
        'callback' => 'forge_element_form_callback',
        'wrapper' => 'element-wrapper',
        'effect' => 'none',
      ),
    );

    return $form;
  }

  /**
   * On-submit processing of element.
   *
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    $dependencies = array();
    foreach ($input['dependencies']['wrapper'] as $row) {
      if ($row['module'] && $row['number']) {
        if (!isset($dependencies[$row['module']])) {
          $dependencies[$row['module']] = array();
        }
        $dependencies[$row['module']][] = $row['number'];
      }
    }
    if (isset($query['module'])) {
      unset($this->value[$query['module']]);
    }
    $this->value[$input['module']][$input['number']] = $dependencies;
  }

  /**
   * @see Forge_Element::getFiles()
   * @ingroup render
   */
  public function getFiles() {
    if ($this->value) {
      return array($this->project . '.install');
    }
    return array();
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $body = array();
      foreach ($this->value as $module => $dependencies) {
        foreach ($dependencies as $number => $drows) {
          $body[] = '$dependencies[\''. $module .'\'][\'' . $number . '\'] = ' . theme('forge_array', array('array' => $drows, 'clean' => TRUE)) . ';';
        }
      }
      $body[] = "\n";
      $body[] = 'return $dependencies;';
      $body = implode("\n", $body);
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'body' => $body));
    }
  }
}
