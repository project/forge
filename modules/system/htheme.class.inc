<?php

/**
 * @file
 * Class implementation of a theme hook element.
 */

/**
 * Theme element class.
 */
class Forge_Element_Hook_Theme extends Forge_Element_Hook_Multiple {
  public $presentTheme = 'forge_element_present_hook_theme';
  public $renderTheme = 'forge_element_render_hook_theme';

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Machine name'),
      '#default_value' => isset($query['key']) ? $query['key'] : '',
      '#required' => TRUE,
    );

    $variables = isset($query['key']) ? $this->value[$query['key']]['variables']['wrapper'] : array();
    if ($input) {
      $variables = $input['variables']['wrapper'];
      if (isset($input['triggering_element_name'])) {
        if ($input['triggering_element_name'] == 'variables_add_more') {
          $variables[] = array();
        }
        if (preg_match('/variables_remove_(\d+)/', $input['triggering_element_name'], $m)) {
          unset($variables[$m[1]]);
        }
      }
    } elseif (!count($variables)) {
      $variables[] = array();
    }
    $form['variables'] = array(
      '#type' => 'fieldset',
      '#title' => t('Variables'),
      '#description' => t('Each array key is the name of the variable, and the value given is used as the default value if the function calling theme() does not supply it.'),
    );
    $form['variables']['wrapper'] = array(
      '#type' => 'item',
      '#prefix' => '<div id="variables-wrapper">',
      '#suffix' => '</div>',
      '#column_title' => t('Variable'),
      '#theme' => 'forge_element_present_hook_theme_dyn_table'
    );
    foreach ($variables as $i => $row) {
      $form['variables']['wrapper'][$i]['value'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($row['value']) ? $row['value'] : '',
      );
      $form['variables']['wrapper'][$i]['remove'] = array(
        '#type' => 'submit',
        '#name' => 'variables_remove_' . $i,
        '#value' => t('Remove'),
        '#ajax' => array(
          'callback' => 'forge_element_form_callback',
          'wrapper' => 'element-wrapper',
          'effect' => 'none',
        ),
      );
    }
    $form['variables']['add_more'] = array(
      '#type' => 'submit',
      '#name' => 'variables_add_more',
      '#value' => t('Add more'),
      '#ajax' => array(
        'callback' => 'forge_element_form_callback',
        'wrapper' => 'element-wrapper',
        'effect' => 'none',
      ),
    );

    $form['render element'] = array(
      '#type' => 'textfield',
      '#title' => t('Render element'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']]['render element'] : '',
      '#description' => t('The name of the renderable element or element tree to pass to the theme function.'),
    );
    $form['template'] = array(
      '#type' => 'textfield',
      '#title' => t('Template'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']]['template'] : '',
      '#description' => t('If specified, this theme implementation is a template, and this is the template file without an extension.'),
    );
    $form['file'] = array(
      '#type' => 'textfield',
      '#title' => t('File'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']]['file'] : '',
      '#description' => t('The file the implementation resides in.'),
    );
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']]['path'] : '',
      '#description' => t('Override the path of the file to be used.'),
    );
    $form['base hook'] = array(
      '#type' => 'textfield',
      '#title' => t('Base hook'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']]['base hook'] : '',
      '#description' => t('A string declaring the base theme hook if this theme implementation is actually implementing a suggestion for another theme hook.'),
    );
    $form['pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Pattern'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']]['pattern'] : '',
      '#description' => t('A regular expression pattern to be used to allow this theme implementation to have a dynamic name. The convention is to use __ to differentiate the dynamic portion of the theme.'),
    );

    $preprocess = isset($query['key']) ? $this->value[$query['key']]['preprocess']['wrapper'] : array();
    if ($input) {
      $preprocess = $input['preprocess']['wrapper'];
      if (isset($input['triggering_element_name'])) {
        if ($input['triggering_element_name'] == 'preprocess_add_more') {
          $preprocess[] = array();
        }
        if (preg_match('/preprocess_remove_(\d+)/', $input['triggering_element_name'], $m)) {
          unset($preprocess[$m[1]]);
        }
      }
    } elseif (!count($preprocess)) {
      $preprocess[] = array();
    }
    $form['preprocess'] = array(
      '#type' => 'fieldset',
      '#title' => t('Preprocess functions'),
      '#description' => t("A list of functions used to preprocess this data. Ordinarily this won't be used; it's automatically filled in."),
    );
    $form['preprocess']['wrapper'] = array(
      '#type' => 'item',
      '#prefix' => '<div id="preprocess-wrapper">',
      '#suffix' => '</div>',
      '#column_title' => t('Function'),
      '#theme' => 'forge_element_present_hook_theme_dyn_table'
    );
    foreach ($preprocess as $i => $row) {
      $form['preprocess']['wrapper'][$i]['value'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($row['value']) ? $row['value'] : '',
      );
      $form['preprocess']['wrapper'][$i]['remove'] = array(
        '#type' => 'submit',
        '#name' => 'preprocess_remove_' . $i,
        '#value' => t('Remove'),
        '#ajax' => array(
          'callback' => 'forge_element_form_callback',
          'wrapper' => 'element-wrapper',
          'effect' => 'none',
        ),
      );
    }
    $form['preprocess']['add_more'] = array(
      '#type' => 'submit',
      '#name' => 'preprocess_add_more',
      '#value' => t('Add more'),
      '#ajax' => array(
        'callback' => 'forge_element_form_callback',
        'wrapper' => 'element-wrapper',
        'effect' => 'none',
      ),
    );

    $form['override preprocess functions'] = array(
      '#type' => 'checkbox',
      '#title' => t('Override preprocess functions'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']]['override preprocess functions'] : FALSE,
      '#description' => t('Set to TRUE when a theme does NOT want the standard preprocess functions to run.'),
    );

    return $form;
  }

  /**
   * On-submit processing of element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $this->value = Forge::arrayKeyReplace($this->value, $query['key'], $input['key'], $input);
    } else {
      $this->value[$input['key']] = $input;
    }
  }

  /**
   * Get variables for the specified theme key.
   */
  public function getVariables($theme_key) {
    $vars = array();
    if (!isset($this->value[$theme_key])) {
      return $vars;
    }
    $row = $this->value[$theme_key];
    if ($row['variables']['wrapper']) {
      foreach ($row['variables']['wrapper'] as $row0) {
        if ($row0['value']) {
          $vars[$row0['value']] = NULL;
        }
      }
    }

    return $vars;
  }

  /**
   * Get defined preprocess functions for the specified theme key.
   */
  public function getPreprocessFunctions($theme_key) {
    $preprocess = array();
    if (!isset($this->value[$theme_key])) {
      return $preprocess;
    }
    $row = $this->value[$theme_key];
    if ($row['preprocess']['wrapper']) {
      $preprocess = array();
      foreach ($row['preprocess']['wrapper'] as $row0) {
        if ($row0['value']) {
          $preprocess[] = $row0['value'];
        }
      }
    }

    return $preprocess;
  }

  /**
   * @see Forge_Element::getFiles().
   */
  public function getFiles() {
    $files = array();
    foreach ($this->value as $row) {
      if ($row['template']) {
        $files[] = $row['template'] . '.tpl.php';
      }
    }

    return $files;
  }

  /**
   * Render the element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $args = $this->optionsArgs($core, $args);
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'args' => $args));
      // Proceed templates
      foreach ($this->value as $key => $row) {
        if (!empty($row['template'])) {
          $file = $row['template'] . '.tpl.php';
          $template_output = "/**\n";
          $vars = array_keys($this->getVariables($key));
          if ($vars) {
            $template_output .= " * Available variables:\n";
            foreach ($vars as $var) {
              $template_output .= ' * - $' . $var . "\n";
            }
          }
          $preprocess = $this->getPreprocessFunctions($key);
          foreach ($preprocess as $func) {
            $template_output .= ' * @see ' . $func . "\n";
          }
          $template_output .= " * @ingroup themeable\n */\n?>";
          $output[$file][] = $template_output;
        }
      }
      // Proceed theme and preprocess functions
      foreach ($this->value as $key => $row) {
        $destination = $this->renderDestination;
        if ($row['file'] && !$row['path']) {
          $destination = $row['file'];
        }
        if (empty($row['template'])) {
          $vars = array_keys($this->getVariables($key));
          $theme_output = "/**\n * Returns HTML for {$key}.\n";
          $variables = '$variables';
          if ($vars) {
            if ($core > Forge::CORE_SUPPORT_6x) {
              $theme_output .= " * @param $variables\n *   An associative array containing:\n";
              foreach ($vars as $var) {
                $theme_output .= " *   - {$var}.\n";
              }
            } else {
              $variables = implode(', ', array_map(function($value){return '$'.$value;}, $vars));
            }
          }
          $theme_output .= " * @ingroup themeable\n */;\n";
          $theme_output .= "function theme_{$key}({$variables}) {\n  //\n}\n";
          $output[$destination][] = $theme_output;
        }
        $preprocess = $this->getPreprocessFunctions($key);
        if ($preprocess) {
          foreach ($preprocess as $func) {
            $func_output = "/**\n * Process variables for {$key}.\n */\nfunction {$func}(" . '$variables' . ") {\n  //\n}\n";
            $output[$destination][] = $func_output;
          }
        }
      }
    }
  }
}
