<?php

/**
 * @file
 * Tests for theme element.
 */

/**
 * Theme hook testing class.
 */
class ForgeElementThemeTestCase extends ForgeWebTestCase {
  protected $user;

  public static function getInfo() {
    return array(
      'name' => 'Theme element',
      'description' => 'Theme element API.',
      'group' => 'Forge',
    );
  }

  public function setUp() {
    parent::setUp(array('ctools', 'forge'));
    $this->user = $this->drupalCreateUser(array('access forge'));
  }

  /**
   * Testing hook_theme element.
   */
  public function testHookTheme() {
    $this->drupalLogin($this->user);
    $name = $this->randomName();
    $machine_name = drupal_strtolower('fp' . $name);
    $project = $this->forgeCreateProject($name);

    $this->drupalGet('admin/forge/project/' . $machine_name . '/edit');
    $edit = array(
      'element[key]' => 'example0',
      'element[variables][wrapper][0][value]' => 'arg0',
      'element[file]' => 'theme.inc',
    );
    $this->drupalPost('admin/forge/' . $machine_name . '/nojs/element/hook_theme/hook_theme/add', $edit, t('Update'), array('query' => array('debug' => TRUE)));
    $edit = array(
      'element[key]' => 'example1',
      'element[template]' => 'example1',
      'element[preprocess][wrapper][0][value]' => 'template_preprocess_example1',
      'element[override preprocess functions]' => TRUE,
    );
    $this->drupalPost('admin/forge/' . $machine_name . '/nojs/element/hook_theme/hook_theme/add', $edit, t('Update'), array('query' => array('debug' => TRUE)));
    $expected = array(
      'example0' => array(
        'key' => 'example0',
        'variables' => array(
          'wrapper' => array(array(
            'value' => 'arg0',
            'remove' => t('Remove'),
          )),
          'add_more' => t('Add more'),
        ),
        'render element' => '',
        'template' => '',
        'file' => 'theme.inc',
        'path' => '',
        'base hook' => '',
        'pattern' => '',
        'preprocess' => array(
          'wrapper' => array(array(
            'value' => '',
            'remove' => t('Remove'),
          )),
          'add_more' => t('Add more'),
        ),
        'override preprocess functions' => 0,
      ),
      'example1' => array(
        'key' => 'example1',
        'variables' => array(
          'wrapper' => array(array(
            'value' => '',
            'remove' => t('Remove'),
          )),
          'add_more' => t('Add more'),
        ),
        'render element' => '',
        'template' => 'example1',
        'file' => '',
        'path' => '',
        'base hook' => '',
        'pattern' => '',
        'preprocess' => array(
          'wrapper' => array(array(
            'value' => 'template_preprocess_example1',
            'remove' => t('Remove'),
          )),
          'add_more' => t('Add more'),
        ),
        'override preprocess functions' => TRUE,
      ),
    );
    $this->assertCommand($expected, 'hook_theme element addition test passed.');

    $edit = array(
      'element[key]' => 'example1',
      'element[template]' => 'example1',
      'element[preprocess][wrapper][0][value]' => 'template_preprocess_example15',
      'element[override preprocess functions]' => FALSE,
    );
    $this->drupalPost('admin/forge/' . $machine_name . '/nojs/element/hook_theme/hook_theme/edit', $edit, t('Update'), array('query' => array('key' => 'example1', 'debug' => TRUE)));
    $expected['example1'] = array(
      'key' => 'example1',
      'variables' => array(
        'wrapper' => array(array(
          'value' => '',
          'remove' => t('Remove'),
        )),
        'add_more' => t('Add more'),
      ),
      'render element' => '',
      'template' => 'example1',
      'file' => '',
      'path' => '',
      'base hook' => '',
      'pattern' => '',
      'preprocess' => array(
        'wrapper' => array(array(
          'value' => 'template_preprocess_example15',
          'remove' => t('Remove'),
        )),
        'add_more' => t('Add more'),
      ),
      'override preprocess functions' => FALSE,
    );
    $this->assertCommand($expected, 'hook_theme element edit test passed.');

    $this->drupalGet('admin/forge/' . $machine_name . '/element/hook_theme/hook_theme/delete', array('query' => array('key' => 'example1', 'debug' => TRUE)));
    unset($expected['example1']);
    $this->assertCommand($expected, 'hook_theme element deletion test passed.');

    $this->drupalGet('admin/forge/' . $machine_name . '/element/hook_theme/hook_theme/clear', array('query' => array('debug' => TRUE)));
    $this->assertCommand(array(), 'hook_theme element clear test passed.');
  }
}
