<?php

/**
 * Theme element definition.
 *
 * @see system_forge_element()
 */
function _system_forge_element_hook_theme() {
  $element['hook_theme'] = array(
    'class' => 'Forge_Element_Hook_Theme',
    'name' => 'Theme',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'hook_theme',
    'options' => array('args' => '$existing, $type, $theme, $path'),
    'info' => array(
      'description' => 'Register theme implementations.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_theme/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_theme/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_theme/8',
      ),
    ),
  );

  $element['hook_theme_registry_alter'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Theme alter',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'hook_theme',
    'options' => array('args' => '&$theme_registry'),
    'info' => array(
      'description' => 'Alter the theme registry information returned from hook_theme().',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_theme_registry_alter/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_theme_registry_alter/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_theme_registry_alter/8',
      ),
    ),
  );
  // D8: https://api.drupal.org/api/drupal/core%21modules%21system%21theme.api.php/function/hook_theme_suggestions_alter/8
  // D8: https://api.drupal.org/api/drupal/core%21modules%21system%21theme.api.php/function/hook_theme_suggestions_HOOK/8
  // D8: https://api.drupal.org/api/drupal/core%21modules%21system%21theme.api.php/function/hook_theme_suggestions_HOOK_alter/8

  return $element;
}
