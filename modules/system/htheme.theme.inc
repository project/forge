<?php

/**
 * Returns HTML for presentation of module theme registry.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Theme object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_theme($variables) {
  $header = $rows = array();

  $header_links = $info_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE, 'hook' => TRUE)), 'class' => 'forge-element-theme-multiple-name', 'width' => '70%', 'colspan' => 2),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'class' => 'forge-element-theme-multiple-links', 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  foreach ($variables['element']->value as $key => $row) {
    $value_links = $variables['element']->getSecondaryLinks(array('key' => $row['key']));
    $value_links = theme('links__ctools_dropbutton', array('links' => $value_links, 'attributes' => array('class' => array('links', 'inline'))));

    $items = array();
    $vars = $variables['element']->getVariables($row['key']);
    if ($vars) {
      $items[] = t('Variables: %value', array('%value' => implode(', ', array_keys($vars))));
    }
    if ($row['render element']) {
      $items[] = t('Render element: %value', array('%value' => $row['render element']));
    }
    if ($row['template']) {
      $items[] = t('Template: %value', array('%value' => $row['template']));
    }
    if ($row['file']) {
      $items[] = t('File: %value', array('%value' => $row['file']));
    }
    if ($row['path']) {
      $items[] = t('Path: %value', array('%value' => $row['path']));
    }
    if ($row['base hook']) {
      $items[] = t('Base hook: %value', array('%value' => $row['base hook']));
    }
    if ($row['pattern']) {
      $items[] = t('Pattern: %value', array('%value' => $row['pattern']));
    }
    $preprocess = $variables['element']->getPreprocessFunctions($row['key']);
    if ($preprocess) {
      $items[] = t('Preprocess functions: %value', array('%value' => implode(', ', $preprocess)));
    }
    if ($row['override preprocess functions']) {
      $items[] = t('Override preprocess functions: %value', array('%value' => t('Yes')));
    }

    $rows[] = array(
      array('data' => $key, 'width' => '30%'),
      array('data' => theme('item_list', array('items' => $items)), 'width' => '50%', 'colspan' => 2),
      array('data' => $value_links, 'width' => '20%', 'class' => array('forge-operations')),
    );
  }
  $class = array('forge-element', 'forge-element-hook-theme');
  if (count($rows) == 2) {
    $class[] = 'forge-element-empty';
    $rows[] = array(
      array('data' => t('no registry items found'), 'colspan' => 4),
    );
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Returns HTML for presentation of theme dynamic table.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: Form part to rendering.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_theme_dyn_table($variables) {
  unset($variables['form']['#theme']);

  $header = array(
    $variables['form']['#column_title'],
    t('Operations'),
  );
  $rows = array();
  foreach ($variables['form'] as $key => $row) {
    if (is_int($key)) {
      $rows[] = array(
        render($row['value']),
        render($row['remove']),
      );
    }
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Render hook_theme element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Theme object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constant.
 *   - args:    Hook arguments.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_theme($variables) {
  $registry = array();
  foreach ($variables['element']->value as $row) {
    $item = array();
    $vars = $variables['element']->getVariables($row['key']);
    if ($vars) {
      $key = $variables['core'] == Forge::CORE_SUPPORT_6x ? 'arguments' : 'variables';
      $item[$key] = $vars;
    }
    if ($row['render element']) {
      $item['render element'] = $row['render element'];
    }
    if ($row['template']) {
      $item['template'] = $row['template'];
    }
    if ($row['file']) {
      $item['file'] = $row['file'];
    }
    if ($row['path']) {
      $item['path'] = $row['path'];
    }
    if ($row['base hook']) {
      $key = $variables['core'] == Forge::CORE_SUPPORT_6x ? 'original hook' : 'base hook';
      $item[$key] = $row['base hook'];
    }
    if ($row['pattern']) {
      $item['pattern'] = $row['pattern'];
    }
    $preprocess = $variables['element']->getPreprocessFunctions($row['key']);
    if ($preprocess) {
      $item['preprocess functions'] = $preprocess;
    }
    if ($row['override preprocess functions']) {
      $item['override preprocess functions'] = 'TRUE';
    }

    $registry[$row['key']] = $item;
  }

  $body = theme('forge_array', array('array' => $registry));
  $body = str_replace("\'", "'", $body);

  $variables['body'] = 'return ' . $body . ';';

  return theme('forge_element_render_hook', $variables);
}
