/**
 * @file
 * Install section JS.
 */
(function ($) {

Drupal.behaviors.forgeInstall = {
  attach: function(context) {
    $('#schema-tabs').tabs();
  }
};

})(jQuery);
