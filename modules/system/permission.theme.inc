<?php

/**
 * Returns HTML for presentation of module permissions.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Permission object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_permission($variables) {
  $header = $rows = array();

  $header_links = $info_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE, 'hook' => TRUE)), 'class' => 'forge-element-permission-multiple-name', 'width' => '70%', 'colspan' => 2),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'class' => 'forge-element-permission-multiple-links', 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  foreach ($variables['element']->value as $module => $row) {
    $value_links = $variables['element']->getSecondaryLinks(array('key' => $row['key']));
    $value_links = theme('links__ctools_dropbutton', array('links' => $value_links, 'attributes' => array('class' => array('links', 'inline'))));
    $title = '<div>' . $row['title'] . '</div><div class="description">' . $row['key'] . '</div>';
    $rows[] = array(
      array('data' => $title, 'width' => '30%'),
      array('data' => $row['description'], 'width' => '50%', 'colspan' => 2),
      array('data' => $value_links, 'width' => '20%', 'class' => array('forge-operations')),
    );
  }
  $class = array('forge-element', 'forge-element-hook-permission');
  if (count($rows) == 2) {
    $class[] = 'forge-element-empty';
    $rows[] = array(
      array('data' => t('no permissions found'), 'colspan' => 4),
    );
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Render hook_permission element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Permission object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constant.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_permission($variables) {
  $permissions = array();
  if ($variables['core'] == Forge::CORE_SUPPORT_6x) {
    $permissions = array_keys($variables['element']->value);
  } else {
    foreach ($variables['element']->value as $key => $row) {
      if (!$row['title']) {
        $row['title'] = drupal_ucfirst($row['title']);
      }
      $permissions[$key] = array(
        'title' => "t('" . $row['title'] . "')",
      );
      if ($row['description']) {
        $permissions[$key]['description'] = "t('" . $row['description'] . "')";
      }
    }
  }

  $body = theme('forge_array', array('array' => $permissions));
  $body = preg_replace("/'(t\(.*\))'/", '$1', $body);
  $body = str_replace("\'", "'", $body);

  $variables['args'] = '';
  $variables['body'] = 'return ' . $body . ';';

  return theme('forge_element_render_hook', $variables);
}
