<?php

/**
 * Returns HTML for requirment element presentation.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_requirement($variables) {
  $header = $rows = array();

  $project = Forge_Project::session($variables['element']->project);
  $info_links = $variables['element']->getLinks();
  $info_links = theme('links__ctools_dropbutton', array('links' => $info_links, 'attributes' => array('class' => array('links', 'inline'))));
  $items = array();
  if (($project->core_support & Forge::CORE_SUPPORT_6x) > 0) {
    $items[] = t('6.x branch: <strong>!value</strong>', array('!value' => $variables['element']->value[Forge::CORE_SUPPORT_6x]));
  }
  if (($project->core_support & Forge::CORE_SUPPORT_7x) > 0) {
    $items[] = t('7.x branch: <strong>!value</strong>', array('!value' => $variables['element']->value[Forge::CORE_SUPPORT_7x]));
  }
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables), 'class' => 'forge-element-title', 'width' => '20%'),
    array('data' => theme('item_list', array('items' => $items)), 'class' => 'forge-element-value', 'width' => '50%'),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $info_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  $class = array('forge-element');
  if (empty($variables['element']->value)) {
    $class[] = 'forge-element-empty';
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Returns HTML for presentation of project stylesheets.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Info_Multiple_Stylesheets object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_info_multiple_stylesheets($variables) {
  $header = $rows = array();
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE)), 'class' => 'forge-element-title', 'width' => '70%'),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => '', 'class' => 'forge-operations', 'width' => '20%'),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 3),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  foreach ($variables['element']->value as $media => $styles) {
    $style_rows = array();
    foreach ($styles as $key => $style) {
      $style_links = $variables['element']->getSecondaryLinks(array('media' => $media, 'key' => $key));
      $style_links = theme('links__ctools_dropbutton', array('links' => $style_links, 'attributes' => array('class' => array('links', 'inline'))));
      $style_rows[] = array(
        'data' => array(
          array('data' => '<em>' . $style . '</em>', 'width' => '80%', 'colspan' => 2, 'class' => array('forge-element-info-multiple-stylesheets-style-name')),
          array('data' => $style_links, 'width' => '20%', 'class' => array('forge-operations')),
        ),
        'class' => array('forge-element-info-multiple-stylesheets-style')
      );
    }

    $media_links = $variables['element']->getPrimaryLinks(array('media' => $media));
    $media_links = theme('links__ctools_dropbutton', array('links' => $media_links, 'attributes' => array('class' => array('links', 'inline'))));
    $class = array('forge-element-info-multiple-stylesheets-media');
    if (empty($style_rows)) {
      $class[] = 'forge-element-empty';
    }
    $rows[] = array(
      'data' => array(
        array('data' => $media, 'width' => '80%', 'colspan' => 2, 'class' => array('forge-element-info-multiple-stylesheets-media-name')),
        array('data' => $media_links, 'width' => '20%', 'class' => array('forge-operations')),
      ),
      'class' => $class,
    );

    if ($style_rows) {
      $style_table = theme('table', array('header' => array(), 'rows' => $style_rows));
      $rows[] = array(
        'data' => array(
          array('data' => $style_table, 'width' => '100%', 'colspan' => 3, 'class' => array('forge-element-info-multiple-stylesheets-media-style')),
        ),
        'class' => array('forge-element-info-multiple-stylesheets-media-styles'),
      );
    }
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('forge-element', 'forge-element-multiple', 'forge-element-info-multiple-stylesheets'), 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Render Stylesheets element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Info_Multiple_Stylesheets object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_info_multiple_stylesheets($variables) {
  $render = array();
  foreach ($variables['element']->value as $media => $media_files) {
    foreach ($media_files as $file) {
      $render[] = "stylesheets[{$media}][] = {$file}";
    }
  }

  return implode("\n", $render);
}
