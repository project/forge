<?php

/**
 * Permission element definition.
 *
 * @see system_forge_element()
 */
function _system_forge_element_permission() {
  $element['hook_permission'] = array(
    'class' => 'Forge_Element_Hook_Permission',
    'name' => 'Permissions',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'permission',
    'info' => array(
      'description' => 'Define user permissions.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_perm/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_permission/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_permission/8',
      ),
    ),
  );

  return $element;
}
