<?php

/**
 * Implements hook_forge_group().
 */
function system_forge_group() {
  $group['info'] = array(
    'title' => 'Info section',
    'weight' => -100,
  );
  $group['install'] = array(
    'title' => 'Installation',
    'weight' => -99,
  );
  $group['permission'] = array(
    'title' => 'Permissions',
    'weight' => -98,
  );
  $group['hook_theme'] = array(
    'title' => 'Theme',
    'weight' => -97,
  );
  $group['theme'] = array(
    'title' => 'Common',
    'weight' => -99,
  );
  $group['theme_system'] = array(
    'title' => 'System',
  );
  $group['theme_form'] = array(
    'title' => 'Form',
  );
  $group['theme_pager'] = array(
    'title' => 'Pager',
  );
  $group['theme_maintenance'] = array(
    'title' => 'Maintenance',
    'weight' => 100,
  );
  return $group;
}

/**
 * Implements hook_forge_element().
 */
function system_forge_element() {
  $element = array();
  $element += _system_forge_element_info();
  $element += _system_forge_element_install();
  $element += _system_forge_element_permission();
  $element += _system_forge_element_hook_theme();
  $element += _system_forge_element_theme();
  $element += _system_forge_element_template();
  $element += _system_forge_element_theme_form();
  $element += _system_forge_element_theme_pager();
  $element += _system_forge_element_theme_maintenance();

  return $element;
}

/**
 * Implements hook_forge_theme().
 */
function system_forge_theme($existing, $type, $theme, $path) {
  return array(
    // Info section.
    // - present.
    'forge_element_present_requirement' => array(
      'variables' => array('element' => NULL),
    ),
    'forge_element_present_info_multiple_stylesheets' => array(
      'variables' => array('element' => NULL),
    ),
    // - render.
    'forge_element_render_info_multiple_stylesheets' => array(
      'variables' => array('element' => NULL),
    ),

    // Install section.
    // - present.
    'forge_element_present_hook_schema' => array(
      'variables' => array('element' => NULL),
    ),
    'forge_element_present_hook_schema_fkey' => array(
      'render element' => 'form',
    ),
    'forge_element_present_hook_update_dependencies' => array(
      'variables' => array('element' => NULL),
    ),
    'forge_element_present_hook_update_dependence' => array(
      'render element' => 'form',
    ),
    // - render.
    'forge_element_render_hook_schema' => array(
      'variables' => array('element' => NULL, 'core' => NULL),
    ),
    'forge_element_render_hook_update_n' => array(
      'variables' => array('element' => NULL, 'core' => NULL),
    ),
    // Permission section.
    // - present.
    'forge_element_present_hook_permission' => array(
      'variables' => array('element' => NULL, 'core' => NULL),
    ),
    // - render.
    'forge_element_render_hook_permission' => array(
      'variables' => array('element' => NULL, 'core' => NULL),
    ),
    // hook_theme section.
    // - present.
    'forge_element_present_hook_theme' => array(
      'variables' => array('element' => NULL, 'core' => NULL),
    ),
    'forge_element_present_hook_theme_dyn_table' => array(
      'render element' => 'form',
    ),
    // - render.
    'forge_element_render_hook_theme' => array(
      'variables' => array('element' => NULL, 'core' => NULL),
    ),
  );
}
