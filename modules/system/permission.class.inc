<?php

/**
 * @file
 * Class implementation of a permission hook element.
 */

/**
 * Permission element class.
 */
class Forge_Element_Hook_Permission extends Forge_Element_Hook_Multiple {
  public $renderDestination = 'module';
  public $presentTheme = 'forge_element_present_hook_permission';
  public $renderTheme = 'forge_element_render_hook_permission';

  /**
   * Return a HTML presentation of the element (preview).
   *
   * @return string
   * @ingroup presentation
   */
  public function getPresent() {
    return theme($this->presentTheme, array('element' => $this, 'display_keys' => TRUE));
  }

  /**
   * Return Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $query = drupal_get_query_parameters();
    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Machine name'),
      '#default_value' => isset($query['key']) ? $query['key'] : '',
      '#required' => TRUE,
    );
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']]['title'] : '',
      '#required' => TRUE,
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($query['key']) ? $this->value[$query['key']]['description'] : '',
    );

    return $form;
  }

  /**
   * On-submit processing of element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $query = drupal_get_query_parameters();
    if (isset($query['key'])) {
      $this->value = Forge::arrayKeyReplace($this->value, $query['key'], $input['key'], $input);
    } else {
      $this->value[$input['key']] = $input;
    }
  }

  /**
   * Directly set new permission in a session storage.
   *
   * @param  string $project    Project machine name.
   * @param  string $permission Permission key
   */
  public static function sessionSet($project, $permission) {
    $project = Forge_Project::session($project);
    if (!isset($project->data['hook_permission'][$permission])) {
      $project->data['hook_permission'][$permission] = array(
        'key' => $permission,
        'title' => drupal_ucfirst($permission),
        'description' => '',
      );
    }
  }

  /**
   * Render the element.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      if ($core == Forge::CORE_SUPPORT_6x) {
        $this->machine_name = 'hook_perm';
        $this->renderName = $this->project . '_perm';
      }
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core));
      if ($core == Forge::CORE_SUPPORT_6x) {
        $this->machine_name = 'hook_permission';
        $this->renderName = $this->project . '_permission';
      }
    }
  }
}
