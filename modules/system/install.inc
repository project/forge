<?php

/**
 * Install elements definition.
 *
 * @see system_forge_element()
 */
function _system_forge_element_install() {
  $element['hook_schema'] = array(
    'class' => 'Forge_Element_Hook_Schema',
    'name' => 'Database schema',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'install',
    'info' => array(
      'description' => 'Define the current version of the database schema.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21install.php/function/hook_schema/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_schema/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_schema/8',
      ),
    ),
    'dependencies' => array(
      Forge::CORE_SUPPORT_6x => array(
        'hook_install',
        'hook_uninstall',
      ),
    ),
  );

  $element['hook_schema_alter'] = array(
    'class' => 'Forge_Element_Hook_Install',
    'name' => 'Database schema alter',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'install',
    'options' => array(
      'args' => '&$schema',
    ),
    'info' => array(
      'description' => 'Perform alterations to existing database schemas.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_schema_alter/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_schema_alter/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_schema_alter/8',
      ),
    ),
  );

  $element['hook_install'] = array(
    'class' => 'Forge_Element_Hook_Install_Install',
    'name' => 'Install',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'install',
    'info' => array(
      'description' => 'Perform setup tasks when the module is installed.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21install.php/function/hook_install/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_install/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_install/8',
      ),
    ),
  );

  $element['hook_uninstall'] = array(
    'class' => 'Forge_Element_Hook_Install_Uninstall',
    'name' => 'Uninstall',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'install',
    'info' => array(
      'description' => 'Remove any information that the module sets.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21install.php/function/hook_uninstall/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_uninstall/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_uninstall/8',
      ),
    ),
  );

  $element['hook_requirements'] = array(
    'class' => 'Forge_Element_Hook_Install',
    'name' => 'Requirements',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'install',
    'options' => array(
      'args' => '$phase',
    ),
    'info' => array(
      'description' => 'Check installation requirements and do status reporting.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21install.php/function/hook_requirements/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_requirements/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_requirements/8',
      ),
    ),
  );

  $element['hook_enable'] = array(
    'class' => 'Forge_Element_Hook_Install',
    'name' => 'Post enable',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'install',
    'info' => array(
      'description' => 'Perform necessary actions after module is enabled.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21install.php/function/hook_enable/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_enable/7',
      ),
    ),
  );

  $element['hook_disable'] = array(
    'class' => 'Forge_Element_Hook_Install',
    'name' => 'Before disable',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'install',
    'info' => array(
      'description' => 'Perform necessary actions before module is disabled.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21install.php/function/hook_disable/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_disable/7',
      ),
    ),
  );

  $element['hook_modules_installed'] = array(
    'class' => 'Forge_Element_Hook_Install',
    'name' => 'Post install modules',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'install',
    'options' => array(
      'args' => '$modules',
    ),
    'info' => array(
      'description' => 'Perform necessary actions after modules are installed.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_modules_installed/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_modules_installed/8',
      ),
    ),
  );

  $element['hook_modules_enabled'] = array(
    'class' => 'Forge_Element_Hook_Install',
    'name' => 'Post enable modules',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'install',
    'options' => array(
      'args' => '$modules',
    ),
    'info' => array(
      'description' => 'Perform necessary actions after modules are enabled.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_modules_enabled/7',
      ),
    ),
  );

  $element['hook_modules_disabled'] = array(
    'class' => 'Forge_Element_Hook_Install',
    'name' => 'Before disable modules',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'install',
    'options' => array(
      'args' => '$modules',
    ),
    'info' => array(
      'description' => 'Perform necessary actions after modules are disabled.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_modules_disabled/7',
      ),
    ),
  );

  $element['hook_update_N'] = array(
    'class' => 'Forge_Element_Hook_Update_N',
    'name' => 'Database update',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'install',
    'info' => array(
      'description' => 'Perform a single update.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21install.php/function/hook_update_N/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_update_N/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_update_N/8',
      ),
    ),
  );

  $element['hook_update_dependencies'] = array(
    'class' => 'Forge_Element_Hook_Update_Dependencies',
    'name' => 'Update dependencies',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'install',
    'info' => array(
      'description' => 'Return an array of information about module update dependencies.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_update_dependencies/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21system%21system.api.php/function/hook_update_dependencies/8',
      ),
    ),
  );

  $element['hook_field_schema'] = array(
    'class' => 'Forge_Element_Hook_Install',
    'name' => 'Field schema structure',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'install',
    'options' => array(
      'args' => '$field',
    ),
    'info' => array(
      'description' => 'Define the Field API schema for a field structure.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_schema/7',
      ),
    ),
  );

  return $element;
}

/**
 * Implements hook_forge_module_js().
 */
function system_forge_module_js() {
  drupal_add_library('system', 'ui.tabs');
  drupal_add_js(drupal_get_path('module', 'forge') . '/modules/system/install.js');
}
