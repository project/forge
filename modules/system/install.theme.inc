<?php

/**
 * Returns HTML for presentation of project schema.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Schema object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_schema($variables) {
  $header = $rows = array();
  $class = array('forge-element', 'forge-element-multiple', 'forge-element-hook-schema');
  $header_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE, 'hook' => TRUE)), 'width' => '70%', 'colspan' => 2),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );

  if (empty($variables['element']->value)) {
    $class[] = 'forge-element-empty';
    $rows[] = array(
      array('data' => '<em>' . t('no tables found') . '</em>', 'colspan' => 4),
    );
  } else {
    $counter = -1;
    $table_output = '';
    $tabs = array();
    foreach ($variables['element']->value as $table => $struct) {
      $counter++;
      $tabs[] = '<li><a href="#schema-table-' . $counter . '">' . $table . '</a></li>';
      $table_output .= '<div id="schema-table-' . $counter . '">';

      // General info
      $info_rows = array();
      $info_class = array('forge-element-hook-schema-table-info');
      $table_links = $variables['element']->getTableLinks(array('table' => $table));
      $table_links = theme('links__ctools_dropbutton', array('links' => $table_links, 'attributes' => array('class' => array('links', 'inline'))));
      $info_rows[] = array(
        array('data' => '<strong>' . $table . '</strong>', 'width' => '30%'),
        array('data' => $struct['description'], 'width' => '50%'),
        array('data' => $table_links, 'width' => '20%', 'class' => array('forge-operations')),
      );
      $table_output .= theme('table', array('header' => array(), 'rows' => $info_rows, 'attributes' => array('class' => $info_class)));

      // Fields
      $field_rows = array();
      $field_class = array('forge-element-hook-schema-table-fields');
      $fields_links = $variables['element']->getFieldLinks(array('table' => $table, 'field' => 0));
      $fields_links = theme('links__ctools_dropbutton', array('links' => $fields_links, 'attributes' => array('class' => array('links', 'inline'))));
      $field_rows[] = array(
        array('data' => '<strong>' . drupal_strtoupper(t('Fields')) . '</strong>', 'width' => '80%', 'colspan' => 3),
        array('data' => $fields_links, 'width' => '20%', 'class' => array('forge-operations')),
      );
      if (empty($struct['fields'])) {
        $field_class[] = 'forge-element-empty';
        $field_rows[] = array(
          array('data' => t('no fields found'), 'colspan' => 4),
        );
      } else {
        $field_rows[] = array(
          array('data' => '<strong>' . t('Field name') . '</strong>', 'width' => '25%'),
          array('data' => '<strong>' . t('Description') . '</strong>', 'width' => '25%'),
          array('data' => '<strong>' . t('Field definition') . '</strong>', 'width' => '50%', 'colspan' => 2),
        );
        foreach ($struct['fields'] as $field => $fstruct) {
          $field_links = $variables['element']->getFieldLinks(array('table' => $table, 'field' => $field));
          $field_links = theme('links__ctools_dropbutton', array('links' => $field_links, 'attributes' => array('class' => array('links', 'inline'))));
          $info_items = array(
            t('Type: %value', array('%value' => $fstruct['type'])),
          );
          if (isset($fstruct['size'])) {
            $info_items[] = t('Size: %value', array('%value' => $fstruct['size']));
          }
          if (isset($fstruct['length']) && !empty($fstruct['length'])) {
            $info_items[] = t('Length: %value', array('%value' => $fstruct['length']));
          }
          if (isset($fstruct['not null'])) {
            $info_items[] = t('Not NULL: %value', array('%value' => $fstruct['not null'] ? t('Yes') : t('No')));
          }
          if (isset($fstruct['default']) && !empty($fstruct['default'])) {
            $info_items[] = t('Default: %value', array('%value' => $fstruct['default']));
          }
          if (isset($fstruct['unsigned'])) {
            $info_items[] = t('Unsigned: %value', array('%value' => $fstruct['unsigned'] ? t('Yes') : t('No')));
          }
          if (isset($fstruct['precision'])) {
            $info_items[] = t('Precision: %value', array('%value' => $fstruct['precision']));
          }
          if (isset($fstruct['scale'])) {
            $info_items[] = t('Scale: %value', array('%value' => $fstruct['scale']));
          }
          if (isset($fstruct['serialize'])) {
            $info_items[] = t('Serialize: %value', array('%value' => $fstruct['serialize'] ? t('Yes') : t('No')));
          }
          if ($fstruct['type'] == 'custom') {
            $supported = array(
              'mysql' => t('MySQL/MariaDB'),
              'pgsql' => t('PostgreSQL'),
              'sqlite' => t('SQLite'),
              'mssql' => t('MS SQL Server'),
              'oracle' => t('Oracle'),
            );
            foreach ($supported as $type => $title) {
              if (isset($fstruct['custom'][$type]) && !empty($fstruct['custom'][$type])) {
                $info_items[] = $title . ': ' . $fstruct['custom'][$type];
              }
            }
          }
          $field_rows[] = array(
            array('data' => $field, 'width' => '25%'),
            array('data' => $fstruct['description'], 'width' => '25%'),
            array('data' => theme('item_list', array('items' => $info_items)), 'width' => '25%'),
            array('data' => $field_links, 'width' => '25%', 'class' => array('forge-operations')),
          );
        }
      }
      $table_output .= theme('table', array('header' => array(), 'rows' => $field_rows, 'attributes' => array('class' => $field_class)));

      // Primary keys
      $pkey_rows = array();
      $pkey_class = array('forge-element-hook-schema-table-pkey');
      $pkey_links = $variables['element']->getPkeyLinks(array('table' => $table, 'pkey' => 0));
      $pkey_links = theme('links__ctools_dropbutton', array('links' => $pkey_links, 'attributes' => array('class' => array('links', 'inline'))));
      $pkey_row = array(array('data' => '<strong>' . drupal_strtoupper(t('Primary keys')) . '</strong>', 'width' => '25%'));
      if (empty($struct['primary key'])) {
        $pkey_class[] = 'forge-element-empty';
        $pkey_row[] = array('data' => t('no primary keys found'), 'width' => '50%');
      } else {
        $pkey_row[] = array('data' => theme('item_list', array('items' => $struct['primary key'])), 'width' => '50%');
      }
      $pkey_row[] = array('data' => $pkey_links, 'width' => '25%', 'class' => array('forge-operations'));
      $pkey_rows[] = $pkey_row;
      $table_output .= theme('table', array('header' => array(), 'rows' => $pkey_rows, 'attributes' => array('class' => $pkey_class)));

      // Unique keys
      $ukey_rows = array();
      $ukey_class = array('forge-element-hook-schema-table-ukey');
      $ukeys_links = $variables['element']->getUkeyLinks(array('table' => $table, 'ukey' => 0));
      $ukeys_links = theme('links__ctools_dropbutton', array('links' => $ukeys_links, 'attributes' => array('class' => array('links', 'inline'))));
      $ukey_rows[] = array(
        array('data' => '<strong>' . drupal_strtoupper(t('Unique keys')) . '</strong>', 'width' => '75%', 'colspan' => 2),
        array('data' => $ukeys_links, 'width' => '25%', 'class' => array('forge-operations')),
      );
      if (empty($struct['unique keys'])) {
        $ukey_class[] = 'forge-element-empty';
        $ukey_rows[] = array(
          array('data' => t('no unique keys found'), 'colspan' => 3),
        );
      } else {
        $ukey_rows[] = array(
          array('data' => '<strong>' . t('Key name') . '</strong>', 'width' => '25%'),
          array('data' => '<strong>' . t('Columns') . '</strong>', 'width' => '70%', 'colspan' => 2),
        );
        foreach ($struct['unique keys'] as $key => $columns) {
          $ukey_links = $variables['element']->getUkeyLinks(array('table' => $table, 'ukey' => $key));
          $ukey_links = theme('links__ctools_dropbutton', array('links' => $ukey_links, 'attributes' => array('class' => array('links', 'inline'))));
          $ukey_rows[] = array(
            array('data' => $key, 'width' => '25%'),
            array('data' => theme('item_list', array('items' => $columns)), 'width' => '50%'),
            array('data' => $ukey_links, 'width' => '25%', 'class' => array('forge-operations')),
          );
        }
      }
      $table_output .= theme('table', array('header' => array(), 'rows' => $ukey_rows, 'attributes' => array('class' => $ukey_class)));

      // Indexes
      $index_rows = array();
      $index_class = array('forge-element-hook-schema-table-index');
      $indexes_links = $variables['element']->getIndexLinks(array('table' => $table, 'index' => 0));
      $indexes_links = theme('links__ctools_dropbutton', array('links' => $indexes_links, 'attributes' => array('class' => array('links', 'inline'))));
      $index_rows[] = array(
        array('data' => '<strong>' . drupal_strtoupper(t('Indexes')) . '</strong>', 'width' => '75%', 'colspan' => 2),
        array('data' => $indexes_links, 'width' => '25%', 'class' => array('forge-operations')),
      );
      if (empty($struct['indexes'])) {
        $index_class[] = 'forge-element-empty';
        $index_rows[] = array(
          array('data' => t('no indexes found'), 'colspan' => 3),
        );
      } else {
        $index_rows[] = array(
          array('data' => '<strong>' . t('Index name') . '</strong>', 'width' => '25%'),
          array('data' => '<strong>' . t('Columns') . '</strong>', 'width' => '50%', 'colspan' => 2),
        );
        foreach ($struct['indexes'] as $key => $columns) {
          $index_links = $variables['element']->getIndexLinks(array('table' => $table, 'index' => $key));
          $index_links = theme('links__ctools_dropbutton', array('links' => $index_links, 'attributes' => array('class' => array('links', 'inline'))));
          $index_rows[] = array(
            array('data' => $key, 'width' => '25%'),
            array('data' => theme('item_list', array('items' => $columns)), 'width' => '50%'),
            array('data' => $index_links, 'width' => '25%', 'class' => array('forge-operations')),
          );
        }
      }
      $table_output .= theme('table', array('header' => array(), 'rows' => $index_rows, 'attributes' => array('class' => $index_class)));

      // Foreign keys
      $fk_rows = array();
      $fk_class = array('forge-element-hook-schema-table-fk');
      $fk_links = $variables['element']->getFkeyLinks(array('table' => $table, 'fkey' => 0));
      $fk_links = theme('links__ctools_dropbutton', array('links' => $fk_links, 'attributes' => array('class' => array('links', 'inline'))));
      $fk_rows[] = array(
        array('data' => '<strong>' . drupal_strtoupper(t('Foreign keys')) . '</strong>', 'width' => '75%', 'colspan' => 3),
        array('data' => $fk_links, 'width' => '25%', 'class' => array('forge-operations')),
      );
      if (empty($struct['foreign keys'])) {
        $fk_class[] = 'forge-element-empty';
        $fk_rows[] = array(
          array('data' => t('no foreign keys found'), 'colspan' => 4),
        );
      } else {
        $fk_rows[] = array(
          array('data' => '<strong>' . t('Key name') . '</strong>', 'width' => '25%'),
          array('data' => '<strong>' . t('Foreign table') . '</strong>', 'width' => '25%'),
          array('data' => '<strong>' . t('Source and target columns') . '</strong>', 'width' => '50%', 'colspan' => 2),
        );
        foreach ($struct['foreign keys'] as $fkey => $frow) {
          $columns = array();
          foreach ($frow['columns'] as $lcolumn => $fcolumn) {
            if ($lcolumn && $fcolumn) {
              $columns[] = $lcolumn . ' (<em>' . $frow['table'] . '.' . $fcolumn . '</em>)';
            }
          }
          $sfk_links = $variables['element']->getFkeyLinks(array('table' => $table, 'fkey' => $fkey));
          $sfk_links = theme('links__ctools_dropbutton', array('links' => $sfk_links, 'attributes' => array('class' => array('links', 'inline'))));
          $fk_rows[] = array(
            array('data' => $fkey, 'width' => '25%'),
            array('data' => $frow['table'], 'width' => '25%'),
            array('data' => theme('item_list', array('items' => $columns)), 'width' => '25%'),
            array('data' => $sfk_links, 'width' => '25%', 'class' => array('forge-operations')),
          );
        }
      }
      $table_output .= theme('table', array('header' => array(), 'rows' => $fk_rows, 'attributes' => array('class' => $fk_class)));

      // Table options
      $to_rows = array();
      $to_class = array('forge-element-hook-schema-table-options');
      $to_links = $variables['element']->getToLinks(array('table' => $table, 'option' => 0));
      $to_links = theme('links__ctools_dropbutton', array('links' => $to_links, 'attributes' => array('class' => array('links', 'inline'))));
      $to_row = array(array('data' => '<strong>' . drupal_strtoupper(t('Table options')) . '</strong>', 'width' => '25%'));
      $to_items = array();
      $keys = array('mysql_suffix', 'mysql_engine', 'mysql_character_set', 'collation');
      foreach ($keys as $key) {
        if (!empty($struct[$key])) {
          $to_items[] = $key . ' = "' . $struct[$key] . '"';
        }
      }
      if (empty($to_items)) {
        $to_class[] = 'forge-element-empty';
        $to_row[] = array('data' => t('no options defined'), 'width' => '50%');
      } else {
        $to_row[] = array('data' => theme('item_list', array('items' => $to_items)), 'width' => '50%');
      }
      $to_row[] = array('data' => $to_links, 'width' => '25%', 'class' => array('forge-operations'));
      $to_rows[] = $to_row;
      $table_output .= theme('table', array('header' => array(), 'rows' => $to_rows, 'attributes' => array('class' => $to_class)));

      $table_output .= '</div>';
    }
    $tabs = '<ul>' . implode("\n", $tabs) . '</ul>';
    $rows[] = array(
      array('data' => '<div id="schema-tabs">' . $tabs . $table_output . '</div>', 'colspan' => 4),
    );
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Returns HTML for presentation of foreign key editing page.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: Form part to rendering.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_schema_fkey($variables) {
  unset($variables['form']['#theme']);

  $header = array(
    t('Source column'),
    t('Target column'),
    t('Operations'),
  );
  $rows = array();
  foreach ($variables['form'] as $key => $row) {
    if (is_int($key)) {
      $rows[] = array(
        render($row['lcolumn']),
        render($row['fcolumn']),
        render($row['remove']),
      );
    }
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Returns HTML for presentation of update dependencies.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Update_Dependencies object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_update_dependencies($variables) {
  $header = $rows = array();

  $header_links = $info_links = $variables['element']->getPrimaryLinks();
  $header_links = theme('links__ctools_dropbutton', array('links' => $header_links, 'attributes' => array('class' => array('links', 'inline'))));
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('upper' => TRUE, 'hook' => TRUE)), 'class' => 'forge-element-info-multiple-name', 'width' => '70%', 'colspan' => 2),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $header_links, 'class' => 'forge-element-info-multiple-links', 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );
  foreach ($variables['element']->value as $module => $row) {
    foreach ($row as $number => $nrow) {
      $value_links = $variables['element']->getSecondaryLinks(array('module' => $module, 'number' => $number));
      $value_links = theme('links__ctools_dropbutton', array('links' => $value_links, 'attributes' => array('class' => array('links', 'inline'))));
      $dependencies = array();
      foreach ($nrow as $nmodule => $nnumbers) {
        $dependencies[] = $nmodule . ' (<em>' . implode(', ', $nnumbers) . '</em>)';
      }
      $rows[] = array(
        array('data' => $module, 'width' => '25%'),
        array('data' => $number, 'width' => '25%'),
        array('data' => theme('item_list', array('items' => $dependencies)), 'width' => '30%', 'colspan' => 2),
        array('data' => $value_links, 'width' => '20%', 'class' => array('forge-operations')),
      );
    }
  }
  $class = array('forge-element', 'forge-element-hook-update-dependencies');
  if (count($rows) == 2) {
    $class[] = 'forge-element-empty';
    $rows[] = array(
      array('data' => t('no dependencies found'), 'colspan' => 4),
    );
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));

  return $output;
}

/**
 * Returns HTML for presentation of update dependencies editing page.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: Form part to rendering.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_update_dependence($variables) {
  unset($variables['form']['#theme']);

  $header = array(
    t('Module'),
    t('Number'),
    t('Operations'),
  );
  $rows = array();
  foreach ($variables['form'] as $key => $row) {
    if (is_int($key)) {
      $rows[] = array(
        render($row['module']),
        render($row['number']),
        render($row['remove']),
      );
    }
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Render hook_schema element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Schema object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constant.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_schema($variables) {
  $tables = $variables['element']->value;
  foreach ($tables as &$table) {
    foreach ($table['fields'] as &$field) {
      if (isset($field['not null'])) {
        if (!empty($field['not null'])) {
          $field['not null'] = 'TRUE';
        } else {
          unset($field['not null']);
        }
      }
      if (isset($field['unsigned']) && !empty($field['unsigned'])) {
        $field['unsigned'] = 'TRUE';
      }
      if (isset($field['type'])) {
        if (in_array($field['type'], array('float', 'numeric')) && isset($field['default'])) {
          $field['default'] = (float)$field['default'];
        } elseif (in_array($field['type'], array('int', 'serial', 'float', 'numeric', 'datetime', 'custom')) && isset($field['default'])) {
          if (empty($field['default'])) {
            unset($field['default']);
          }
        }
      }
      if ($field['type'] == 'custom') {
        $supported = array(
          'mysql' => t('MySQL/MariaDB'),
          'pgsql' => t('PostgreSQL'),
          'sqlite' => t('SQLite'),
          'mssql' => t('MS SQL Server'),
          'oracle' => t('Oracle'),
        );
        foreach ($supported as $type => $title) {
          if (isset($field['custom'][$type]) && !empty($field['custom'][$type])) {
            $field[$type . '_type'] = $field['custom'][$type];
          }
        }
        unset($field['custom']);
        unset($field['type']);
      }
      if (isset($field['type']) && $field['type'] == 'text') {
        if (isset($field['length']) && empty($field['length'])) {
          unset($field['length']);
        }
      }
    }

    switch ($variables['core']) {
      case Forge::CORE_SUPPORT_6x:
        if (isset($table['mysql_engine'])) {
          unset($table['mysql_engine']);
        }
        if (isset($table['mysql_character_set'])) {
          unset($table['mysql_character_set']);
        }
        if (isset($table['collation'])) {
          unset($table['collation']);
        }
        if (isset($table['mysql_suffix']) && empty($table['mysql_suffix'])) {
          unset($table['mysql_suffix']);
        }
        break;
      case Forge::CORE_SUPPORT_7x:
        if (isset($table['mysql_suffix'])) {
          unset($table['mysql_suffix']);
        }
        if (isset($table['mysql_engine']) && empty($table['mysql_engine'])) {
          unset($table['mysql_engine']);
        }
        if (isset($table['mysql_character_set']) && empty($table['mysql_character_set'])) {
          unset($table['mysql_character_set']);
        }
        if (isset($table['collation']) && empty($table['collation'])) {
          unset($table['collation']);
        }
        break;
    }
  }

  $body = theme('forge_array', array('array' => $tables));
  $body = '$schema = ' . $body . ";\n\nreturn " . '$schema;';

  $variables['args'] = '';
  $variables['body'] = $body;

  return theme('forge_element_render_hook', $variables);
}

/**
 * Render hook_update_N element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Update_N object to render.
 *   - core:    One of Forge::CORE_SUPPORT_*x constant.
 *
 * @ingroup themeable
 */
function theme_forge_element_render_hook_update_n($variables) {
  $variables['args'] = '&$sandbox';
  switch ($variables['core']) {
    case Forge::CORE_SUPPORT_6x:
      $variables['body'] = <<<EOL
\$ret = array();

//

return \$ret;
EOL;
      break;
    case Forge::CORE_SUPPORT_7x:
      $variables['body'] = <<<EOL
\$sandbox['#finished'] = 0;

//

\$sandbox['#finished'] = 1;
EOL;
      break;
  }

  return theme('forge_element_render_hook', $variables);
}
