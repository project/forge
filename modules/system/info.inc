<?php

/**
 * Info elements definition.
 *
 * @see system_forge_element()
 */
function _system_forge_element_info() {
  // Common elements
  $default_info = array(
    'apiUrl' => array(
      Forge::CORE_SUPPORT_6x => 'https://drupal.org/node/231036',
      Forge::CORE_SUPPORT_7x => 'https://drupal.org/node/542202',
      Forge::CORE_SUPPORT_8x => 'https://drupal.org/node/1935708',
    ),
  );
  $element['name'] = array(
    'class' => 'Forge_Element_Info',
    'name' => 'Name',
    'defaultValue' => '',
    'projectType' => Forge::PROJECT_TYPE_ALL,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'options' => array('required' => TRUE),
    'info' => array(
      'description' => 'The title of your module, which will be shown on the Modules page.',
    ) + $default_info,
  );
  $element['description'] = array(
    'class' => 'Forge_Element_Info',
    'name' => 'Description',
    'defaultValue' => '',
    'projectType' => Forge::PROJECT_TYPE_ALL,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => 'A short, preferably one-line description, which will appear on the Modules page.',
    ) + $default_info,
  );
  $element['core'] = array(
    'class' => 'Forge_Element_Info_Core',
    'name' => 'Drupal core',
    'defaultValue' => array(
      Forge::CORE_SUPPORT_6x => '6.x',
      Forge::CORE_SUPPORT_7x => '7.x',
    ),
    'projectType' => Forge::PROJECT_TYPE_ALL,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => 'The version of Drupal that your module is for.',
    ) + $default_info,
  );
  $element['php'] = array(
    'class' => 'Forge_Element_Info_Php',
    'name' => 'PHP version',
    'defaultValue' => array(
      Forge::CORE_SUPPORT_6x => '5.0',
      Forge::CORE_SUPPORT_7x => DRUPAL_MINIMUM_PHP,
    ),
    'projectType' => Forge::PROJECT_TYPE_ALL,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => 'Minimum PHP version required.',
    ) + $default_info,
  );
  // Module and theme elements
  $element['stylesheets'] = array(
    'class' => 'Forge_Element_Info_Multiple_Stylesheets',
    'name' => 'Stylesheets',
    'defaultValue' => array(
      //                          @see http://www.w3schools.com/css/css_mediatypes.asp
      'all'        => array(), // Used for all media type devices
      'print'      => array(), // ... printers
      'aural'      => array(), // ... speech and sound synthesizers
      'braille'    => array(), // ... braille tactile feedback devices
      'embossed'   => array(), // ... paged braille printers
      'handheld'   => array(), // ... small or handheld devices
      'projection' => array(), // ... projected presentations, like slides
      'screen'     => array(), // ... computer screens
      'tty'        => array(), // ... media using a fixed-pitch character grid, like teletypes and terminals
      'tv'         => array(), // ... television-type devices
    ),
    'projectType' => Forge::PROJECT_TYPE_MODULE | Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'info',
    'info' => array(
      'description' => 'Add CSS file on every page.',
    ) + $default_info,
  );
  $element['scripts'] = array(
    'class' => 'Forge_Element_Info_Multiple_Js',
    'name' => 'JavaScript files',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE | Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'info',
    'info' => array(
      'description' => 'Add JS file on every page.',
    ) + $default_info,
  );
  // Module elements
  $element['files'] = array(
    'class' => 'Forge_Element_Info_Multiple_Files',
    'name' => 'Dynamic-loaded files',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'info',
    'info' => array(
      'description' => 'Classes or interfaces autoloader.',
    ) + $default_info,
  );
  $element['dependencies'] = array(
    'class' => 'Forge_Element_Info_Multiple',
    'name' => 'Required modules',
    'defaultValue' => array(),
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => 'List of required modules.',
    ) + $default_info,
  );
  $element['package'] = array(
    'class' => 'Forge_Element_Info',
    'name' => 'Package',
    'defaultValue' => '',
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => 'Modules package name.',
    ) + $default_info,
  );
  $element['configure'] = array(
    'class' => 'Forge_Element_Info',
    'name' => 'Configuration page',
    'defaultValue' => '',
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_MORE_THAN_6x,
    'group' => 'info',
    'info' => array(
      'description' => 'The path of the module configuration page.',
    ) + $default_info,
  );
  $element['required'] = array(
    'class' => 'Forge_Element_Info_Boolean',
    'name' => 'Enable module automatically during install',
    'defaultValue' => 'NULL',
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'info',
    'info' => array(
      'description' => 'Project should never be disabled flag.',
    ) + $default_info,
  );
  $element['hidden'] = array(
    'class' => 'Forge_Element_Info_Boolean',
    'name' => 'Hidden in module list',
    'defaultValue' => 'NULL',
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => 'Project should not be visible on the modules page flag.',
    ) + $default_info,
  );
  $element['project status url'] = array(
    'class' => 'Forge_Element_Info',
    'name' => 'Update URL',
    'defaultValue' => '',
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'info',
    'info' => array(
      'description' => 'URL to check for updates to their module.',
    ) + $default_info,
  );
  // Theme elements
  $default_info = array(
    'apiUrl' => array(
      Forge::CORE_SUPPORT_6x => 'https://drupal.org/node/171205',
      Forge::CORE_SUPPORT_7x => 'https://drupal.org/node/171205',
    ),
  );
  $element['screenshot'] = array(
    'class' => 'Forge_Element_Info',
    'name' => "Theme's thumbnail image",
    'defaultValue' => '',
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => "Theme's thumbnail image, used on the theme selection page.",
    ) + $default_info,
  );
  $element['base theme'] = array(
    'class' => 'Forge_Element_Info',
    'name' => 'Machine name of the parent theme',
    'defaultValue' => '',
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => 'Machine name of the parent theme.',
    ) + $default_info,
  );
  $element['engine'] = array(
    'class' => 'Forge_Element_Info',
    'name' => 'Theme engine',
    'defaultValue' => 'phptemplate',
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_6x,
    'group' => 'info',
    'info' => array(
      'description' => 'The theme engine, which is used by the theme.',
    ) + $default_info,
  );
  $element['features'] = array(
    'class' => 'Forge_Element_Info_Multiple',
    'name' => 'Theme features',
    'defaultValue' => array(
      'logo',
      'name',
      'slogan',
      'node_user_picture',
      'comment_user_picture',
      'comment_user_verification',
      'favicon',
      'main_menu',
      'secondary_menu',
    ),
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => 'Control which page elements display on the page.',
    ) + $default_info,
  );
  $element['regions'] = array(
    'class' => 'Forge_Element_Info_Multiple_Regions',
    'name' => 'Regions',
    'defaultValue' => array(
      'sidebar_first' => 'Left sidebar',
      'sidebar_second' => 'Right sidebar',
      'content' => 'Content',
      'header' => 'Header',
      'footer' => 'Footer',
      'left' => 'Left',
      'right' => 'Right',
      'highlighted' => 'Highlighted',
      'help' => 'Help',
      'page_top' => 'Page Top',
      'page_bottom' => 'Page Bottom',
    ),
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'info',
    'info' => array(
      'description' => 'Block regions definition.',
    ) + $default_info,
  );
  $element['settings'] = array(
    'class' => 'Forge_Element_Info_Multiple',
    'name' => 'Theme settings',
    'defaultValue' => array(
      'toggle_logo',
      'toggle_name',
      'toggle_slogan',
      'toggle_node_user_picture',
      'toggle_comment_user_picture',
      'toggle_comment_user_verification',
      'toggle_favicon',
      'toggle_main_menu',
      'toggle_secondary_menu',
    ),
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'info',
    'info' => array(
      'description' => 'Additional theme settings.',
    ) + $default_info,
  );

  return $element;
}
