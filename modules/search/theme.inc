<?php

/**
 * Returns HTML for presentation of search info hook.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Forge_Element_Hook_Search_Info object to render.
 *
 * @ingroup themeable
 */
function theme_forge_element_present_hook_search_info($variables) {
  $header = $rows = array();

  $class = array('forge-element', 'forge-element-hook-search-info');
  $links = $variables['element']->getLinks();
  $links = theme('links__ctools_dropbutton', array('links' => $links, 'attributes' => array('class' => array('links', 'inline'))));
  $info = array();
  if ($variables['element']->value) {
    $value = $variables['element']->value;
    $info[] = t('Title: %value', array('%value' => $value['title']));
    $info[] = t('Path: %value', array('%value' => $value['path']));
    if ($value['conditions_callback']) {
      $info[] = t('Conditions callback: %value', array('%value' => $value['conditions_callback']));
    }
    $info = theme('item_list', array('items' => $info));
  } else {
    $info = t('no search info found');
    $class[] = 'forge-element-empty';
  }
  $rows[] = array(
    array('data' => theme('forge_element_title', $variables + array('hook' => TRUE)), 'width' => '30%'),
    array('data' => $info, 'width' => '40%'),
    array('data' => theme('forge_element_core_support', $variables), 'class' => 'forge-element-core-support', 'width' => '10%'),
    array('data' => $links, 'width' => '20%', 'class' => array('forge-operations')),
  );
  $rows[] = array(
    'data' => array(
      array('data' => theme('forge_element_description', $variables), 'class' => 'forge-element-description', 'colspan' => 4),
    ),
    'class' => array('forge-element-description'),
    'no_striping' => TRUE,
  );

  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => $class, 'id' => $variables['element']->getHtmlId())));
}
