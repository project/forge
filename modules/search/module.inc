<?php

/**
 * @file
 * Provides Forge hooks for search.module.
 */

/**
 * Implements hook_forge_group().
 */
function search_forge_group() {
  $group['search'] = $group['theme_search'] = array(
    'title' => 'Search',
  );
  return $group;
}

/**
 * Implements hook_forge_element().
 */
function search_forge_element() {
  // Module support.
  $element['hook_search_info'] = array(
    'class' => 'Forge_Element_Hook_Search_Info',
    'name' => 'Search type',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'search',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_6x => '$op = \'search\', $keys = NULL',
        Forge::CORE_SUPPORT_7x => '',
      ),
    ),
    'info' => array(
      'description' => 'Define a custom search type.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_search/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_search_info/7',
      ),
    ),
  );
  $element['hook_search_page'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Search result page',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'search',
    'options' => array(
      'args' => '$results',
    ),
    'info' => array(
      'description' => 'Override the rendering of search results.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_search_page/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_search_page/7',
      ),
    ),
  );
  $element['hook_search_preprocess'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Preprocess text',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_ALL,
    'group' => 'search',
    'options' => array(
      'args' => '$text',
    ),
    'info' => array(
      'description' => 'Preprocess text for the search index.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/developer%21hooks%21core.php/function/hook_search_preprocess/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_search_preprocess/7',
        Forge::CORE_SUPPORT_8x => 'https://api.drupal.org/api/drupal/core%21modules%21search%21search.api.php/function/hook_search_preprocess/8',
      ),
    ),
  );
  $element['hook_search_access'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Access search',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'search',
    'info' => array(
      'description' => 'Define access to a custom search routine.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_search_access/7',
      ),
    ),
  );
  $element['hook_search_reset'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Rebuild index',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'search',
    'info' => array(
      'description' => 'Take action when the search index is going to be rebuilt.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_search_reset/7',
      ),
    ),
  );
  $element['hook_search_status'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Index status',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'search',
    'info' => array(
      'description' => 'Report the status of indexing.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_search_status/7',
      ),
    ),
  );
  $element['hook_search_admin'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Settings form',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'search',
    'info' => array(
      'description' => 'Add elements to the search settings form.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_search_admin/7',
      ),
    ),
  );
  $element['hook_search_execute'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Search execute',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'search',
    'options' => array(
      'args' => '$keys = NULL, $conditions = NULL',
    ),
    'info' => array(
      'description' => 'Execute a search for a set of key words.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_search_execute/7',
      ),
    ),
  );
  $element['hook_update_index'] = array(
    'class' => 'Forge_Element_Hook',
    'name' => 'Update index',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_MODULE,
    'coreSupport' => Forge::CORE_SUPPORT_7x,
    'group' => 'search',
    'info' => array(
      'description' => 'Update the search index for this module.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_update_index/7',
      ),
    ),
  );

  // Theme support.
  $element['template_preprocess_search_block_form'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Preprocess variables for theme_search_block_form()',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_search',
    'options' => array(
      'args' => '&$variables',
    ),
    'info' => array(
      'description' => 'Process variables for search-block-form.tpl.php.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21search%21search.module/function/template_preprocess_search_block_form/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.module/function/template_preprocess_search_block_form/7',
      ),
    ),
  );

  $element['template_preprocess_search_results'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Preprocess variables for theme_search_results()',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$variables',
      ),
    ),
    'info' => array(
      'description' => 'Process variables for search-results.tpl.php.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21search%21search.pages.inc/function/template_preprocess_search_results/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.pages.inc/function/template_preprocess_search_results/7',
      ),
    ),
  );

  $element['template_preprocess_search_result'] = array(
    'class' => 'Forge_Element_Theme',
    'name' => 'Preprocess variables for theme_search_result()',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme_node',
    'options' => array(
      'args' => array(
        Forge::CORE_SUPPORT_7x => '$variables',
      ),
    ),
    'info' => array(
      'description' => 'Process variables for search-result.tpl.php.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21search%21search.pages.inc/function/template_preprocess_search_result/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search.pages.inc/function/template_preprocess_search_result/7',
      ),
    ),
  );

  $element['search-block-form.tpl.php'] = array(
    'class' => 'Forge_Element_Template',
    'name' => 'Theme implementation for search form',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme',
    'options' => array(
      'args' => 'modules/search/search-block-form.tpl.php',
    ),
    'info' => array(
      'description' => 'Default theme implementation for displaying a search form within a block region.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21search%21search-block-form.tpl.php/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search-block-form.tpl.php/7',
      ),
    ),
  );

  $element['search-results.tpl.php'] = array(
    'class' => 'Forge_Element_Template',
    'name' => 'Theme implementation for search results',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme',
    'options' => array(
      'args' => 'modules/search/search-results.tpl.php',
    ),
    'info' => array(
      'description' => 'Default theme implementation for displaying search results.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21search%21search-results.tpl.php/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search-results.tpl.php/7',
      ),
    ),
  );

  $element['search-result.tpl.php'] = array(
    'class' => 'Forge_Element_Template',
    'name' => 'Theme implementation for search result',
    'defaultValue' => FALSE,
    'projectType' => Forge::PROJECT_TYPE_THEME,
    'coreSupport' => Forge::CORE_SUPPORT_BOTH_6x_7x,
    'group' => 'theme',
    'options' => array(
      'args' => 'modules/search/search-result.tpl.php',
    ),
    'info' => array(
      'description' => 'Default theme implementation for displaying search result.',
      'apiUrl' => array(
        Forge::CORE_SUPPORT_6x => 'https://api.drupal.org/api/drupal/modules%21search%21search-result.tpl.php/6',
        Forge::CORE_SUPPORT_7x => 'https://api.drupal.org/api/drupal/modules%21search%21search-result.tpl.php/7',
      ),
    ),
  );

  return $element;
}

/**
 * Implements hook_forge_theme().
 */
function search_forge_theme($existing, $type, $theme, $path) {
  return array(
    'forge_element_present_hook_search_info' => array(
      'variables' => array('element' => NULL),
    ),
  );
}
