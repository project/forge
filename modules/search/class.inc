<?php

/**
 * @file
 * Class implementation of a search hook elements.
 */

/**
 * Search info definition hook class.
 *
 * @see Forge_Element_Hook
 * @see https://api.drupal.org/api/drupal/modules%21search%21search.api.php/function/hook_search_info/7
 */
class Forge_Element_Hook_Search_Info extends Forge_Element_Hook {
  public $presentTheme = 'forge_element_present_hook_search_info';

  /**
   * Returns primary operations of the element.
   *
   * @return array
   * @ingroup operations
   */
  public function getLinks() {
    $op = $this->value ? 'edit' : 'add';
    $links = array(
      forge_popup_link($op, $this, $op),
      forge_link('clear', $this, 'clear'),
    );

    return $links;
  }

  /**
   * Clear an element.
   *
   * @ingroup operations
   */
  public function clear() {
    $this->value = array();
  }

  /**
   * Returns Drupal form for element editing.
   *
   * @see Forge_Element::getForm()
   * @ingroup forms
   */
  public function getForm($input = array()) {
    $input = $this->value;
    $form = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($input['title']) ? $input['title'] : '',
      '#required' => TRUE,
      '#description' => t('Title for the tab on the search page for this module. Defaults to the module name if not given.'),
    );
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#default_value' => isset($input['path']) ? $input['path'] : '',
      '#required' => TRUE,
      '#description' => t("Path component after 'search/' for searching with this module. Defaults to the module name if not given."),
    );
    $form['conditions_callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Conditions callback'),
      '#default_value' => isset($input['conditions_callback']) ? $input['conditions_callback'] : '',
      '#description' => t('An implementation of callback_search_conditions().'),
    );

    return $form;
  }

  /**
   * On-submit processing of the element.
   *
   * @see Forge_Element::submit()
   * @ingroup forms
   */
  public function submit($input) {
    $this->value = $input;
  }

  /**
   * Render the hook.
   *
   * @see Forge_Element::render()
   * @ingroup render
   */
  public function render(&$output, $core, $args = '') {
    if ($this->value) {
      $info = $this->value;
      if ($core == Forge::CORE_SUPPORT_6x) {
        $this->machine_name = 'hook_search';
        $this->renderName = $this->project . '_search';
        $body = '$title = "' . $info['title'] . '";' . PHP_EOL;
        $body .= <<<'PHP'
switch ($op) {
  case 'name':
    return t($title);

  case 'reset':
    return;

  case 'status':
    return array(
      'remaining' => 0,
      'total' => 0,
    );

  case 'admin':
    $form = array();
    return $form;

  case 'search':
    $results = array();
    return $results;
}
PHP;
      } else {
        if (!$info['conditions_callback']) {
          $info['conditions_callback'] = 'callback_search_conditions';
        }
        $body = theme('forge_array', array('array' => $info));
        $body = "return $body;";
      }

      $args = $this->optionsArgs($core, $args);
      $output[$this->renderDestination][] = theme($this->renderTheme, array('element' => $this, 'core' => $core, 'args' => $args, 'body' => $body));
      if ($core == Forge::CORE_SUPPORT_6x) {
        $this->machine_name = 'hook_search_info';
        $this->renderName = $this->project . '_search_info';
      }
    }
  }
}
